################################################################################
# Funcion     : %M%
# nombre : Catalogo de Empresas
#               Funcion para definicion de globales
# Funciones   : 
#               
#               
#  
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        nombre de la modificacion
#
################################################################################
--DATABASE comisiones
DATABASE rh

GLOBALS 
TYPE 
   tDet RECORD 
      id_commemp  LIKE commemp.id_commemp,
      codigo      LIKE commemp.codigo,
      nit         LIKE commemp.nit,
      nombre      LIKE commemp.nombre,
      iniciales   LIKE commemp.iniciales,
      direccion   LIKE commemp.direccion,
      telefono    LIKE commemp.telefono,
      espropia    LIKE commemp.espropia,
      estado      LIKE commemp.estado,
      dbname      LIKE commemp.dbname
   END RECORD

DEFINE
   reg_det DYNAMIC ARRAY OF tDet, 
   g_reg, u_reg tDet,
   dbname      STRING,
   
   condicion   STRING --Condicion de la clausula Where 
   CONSTANT    prog_name = "comm0103"

{DEFINE gr_item DYNAMIC ARRAY OF RECORD 
   id_commemp  LIKE commemp.id_commemp,
   idItem       LIKE glbmitems.iditem,
   desItem      LIKE glbmitems.desitemlg
END RECORD} 
END GLOBALS