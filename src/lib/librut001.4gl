{
Programa : librut001 
Programo : Mynor Ramirez 
Objetivo : Programa de subrutinas de libreria.
}

DATABASE rh

-- Subrutina para el menu de grabacion 

FUNCTION librut001_menugraba(msg,texto,opc1,opc2,opc3,opc4)
 DEFINE ope  SMALLINT,
        texto, 
        msg,
        opc1,
        opc2,
        opc3,
        opc4 STRING

  MENU msg 
  ATTRIBUTE(STYLE="dialog",COMMENT=texto,IMAGE="question")
  BEFORE MENU
   IF LENGTH(opc1)=0 THEN HIDE OPTION opc1 END IF
   IF LENGTH(opc2)=0 THEN HIDE OPTION opc2 END IF
   IF LENGTH(opc3)=0 THEN HIDE OPTION opc3 END IF
   IF LENGTH(opc4)=0 THEN HIDE OPTION opc4 END IF
  COMMAND opc1
   LET ope = 1
  COMMAND opc2
   LET ope = 2
  COMMAND opc3
   LET ope = 0
  COMMAND opc4 
   LET ope = 3 
  END MENU
  RETURN ope
END FUNCTION

-- Subrutina para el menu de confirmacion a una pregunta 

FUNCTION librut001_yesornot(title,msg,opc1,opc2,icon)
 DEFINE ope  SMALLINT,
        title,
        msg,
        opc1,
        opc2,
        icon STRING

  MENU title
  ATTRIBUTE(STYLE="dialog",COMMENT=msg,IMAGE=icon)
  COMMAND opc1
   LET ope = 1
  COMMAND opc2
   LET ope = 0
  END MENU
  RETURN ope
END FUNCTION

-- Subrutina para cambiar el atributo TEXT de algun objeto en un forma 

FUNCTION librut001_dpelement(ne,st)
 DEFINE st,ne STRING,
        w     ui.Window,
        f     ui.Form

 -- Obteniendo datos de la ventana
 LET w = ui.Window.getCurrent()

 -- Obteniendo datos de la forma
 LET f = w.getForm()

 -- Desplegando elemento
 CALL f.setElementText(ne,st)
END FUNCTION

-- Subrutina para un menu de opciones 

FUNCTION librut001_menuopcs(msg1,msg2,opc1,opc2,opc3,opc4)
 DEFINE ope  SMALLINT,
        msg1,
        msg2,
        opc1,
        opc2,
        opc3,
        opc4 STRING

 -- Menu de opciones 
 MENU msg1 
  ATTRIBUTE(STYLE="dialog",COMMENT=msg2,IMAGE="question")
  BEFORE MENU
   IF LENGTH(opc1)=0 THEN
      HIDE OPTION opc1
   END IF
   IF LENGTH(opc2)=0 THEN
      HIDE OPTION opc2
   END IF
  COMMAND opc1
   LET ope = 1
  COMMAND opc2
   LET ope = 2
 END MENU
 RETURN ope
END FUNCTION

-- Subrutina para los nombres de los meses del anio 

FUNCTION librut001_nombremeses(n_mes,idioma)
 DEFINE array_mes    ARRAY[12] OF CHAR(10),
        n_mes,idioma SMALLINT,
	nombre_mes   CHAR(10)

 -- Verificando idioma
 CASE (idioma)
  WHEN 0 -- Ingles
   LET array_mes[1] ="January"
   LET array_mes[2] ="February"
   LET array_mes[3] ="March"
   LET array_mes[4] ="April"
   LET array_mes[5] ="May"  
   LET array_mes[6] ="June"  
   LET array_mes[7] ="July" 
   LET array_mes[8] ="August"
   LET array_mes[9] ="September"
   LET array_mes[10]="October" 
   LET array_mes[11]="November"
   LET array_mes[12]="December" 
   LET nombre_mes   = array_mes[n_mes]
  WHEN 1 -- Espaniol
   LET array_mes[1] ="Enero"
   LET array_mes[2] ="Febrero"
   LET array_mes[3] ="Marzo"
   LET array_mes[4] ="Abril"
   LET array_mes[5] ="Mayo" 
   LET array_mes[6] ="Junio"
   LET array_mes[7] ="Julio"
   LET array_mes[8] ="Agosto"
   LET array_mes[9] ="Septiembre"
   LET array_mes[10]="Octubre"
   LET array_mes[11]="Noviembre"
   LET array_mes[12]="Diciembre"
   LET nombre_mes   = array_mes[n_mes]
 END CASE
 RETURN nombre_mes
END FUNCTION

-- Subrutina para obtener los dias del mes

FUNCTION librut001_diasmes(wmes,wanio)
 DEFINE array_dias       ARRAY[12] OF SMALLINT,
        wmes,wanio,wdias SMALLINT,
        residuo          INTEGER

 -- Verificando si es anio bisiesto
 LET residuo = (wanio mod 4)
 IF (residuo>0) THEN
    LET array_dias[2] = 28
 ELSE
    LET array_dias[2] = 29
 END IF

 -- Asigando dias del mes
 LET array_dias[1]  = 31
 LET array_dias[3]  = 31
 LET array_dias[4]  = 30
 LET array_dias[5]  = 31
 LET array_dias[6]  = 30
 LET array_dias[7]  = 31
 LET array_dias[8]  = 31
 LET array_dias[9]  = 30
 LET array_dias[10] = 31
 LET array_dias[11] = 30
 LET array_dias[12] = 31
 LET wdias = array_dias[wmes]
 RETURN wdias
END FUNCTION

-- Subrutina para formatear una fecha 

FUNCTION librut001_formatofecha(w_fecha,idioma,pais)
 DEFINE w_mname,w_dname CHAR(10),
        w_month,w_day   SMALLINT,
        idioma          SMALLINT,
        w_date          CHAR(78),
        pais            CHAR(30),
        abbr            CHAR(2),
        w_fecha         DATE

 -- Obteniendo datos de la fecha
 LET w_date  = NULL
 LET w_month = MONTH(w_fecha)
 LET w_mname = librut001_nombremeses(w_month,idioma)
 LET w_day   = WEEKDAY(w_fecha)  
 LET w_dname = librut001_nombredias(w_day,idioma)

 -- Verificando pais
 IF (LENGTH(pais)>0) THEN
    LET pais = pais CLIPPED,","
 END IF 

 -- Obteniendo fecha formateada  
 CASE (idioma)
  WHEN 0 -- Ingles
   -- Verificando numero de dia
   CASE (DAY(w_fecha))
    WHEN 1    LET abbr = "st"
    WHEN 2    LET abbr = "nd"
    WHEN 3    LET abbr = "rd"
    OTHERWISE LET abbr = "th"
   END CASE 

   -- Construyendo fecha
   LET w_date = pais CLIPPED," ",w_mname CLIPPED," ",
                DAY(w_fecha) USING "<<<<<",abbr,", ",
                YEAR(w_fecha) USING "<<<<","."

  WHEN 1 -- Espaniol
   LET w_date = pais CLIPPED," ",
                DAY(w_fecha) USING"<<<<<"," de ",
                w_mname CLIPPED," de ",
                YEAR(w_fecha) USING "<<<<","."
 END CASE
 RETURN w_date
END FUNCTION

-- Subrutina para desplegar valores

FUNCTION librut001_valores(wcodigo)
 DEFINE wcodigo CHAR(3)
 
 -- Evaluando valor
 CASE (wcodigo)
  WHEN "V" RETURN "VIGENTE" 
  WHEN "A" RETURN "ANULADO" 
 END CASE
END FUNCTION 

-- Subrutina para los nombre de los dias de la semana 

FUNCTION librut001_nombredias(w_day,idioma)
 DEFINE w_arrayw ARRAY[7] OF CHAR(10),
	w_dname  CHAR(9),
	w_day    SMALLINT,
	idioma   SMALLINT        

 -- Verificando dias de la semana
 IF (w_day=0) THEN
    LET w_day = 7
 END IF

 -- Verificando idioma   
 CASE (idioma)
  WHEN 0 -- Ingles
   LET w_arrayw[1] = "Monday" 
   LET w_arrayw[2] = "Tuesday"
   LET w_arrayw[3] = "Wednesday"
   LET w_arrayw[4] = "Thrusday"
   LET w_arrayw[5] = "Friday"
   LET w_arrayw[6] = "Saturday"
   LET w_arrayw[7] = "Sunday"
   LET w_dname     = w_arrayw[w_day]   
  WHEN 1 -- Espaniol
   LET w_arrayw[1] = "Lunes"  
   LET w_arrayw[2] = "Martes"
   LET w_arrayw[3] = "Miercoles"
   LET w_arrayw[4] = "Jueves"
   LET w_arrayw[5] = "Viernes"
   LET w_arrayw[6] = "Sabado"
   LET w_arrayw[7] = "Domingo"
   LET w_dname     = w_arrayw[w_day]   
 END CASE

 RETURN w_dname
END FUNCTION

-- Subrutina para desplegar datos del encabezado de un programa en pantala

{FUNCTION librut001_header(wcodpro,wpais,wlang) 
 DEFINE wcodpro LIKE glb_programs.codpro,
        wdcrpro LIKE glb_programs.dcrpro,
        wlang   SMALLINT, 
        wfecha  STRING,
        wpais   STRING 

 -- Seleccionando nombre del programa
 SELECT NVL(a.dcrpro,"NO DEFINIDO")
  INTO  wdcrpro
  FROM  glb_programs a
  WHERE (a.codpro = wcodpro)

 -- Desplegando titulo del programa
 CALL fgl_settitle(wdcrpro)

 -- Deslegando fecha 
 LET wfecha = librut001_formatofecha(TODAY,wlang,wpais) CLIPPED
 CALL librut001_dpelement("labelz",wfecha)

 -- Despelgando usuario
 MESSAGE " Usuario: "||FGL_GETENV("LOGNAME") CLIPPED ATTRIBUTE(RED) 
END FUNCTION }

-- Subrutina para configurar los tipos de letras de acuerdo la impresora

FUNCTION librut001_fontsprn(pipeline,impresora)
 DEFINE fnt     RECORD
        cmp,nrm   CHAR(12),
        tbl,fbl   CHAR(12),
        t88,t66   CHAR(12),
        p12,p10   CHAR(12),
        srp       CHAR(12),
        twd,fwd   CHAR(12),
        tda,fda   CHAR(12),
        ini       CHAR(12)
       END RECORD,
       impresora   STRING,
       pipeline    STRING 

 -- Definiendo tipos de fonts
 INITIALIZE fnt.* TO NULL

 -- Verificando si salida es pantalla, pdf o corre electronico
 IF (pipeline="screen") OR
    (pipeline="pdf") OR
    (pipeline MATCHES "*@*") THEN
    RETURN fnt.*
 END IF

 -- Verificando tipos de impresora
 CASE (impresora)
  WHEN "epson"
   -- Salida a impresora EPSON ESC/2
   LET fnt.cmp = ASCII 15 --condensado
   LET fnt.nrm = ASCII 18 --normal
   LET fnt.tbl = ASCII 27,ASCII 71 --Habilita negrita
   LET fnt.fbl = ASCII 27,ASCII 72 --Desabilita negrita
   LET fnt.twd = ASCII 27,ASCII 87,"1" --Habilita doble ancho
   LET fnt.fwd = ASCII 27,ASCII 87,"0" --deshabilita doble ancho
   LET fnt.t88 = ASCII 27,ASCII 48 --1/8 en altura
   LET fnt.t66 = ASCII 27,ASCII 50 --Regresa a 1/6  de altura(default)
   LET fnt.p12 = ASCII 27,ASCII 77 --pica condensada
   LET fnt.p10 = ASCII 27,ASCII 80 --desabilita pica
   LET fnt.tda = ASCII 27,ASCII 119,"1" --habilita doble alto
   LET fnt.fda = ASCII 27,ASCII 119,"0" --desabilita doble alto
   LET fnt.ini = ASCII 27,"@" --inicializa impresora y toma el default
   LET fnt.srp = ASCII 27,ASCII 56 --desabilita sensor de papel
  OTHERWISE
   -- Ningun codigo 
   INITIALIZE fnt.* TO NULL 
 END CASE

 RETURN fnt.*
END FUNCTION

-- Subrutina para enviar un reporte a un destino 

FUNCTION librut001_sendreport(filename,pipeline,title,flags)
 DEFINE wcommand,pipeline,filename,title,wdocto,flags,ft STRING,
        result                                           SMALLINT 

 -- Tipos de dispositivo (pipeline)
 -- local  = impresora conectada a puerto local donde se ejecuta el aplicativo
 -- screen = pantalla
 -- *@*    = buzon de correo

 -- Reemplazando enes
 LET wcommand = "../../cmd/repene ",filename CLIPPED
 RUN wcommand

 -- Seleccionando dispositivo
 CASE
  WHEN (pipeline="screen")
   CALL librut001_visor(filename,title)
  WHEN (pipeline="local")
   LET result =  librut001_printdos(filename)
  WHEN (pipeline="pdf")
   LET wcommand = "../../cmd/txt2pdf ",filename CLIPPED," ",flags CLIPPED 

   --Ejecutando comando
   RUN wcommand
   LET filename = librut01_basename(filename,"pdf")
   LET wdocto   = "https://",FGL_GETENV("IPWEB") CLIPPED,"/",
                  FGL_GETENV("LOGNAME") CLIPPED,"/",filename CLIPPED
   IF NOT winshellexec(wdocto) THEN
      CALL fgl_winmessage(
      " Atencion: ",
      " Problemas con el acrobar reader o con el archivo. \n Consulta con el administrador del sistema.",
      "stop")
   END IF
 END CASE

 -- Ejecutando commando
 RUN wcommand
END FUNCTION

-- Subrutina para enviar un arhivo a un visor de pantalla 

FUNCTION librut001_visor(fn,title)
 DEFINE visordata,fn,title STRING

 -- Asignando titulo
 LET title = " Visor de Reportes - ",title CLIPPED

 -- Llenando visor de datos
 LET visordata = librut001_readfile(fn,1)

 -- Abriendo la ventana del visor
 OPEN WINDOW visor WITH FORM "formvisor"
  ATTRIBUTE(TEXT=title)

  -- Desplegando datos en el visor
  DISPLAY BY NAME visordata
  MENU ""
   ON ACTION regresar
    EXIT MENU
  END MENU

 -- Cerrando ventana del visor
 CLOSE WINDOW visor
END FUNCTION

-- Subrutina para leer un archivo 

FUNCTION librut001_readfile(fn,ope)
 DEFINE fn  STRING
 DEFINE txt STRING
 DEFINE ln  STRING
 DEFINE ch  base.Channel
 DEFINE ope SMALLINT
 WHENEVER ERROR CONTINUE 
 LET ch=base.Channel.create()
 CALL ch.openfile(fn,"r")
 CALL ch.setDelimiter("")
 WHENEVER ERROR STOP 
 CASE (ope)
  WHEN 1
   WHILE ch.read(ln)
    IF txt IS NULL THEN
      IF ln IS NULL THEN
         LET txt = "\n"
      ELSE
         LET txt = ln
      END IF
    ELSE
      IF ln IS NULL THEN
         LET txt = txt || "\n"
      ELSE
         LET txt = txt || "\n" || ln
      END IF
    END IF
   END WHILE  
  WHEN 2
   WHILE ch.read(ln) 
    LET txt = ln
   END WHILE
 END CASE

 CALL ch.close()
 RETURN txt
END FUNCTION

-- Subrutina para calcular el area

FUNCTION librut001_area(xancho,ylargo)
 DEFINE xancho,ylargo,area  DEC(8,2)

 -- Calculando
 LET area = (xancho*ylargo)
 IF area IS NULL THEN LET area = 0 END IF 
 RETURN area
END FUNCTION

-- Subrutina para convertir los numeros a letras 

FUNCTION librut001_numtolet(valor)
DEFINE f_tentxt ARRAY[8] OF RECORD
         tentxt         CHAR(10)
       END RECORD,
       f_unittxt ARRAY[20] OF RECORD
         unittxt        CHAR(11)
       END RECORD,
       f_gruptxt ARRAY[3] OF RECORD
         gruptxt        CHAR(7)
       END RECORD,
       f_letras,letras  CHAR(100),
       i,j,esp SMALLINT,
       f_cox            CHAR(20),
       f_deci           DECIMAL(3,0),
       valor            DECIMAL(11,2),
       f_numero, f_stp, f_divisor, f_grup, f_unit, f_ten, f_kk  INTEGER,
       f_deci2          CHAR(1)
LET f_tentxt[1].tentxt = "VEINTI"
LET f_tentxt[2].tentxt = "TREINTA "
LET f_tentxt[3].tentxt = "CUARENTA "
LET f_tentxt[4].tentxt = "CINCUENTA "
LET f_tentxt[5].tentxt = "SESENTA "
LET f_tentxt[6].tentxt = "SETENTA "
LET f_tentxt[7].tentxt = "OCHENTA "
LET f_tentxt[8].tentxt = "NOVENTA "

LET f_unittxt[1].unittxt = "UN "
LET f_unittxt[2].unittxt = "DOS "
LET f_unittxt[3].unittxt = "TRES "
LET f_unittxt[4].unittxt = "CUATRO "
LET f_unittxt[5].unittxt = "CINCO "
LET f_unittxt[6].unittxt = "SEIS "
LET f_unittxt[7].unittxt = "SIETE "
LET f_unittxt[8].unittxt = "OCHO "
LET f_unittxt[9].unittxt = "NUEVE "
LET f_unittxt[10].unittxt = "DIEZ "
LET f_unittxt[11].unittxt = "ONCE "
LET f_unittxt[12].unittxt = "DOCE "
LET f_unittxt[13].unittxt = "TRECE "
LET f_unittxt[14].unittxt = "CATORCE "
LET f_unittxt[15].unittxt = "QUINCE "
LET f_unittxt[16].unittxt = "DIECISEIS "
LET f_unittxt[17].unittxt = "DIECISIETE "
LET f_unittxt[18].unittxt = "DIECIOCHO "
LET f_unittxt[19].unittxt = "DIECINUEVE "
LET f_unittxt[20].unittxt = "VEINTE "

LET f_gruptxt[1].gruptxt = "MILLON "
LET f_gruptxt[2].gruptxt = "MIL "
LET f_gruptxt[3].gruptxt = " "


LET f_numero = valor
LET f_letras = NULL
LET f_stp = 1
LET f_divisor = 1000000
LET f_grup = 0
LET f_unit = 0
LET f_ten  = 0

WHILE f_stp <= 3

        LET f_grup = (f_numero / f_divisor)
        LET f_grup = f_grup * f_divisor
        LET f_numero = f_numero - f_grup
        LET f_grup = f_grup / f_divisor

        IF f_grup > 0
           THEN LET f_kk = f_grup
                LET f_ten = 0
           IF f_grup > 99
              THEN LET f_unit = (f_grup / 100)
                   LET f_grup = f_grup - (f_unit * 100)
                   LET f_cox = f_unittxt[f_unit].unittxt CLIPPED,"CIENTOS "
                   IF f_unit = 1
                      THEN IF f_grup = 0
                              THEN LET f_cox = "CIEN "
                              ELSE LET f_cox = "CIENTO "
                           END IF
                      END IF
                   IF f_unit = 5
                      THEN LET f_letras = f_letras CLIPPED," ","QUINIENTOS "
                      ELSE IF f_unit = 7
                              THEN 
                           LET f_letras = f_letras CLIPPED," ","SETECIENTOS "
                              ELSE IF f_unit = 9
                                      THEN
                           LET f_letras = f_letras CLIPPED," ","NOVECIENTOS "
                                      ELSE 
                           LET f_letras = f_letras CLIPPED," ",f_cox
                                   END IF
                           END IF
                   END IF
           END IF
        IF f_grup > 30
           THEN LET f_ten = (f_grup / 10)
                LET f_grup = f_grup - (f_ten * 10)
                LET f_letras = f_letras CLIPPED," ",f_tentxt[f_ten-1].tentxt
                IF ((f_grup > 0) AND (f_letras IS NOT NULL))
                   THEN LET f_letras = f_letras CLIPPED," ","Y "
                END IF
           ELSE IF f_grup > 20
                 THEN LET f_ten = (f_grup / 10)
                      LET f_grup = f_grup - (f_ten * 10)
                      LET f_letras = f_letras CLIPPED," ",f_tentxt[f_ten-1].tentxt
                END IF
         END IF
        IF f_grup > 0
           THEN IF f_ten = 2
           THEN LET f_letras = f_letras CLIPPED,f_unittxt[f_grup].unittxt
           ELSE LET f_letras = f_letras CLIPPED," ",f_unittxt[f_grup].unittxt
                END IF
        END IF
        LET f_letras = f_letras CLIPPED," ",f_gruptxt[f_stp].gruptxt
        IF ((f_stp = 1) AND (f_grup > 1))
           THEN LET f_letras = f_letras CLIPPED,"ES "
        END IF
        END IF

        LET f_stp = f_stp + 1
        LET f_divisor = f_divisor /1000

END WHILE
    LET f_numero = valor
    LET f_deci = (valor - f_numero)*100
    IF valor >= 2
       THEN IF f_deci = 0
             THEN LET f_cox = " EXACTOS"
             ELSE LET f_cox = " CON ",f_deci,"/100"
            END IF
            IF f_deci > 0 AND f_deci < 9
              THEN LET f_deci2 = f_deci
                   LET f_cox = " CON 0",f_deci2,"/100" 
            END IF 

            LET f_letras = "*",f_letras CLIPPED," ",f_cox CLIPPED," *"
       ELSE IF valor < 1
             THEN IF f_deci > 1
                   THEN LET f_letras = "* ",f_deci," CENTAVOS *"
                   ELSE LET f_letras = "* ",f_deci," CENTAVO *"
                  END IF
            ELSE IF f_deci = 0
                  THEN LET f_cox = " EXACTO"
                  ELSE LET f_cox = " CON ",f_deci,"/100"
                  END IF

                  IF f_deci > 0 AND f_deci < 9
                     THEN LET f_deci2 = f_deci
                          LET f_cox = " CON 0",f_deci2,"/100" 
                  END IF 

                  LET f_letras="* ",f_letras CLIPPED," ",f_cox CLIPPED," *"

            END IF
    END IF

    LET esp  = 0
    LET j    = 1
    FOR i = 1 TO LENGTH(f_letras)
     IF (f_letras[i,i] = " ") THEN
        LET esp = (esp+1)
     ELSE
        LET esp = 0 
     END IF

     IF (esp<=1) THEN
        LET letras[j,j] = f_letras[i,i] 
        LET j = (j+1)
     END IF
    END FOR
    RETURN letras  
END FUNCTION

{ Subrutina para quitarle la extension a un nombre de archivo y ponerle
  otra }

FUNCTION librut01_basename(filename,extension)
 DEFINE filename,extension,newfile CHAR(200),
        i                          SMALLINT

 -- Asignando nueva extension
 LET newfile = NULL
 FOR i = 1 TO LENGTH(filename)
  IF (filename[i,i]=".") THEN
     EXIT FOR
  END IF
  LET newfile = newfile CLIPPED,filename[i,i]
 END FOR
 LET filename = newfile CLIPPED,".",extension CLIPPED

 -- Quitando directorio base
 LET newfile = NULL
 FOR i = LENGTH(filename) TO 1 STEP -1
  IF (filename[i,i]="/") THEN
     EXIT FOR
  END IF
  LET newfile = newfile CLIPPED,filename[i,i]
 END FOR
 LET filename = newfile

 -- Armando nuevo nombre
 LET newfile = NULL
 FOR i = LENGTH(filename) TO 1 STEP -1
  LET newfile = newfile CLIPPED,filename[i,i]
 END FOR

 RETURN newfile
END FUNCTION

{ Subrutina para reemplazar una parte de una cadena de caracteres }

FUNCTION librut001_replace(hilera,pattern,replace,ntimes)
 DEFINE buf                    base.StringBuffer,
        ntimes                 SMALLINT,
        hilera,replace,pattern STRING

 -- Creando buffer
 LET buf = base.StringBuffer.create()

 -- Agregando hilera al buffer
 CALL buf.append(hilera)

 -- Reemplazando string buscado x nuevo string en la hilera x n veces
 CALL buf.replace(pattern,replace,ntimes)

 -- Returnando el string
 RETURN buf.toString()
END FUNCTION

{ Subrutina para centrar titulos }

FUNCTION librut001_centrado(wmsg,tl)
 DEFINE wmsg   CHAR(80),
        tl,col SMALLINT

 -- Centrando
 LET col = ((tl-LENGTH(wmsg))/2)
 IF (col<=0) THEN
    LET col = 1
 END IF
 RETURN col
END FUNCTION

-- Subrutina para transferir un archivo del front-end al server

FUNCTION librut001_getfile(fs,ft)
 DEFINE fs,ft STRING
 WHENEVER ERROR CONTINUE 
 -- Obteniendo el archivo origen
 CALL fgl_getfile(fs,ft)
 WHENEVER ERROR STOP 
END FUNCTION 

-- Subrutina para transferir un archivo del server al front-end 

FUNCTION librut001_putfile(fs,ft)
 DEFINE fs,ft STRING

 -- Obteniendo el archivo origen
 CALL fgl_putfile(fs,ft)
END FUNCTION

-- Subrutina para leer el peso de bascula 

FUNCTION librut001_leerbascula(fn,fs,ft,spd)
 DEFINE fn,fs,ft   STRING,
        pesochr    STRING,
        spd        CHAR(1),
        peso       DEC(9,2),
        result     INT

 -- Ejecutando comando para leer bascula
 LET result=winexecwait(fn)
 IF NOT result THEN
    CALL fgl_winmessage(
    " Atencion: ",
    " Programa para leer la bascula no encontrado. \n"||
    " Consulta con el administrador del sistema.",
    "stop")

    RETURN 0,0
 ELSE
    -- Obteniendo peso del archivo
    CALL librut001_getfile(fs,ft)
    LET pesochr = librut001_readfile(ft,2)

    -- Depurando peso, eliminando caracteres
    LET pesochr = librut001_replace(pesochr,"G"," ",10)
    LET peso    = pesochr 
    IF peso IS NULL THEN 
       LET peso = 0
    END IF 

    RETURN 1,peso 
 END IF
END FUNCTION 

-- Subrutina para imprimir un archivo de impresion al puerto paralelo
-- utilizando el modo DOS 

FUNCTION librut001_printdos(fs)
 DEFINE fn,fs,ft   STRING,
        result     INT

 -- Definiendo archivo destino
 LET ft = "C:\\SolTech\\LeerBas\\ReporteTexto"

 -- Copiando archivo del server al front-end 
 CALL librut001_putfile(fs,ft)

 -- Ejecutando comando para imprimir el archivo 
 LET fn = "dosprint.bat"  
 LET result=winexec(fn)
 IF NOT result THEN
    CALL fgl_winmessage(
    " Atencion: ",
    " Programa para imprimir no disponible. \n"||
    " Consulta con el administrador del sistema.",
    "stop")

    RETURN 0
 ELSE 
    RETURN 1
 END IF
END FUNCTION 
