IMPORT os
DEFINE xlwb INTEGER
DEFINE xlapp INTEGER
DEFINE xpaste INTEGER

################################################################################
# Nombre   : tmpFormato
# Recibe   :
# Devuelve : flag_formato
# Descripcion : Verifica que no exista la tabla de formato para crearla
################################################################################
FUNCTION tmpFormato()
DEFINE flag_formato INTEGER

   -- verificacion de existencia de tabla
   IF existe_tabla("tmpFormato") = TRUE THEN
      DROP TABLE tmpFormato
   END IF

   -- tabla temporal para almacenar formatos que se realizaran al reporte
   CREATE TEMP TABLE tmpFormato
   (
      form_cor   SERIAL,
      form_pos   VARCHAR(15),
      form_nom   VARCHAR(50)
   ) WITH NO LOG
   IF SQLCA.SQLCODE < 0 THEN
      LET flag_formato = FALSE
      CALL box_valdato("No se dara formato al reporte")
   ELSE
      LET flag_formato = TRUE
   END IF

   RETURN flag_formato
END FUNCTION



################################################################################
# Nombre   : freeMemory
# Recibe   :
# Devuelve :
# Descripcion : funcion con la liberamos la memoria del ordenador
################################################################################
FUNCTION freeMemory()
DEFINE res INTEGER

   IF xpaste <> -1 THEN
      CALL ui.Interface.frontCall("WinCOM","ReleaseInstance", [xpaste], [res] )
   END IF

   IF xlwb <> -1 THEN
      CALL ui.Interface.frontCall("WinCOM","ReleaseInstance", [xlwb], [res] )
   END IF

   IF xlapp <> -1 THEN
      CALL ui.Interface.frontCall("WinCOM","ReleaseInstance", [xlapp], [res] )
   END IF

END FUNCTION


################################################################################
# Nombre   : checkErrorwcom
# Recibe   : res, lin
# Devuelve :
# Descripcion : funcion con la que determinamos si al momento de enviar datos a
#               excel ocurre algun error
################################################################################
FUNCTION checkErrorwcom(res, lin)
DEFINE res INTEGER
DEFINE lin INTEGER
DEFINE mess STRING

   IF res = -1 THEN
      DISPLAY "COM Error for call at line:", lin
      CALL ui.Interface.frontCall("WinCOM","GetError",[],[mess])
      DISPLAY "mensaje de error",  mess
      CALL freeMemory()
      DISPLAY "Exit with COM Error."
      EXIT PROGRAM (-1)
   END IF

END FUNCTION


################################################################################
# Nombre   : ctrlApp
# Recibe   : lcomando
# Devuelve : lstatus
# Descripcion : Funcion para manejar los comando que se ejecutan en la aplicacion
################################################################################
FUNCTION ctrlApp(lcomando)
DEFINE lcomando STRING
DEFINE result INTEGER

   CASE lcomando
      WHEN "xlsOpen" -- Abre Excel y activa una hoja
         -- inicializacion de variables
         LET xlapp = -1
         LET xlwb = -1

         -- primero creamos una aplicacion de excel
         CALL ui.Interface.frontCall("WINCOM", "CreateInstance", ["Excel.Application"], [xlapp])
         CALL CheckErrorwcom(xlapp, __LINE__)


         -- despues agregamos un libro de trabajo a la aplicacion excel
         CALL ui.interface.frontCall("WINCOM", "CallMethod", [xlapp, "WorkBooks.Add"], [xlwb])
         CALL CheckErrorwcom(xlwb, __LINE__)

         -- despues lo configuramos para que se haga visible
         CALL ui.interface.frontCall("WINCOM", "SetProperty", [xlapp, "Visible", true], [result])
         CALL CheckErrorwcom(result, __LINE__)

      WHEN "xlsClose" -- Cierra la conexion a Excel y libera la memoria
         -- Llamamos a la funcion que libera la memoria del ordenador despues de que
         -- finalizamos de enviar info a excel
         CALL freeMemory()

      OTHERWISE ERROR "Comando no existe"
         LET result = FALSE
   END CASE
END FUNCTION


################################################################################
# Nombre   : ctrlAppInst
# Recibe   : lcomando
# Devuelve : xlapp
# Descripcion : Crea una instancia en Excel y devuelve el id de instancia
################################################################################
FUNCTION ctrlAppInst(lcomando)
DEFINE lcomando STRING
DEFINE result INTEGER

   CASE lcomando
      WHEN "xlsOpen" -- Abre Excel y activa una hoja
         -- inicializacion de variables
         LET xlapp = -1
         LET xlwb = -1
         LET xpaste = -1

         -- primero creamos una aplicacion de excel
         CALL ui.Interface.frontCall("WINCOM", "CreateInstance", ["Excel.Application"], [xlapp])
         CALL CheckErrorwcom(xlapp, __LINE__)

         -- despues agregamos un libro de trabajo a la aplicacion excel
         CALL ui.interface.frontCall("WINCOM", "CallMethod", [xlapp, "WorkBooks.Add"], [xlwb])
         CALL CheckErrorwcom(xlwb, __LINE__)

         -- despues lo configuramos para que se haga visible
         CALL ui.interface.frontCall("WINCOM", "SetProperty", [xlapp, "Visible", true], [result])
         CALL CheckErrorwcom(result, __LINE__)

      WHEN "xlsClose" -- Cierra la conexion a Excel y libera la memoria
         -- Llamamos a la funcion que libera la memoria del ordenador despues de que
         -- finalizamos de enviar info a excel
         CALL freeMemory()

      OTHERWISE ERROR "Comando no existe"
         LET xlapp =0
   END CASE

   RETURN xlapp
END FUNCTION


################################################################################
# Nombre   : DefineHoja
# Recibe   : NumHojas
# Devuelve :
# Descripcion : Define el No de Hojas del Libro en Excel
#  -  Recibe el no de hojas que debe tener el Libro
#  -  Define si debe agregar o eliminar hojas
################################################################################
FUNCTION DefineHoja(NumHojas,NumHojasAct)
DEFINE NumHojas      SMALLINT -- Numero Total de Hojas Deseadas
DEFINE NumHojasAct   SMALLINT -- Numero Total de Hojas Actualmente
DEFINE HojasAct      SMALLINT -- Hoja Actual
DEFINE command       STRING
DEFINE PosCnt        SMALLINT -- Posicion de Contador

   -- Siempre al inicio tendra 3 (Hoja1,Hoja2 y Hoja3)
   --LET NumHojasAct = 3

   -- Si las Hojas Deseadas son diferentes a las que estan Actualmente
   IF NumHojas <> NumHojasAct THEN
      IF NumHojas > NumHojasAct THEN
         FOR PosCnt = (NumHojasAct+1) TO NumHojas
            -- Agrega una Hoja
            CALL ui.interface.frontCall("WINCOM", "CallMethod",[xlapp, "Sheets.Add"], [xlwb])
            CALL CheckErrorwcom(xlwb, __LINE__)
         END FOR
         -- Cambia Nombres de Hoja
         --CALL CambiaNomHoja(NumHojas)
      ELSE
         -- Si las Hojas Deseadas es menor que las que estan Actualmente ELIMINA
         IF NumHojas < NumHojasAct THEN
            FOR PosCnt = NumHojasAct TO (NumHojas+1) STEP -1
               -- Borra una Hoja
               LET command = "Sheets("||PosCnt||").Delete"
               CALL ui.interface.frontCall("WINCOM", "CallMethod",[xlapp, command],[xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
            END FOR
         END IF
      END IF
   END IF
END FUNCTION


################################################################################
# Nombre   : SeleccionaHoja
# Recibe   : NombreHojas
# Devuelve :
# Descripcion : Se posiciona en una hoja especifica
################################################################################
FUNCTION SeleccionaHoja(NombreHoja)
DEFINE NombreHoja    STRING   -- Nombre de Hoja que se desea seleccionar
DEFINE NumHojasAct   SMALLINT -- Numero de Hoja Seleccionada
DEFINE Hojas         SMALLINT -- Hoja Actual
DEFINE command       STRING
DEFINE Valor         STRING
DEFINE PosCnt        SMALLINT -- Posicion de Contador
DEFINE HojaTemporal  CHAR(3)
DEFINE NomOriginal   VARCHAR(255)
DEFINE NomTemporal   VARCHAR(55)
DEFINE NomOrdenado   VARCHAR(255)
DEFINE result        SMALLINT

   -- Cambia el Nombre de Hoja
   LET Valor = NomTemporal
   LET command = 'Sheets("'||NombreHoja||'").Select'
   CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,command],[xlwb])
   CALL CheckErrorwcom(xlwb, __LINE__)
   CALL sendData("A1",".")

END FUNCTION

################################################################################
# Nombre   : RenombraHoja
# Recibe   : NomHojaOri,NomHojaNue
# Devuelve :
# Descripcion : Cambia nombre de hojas
################################################################################
FUNCTION RenombraHoja(NomHojaOri,NomHojaNue)
DEFINE NomHojaOri    STRING
DEFINE NomHojaNue    STRING
DEFINE command       STRING
DEFINE result        SMALLINT

   -- Cambia el Nombre de Hoja
   LET command = 'Sheets("'||NomHojaOri||'").Name'
   CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp,command,NomHOjaNue],[result])
   CALL CheckErrorwcom(xlwb, __LINE__)

END FUNCTION


################################################################################
# Nombre   : CambiaNomHoja
# Recibe   : NumHojas
# Devuelve :
# Descripcion : Cambia nombre de hojas insertadas
################################################################################
FUNCTION CambiaNomHoja(NomOriginal,NomTemporal)
DEFINE NumHojas      SMALLINT -- Numero Total de Hojas Deseadas
DEFINE NumHojasAct   SMALLINT -- Numero de Hoja Seleccionada
DEFINE Hojas         SMALLINT -- Hoja Actual
DEFINE command       STRING
DEFINE Valor         STRING
DEFINE PosCnt        SMALLINT -- Posicion de Contador
DEFINE HojaTemporal  CHAR(3)
DEFINE NomOriginal   VARCHAR(255)
DEFINE NomTemporal   VARCHAR(55)
DEFINE NomOrdenado   VARCHAR(255)
DEFINE result        SMALLINT

      LET Valor = NomTemporal
      LET command = 'Sheets("'||NomOriginal||'").Name'
      CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp,command,Valor],[result])
      CALL CheckErrorwcom(xlwb, __LINE__)
	RETURN
   -- verificacion de existencia de tabla
   IF existe_tabla("tmpCambiaNom") = TRUE THEN
      DROP TABLE tmpCambiaNom
   END IF

   -- tabla temporal para almacenar nombres de Hojas
   CREATE TEMP TABLE tmpCambiaNom
   (
      nombre_original   VARCHAR(255),
      nombre_temporal   VARCHAR(255),
      nombre_ordenado   VARCHAR(255)
   ) WITH NO LOG

   LET Hojas = 1
   -- Ciclo para definir nombres de desde la ultima a la 4ta hoja
   FOR PosCnt = NumHojas TO 4 STEP-1
{	
      SELECT a.des_desc_ct
      INTO HojaTemporal
      FROM sd_des a
      WHERE a.des_tipo = 92
      AND a.des_cod = Hojas
}
      LET NomOriginal = 'Hoja'||PosCnt
      LET NomTemporal = HojaTemporal
      LET NomOrdenado = 'Hoja'||Hojas

      -- Insertamos los nombres en la tabla temporal
      INSERT INTO tmpCambiaNom VALUES(NomOriginal,NomTemporal,NomOrdenado)
      LET Hojas = Hojas + 1
   END FOR

   -- Ciclo para definir nombres de las hojas predeterminadas
   FOR PosCnt = 1 TO 3
      SELECT a.des_desc_ct
      INTO HojaTemporal
      FROM sd_des a
      WHERE a.des_tipo = 92
      AND a.des_cod = Hojas

      LET NomOriginal = 'Hoja'||PosCnt
      LET NomTemporal = HojaTemporal
      LET NomOrdenado = 'Hoja'||Hojas

      -- Insertamos los nombres en la tabla temporal
      INSERT INTO tmpCambiaNom VALUES(NomOriginal,NomTemporal,NomOrdenado)
      LET Hojas = Hojas + 1
   END FOR

   -- Cursor que recorre la temporal y renombra todas las hojas
   DECLARE cur_Nomtemporal CURSOR FOR
      SELECT a.nombre_original,
            a.nombre_temporal
      FROM tmpCambiaNom a
   FOREACH cur_Nomtemporal INTO NomOriginal,
                              NomTemporal

      -- Cambia el Nombre de Hoja
      LET Valor = NomTemporal
      LET command = 'Sheets("'||NomOriginal||'").Name'
      CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp,command,Valor],[result])
      CALL CheckErrorwcom(xlwb, __LINE__)
   END FOREACH

   -- Cursor que recorre la temporal y renombra todas las hojas
   DECLARE cur_NomOrdenado CURSOR FOR
      SELECT a.nombre_temporal,
            a.nombre_ordenado
      FROM tmpCambiaNom a
   FOREACH cur_NomOrdenado INTO NomTemporal,
                              NomOrdenado

      -- Cambia el Nombre de Hoja
      LET Valor = NomOrdenado
      LET command = 'Sheets("'||NomTemporal||'").Name'
      CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp,command,Valor],[result])
      CALL CheckErrorwcom(xlwb, __LINE__)
   END FOREACH
END FUNCTION


################################################################################
# Nombre   : sendData
# Recibe   : lpos, ldata
# Devuelve :
# Descripcion : Funcion que envia los datos a la aplicaciòn
################################################################################
FUNCTION sendData(lpos, ldata)
DEFINE lpos    STRING   -- Posicion o celda a donde se desea enviar la data
DEFINE ldata   STRING   -- Data que se desea escribir
DEFINE vRango  STRING
DEFINE result  INTEGER

   -- armamos cadena para enviar a escribir a excel
   LET vRango =  "Range("||'"'||lpos||'"'||").Select"
   CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])
   CALL CheckErrorwcom(xlwb, __LINE__)
  --CALL ui.interface.frontCall("WinCOM", "SetProperty", [xlapp, "Visible", true], [result])
   --LET vRango =  'activesheet.Range("',lpos,'").Value'
   --CALL ui.Interface.frontCall("WinCOM", "SetProperty", [xlwb, vRango, ldata],[result])
   --CALL ui.Interface.frontCall("WinCOM", "SetProperty", [xlwb, 'activesheet.Range("A1").Value', ldata],[result])
   --CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, vRango, ldata],[result])
   CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, "ActiveCell.FormulaR1C1", ldata],[result])
   --CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, "ActiveCell.FormulaF1C1", ldata],[result])
   CALL CheckErrorwcom(result, __LINE__)
END FUNCTION


################################################################################
# Nombre   : sendInfo
# Recibe   : lpos, ldata
# Devuelve :
# Descripcion : Funcion que envia los datos a la aplicaciòn
################################################################################
FUNCTION sendInfo(lpos, ldata)
DEFINE lpos    STRING   -- Posicion o celda a donde se desea enviar la data
DEFINE ldata   STRING   -- Data que se desea escribir
DEFINE vRango  STRING
DEFINE result  INTEGER

   LET vRango = 'activesheet.Range("',lpos CLIPPED,'").Select'
   CALL ui.Interface.frontCall("standard","cbclear",[], [result])
   CALL ui.Interface.frontCall("standard","cbset",[ldata], [result])
   CALL ui.Interface.frontCall("WinCOM", "CallMethod", [xlwb,vRango], [xpaste])
   CALL wincom_error(xpaste, __LINE__)
   CALL ui.Interface.frontCall("WinCOM", "CallMethod", [xpaste,"activesheet.Paste"], [result])
   CALL wincom_error(result, __LINE__)

   CALL excel_freeMem()

END FUNCTION


################################################################################
# Nombre   : writeCell
# Recibe   : lpos, ldata
# Devuelve :
# Descripcion : Funcion que envia los datos a la aplicaciòn
################################################################################
FUNCTION writeCell(sheet,lpos,ldata)
DEFINE sheet   STRING   -- Hoja de Excel donde es desea escribir
DEFINE lpos    STRING   -- Posicion o celda a donde se desea escribir
DEFINE ldata   STRING   -- Data que se desea escribir
DEFINE command STRING
DEFINE result  INTEGER

   IF ldata IS NOT NULL THEN
      -- armamos cadena para enviar a escribir a excel
      LET command = 'Worksheets("'||Sheet||'").Cells('||lpos||').Value'
      CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp,command,ldata],[result])
      CALL CheckErrorwcom(result, __LINE__)
   END IF

END FUNCTION


################################################################################
# Nombre   :prepFormat
# Recibe   : lform_pos, lform_nom
# Devuelve :
# Descripcion : Funcion que inserta en tabla temporal los formatos requeridos para
#               el reporte, para luego aplicar el formato al reporte en un solo paso
#               al finalizar el reporte. Se realiza de esta forma porque en las pruebas
#               demostro mejor rendimiento que aplicar formato por cada linea
################################################################################
FUNCTION prepFormat(lform_pos, lform_nom)
DEFINE lform_pos VARCHAR(20) -- Posicion o celda a donde se desea aplicar un formato
DEFINE lform_nom VARCHAR(255) -- Data que se desea escribir

   WHENEVER ERROR CONTINUE
      INSERT INTO tmpFormato VALUES(0,lform_pos, lform_nom)
   WHENEVER ERROR STOP
   IF SQLCA.SQLCODE <> 0 THEN
      ERROR "Error al insertar a temporal ", sqlca.sqlcode
   END IF
END FUNCTION


################################################################################
# Nombre   : repProcesaFormato
# Recibe   :
# Devuelve :
# Descripcion : Leer a temporal los formatos y llamar a repFormat para ejecutar
#               los formatos
################################################################################
FUNCTION repProcesaFormato()
DEFINE gr_tmpFormato RECORD
   form_cor   INTEGER,
   form_pos   VARCHAR(15),
   form_nom   VARCHAR(50)
END RECORD

   DECLARE ccursor CURSOR FOR
      SELECT tmpFormato.* FROM tmpFormato
      ORDER BY 1
   FOREACH ccursor INTO gr_tmpFormato.*
      CALL repFormat(gr_tmpFormato.form_pos, gr_tmpFormato.form_nom)
   END FOREACH
   FREE ccursor
END FUNCTION
################################################################################
# Nombre   : repFormat
# Recibe   : lpos, lformato
# Devuelve : val2
# Descripcion : Funcion que envia los datos a la aplicaciòn
################################################################################
FUNCTION repFormat(lpos, lformato)
DEFINE lpos       STRING -- Posicion o celda a donde se desea enviar la data
DEFINE categ      STRING
DEFINE grupo      STRING
DEFINE vRango     STRING
DEFINE lformato   STRING -- ID del formato que se desea aplicar
DEFINE param      STRING -- Objetos que tienen mas de un parametro

   LET categ = lformato.subString(1,3)
   LET grupo = lformato.subString(4,lformato.getLength())

   -- pocisiciones decimales: dec + numero de posiciones decimales
   -- formato texto: txt + formato (negrita, cursiva , subrayado, etc)
   -- tamaño de text: tam + numero de tamaño de letra
   -- color de font:  clf + id de color
   -- color de relleno: clr + id de color
   -- tamaño de columna: col + numero de tamaño de columna
   -- subrayado de celda: sub + estilo de linea
   -- Ajustar Texto: sub + estilo de linea
   -- Alinea Texto en la parte inferior de la celda
   -- Combinar celdas
   -- Congelar filas en la parte superior de la hoja: FRE

   CASE
      -- Formato de font: Negrita, Cursiva y Subrayado
      WHEN categ.toUpperCase() = "TXT"
         CASE
            WHEN grupo.toUpperCase() = "NEG"
               -- se selecciona el rango
               LET vRango =  "Range("||'"'||lpos||'"'||").Select"
               CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])

              -- se aplica la propiedad
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Font.Bold',true], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
            WHEN grupo.toUpperCase() = "CUR"
               -- se selecciona el rango
               LET vRango =  "Range("||'"'||lpos||'"'||").Select"
               CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])

               -- se aplica la propiedad
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Font.Italic',true], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
            WHEN grupo.toUpperCase() = "SUB"
               -- se selecciona el rango
               LET vRango =  "Range("||'"'||lpos||'"'||").Select"
               CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])

               -- se aplica la propiedad
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Font.Underline',2], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
            WHEN grupo.toUpperCase() = "IZQ" -- Alineado a la Izquierda
               -- se selecciona el rango
               LET vRango =  "Range("||'"'||lpos||'"'||").Select"
               CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])

               -- se aplica la propiedad
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.HorizontalAlignment',-4131], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
            WHEN grupo.toUpperCase() = "CEN" -- Alineado al Centro
               -- se selecciona el rango
               LET vRango =  "Range("||'"'||lpos||'"'||").Select"
               CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])

               -- se aplica la propiedad
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.HorizontalAlignment',-4108], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)

            WHEN grupo.toUpperCase() = "DER" -- Alineado a la Derecha
               -- se selecciona el rango
               LET vRango =  "Range("||'"'||lpos||'"'||").Select"
               CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])

               -- se aplica la propiedad
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.HorizontalAlignment',-4152], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)

            WHEN grupo.toUpperCase() = "INF" -- Alineado en la parte inferior de la celda
               -- se selecciona el rango
               LET vRango =  "Range("||'"'||lpos||'"'||").Select"
               CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])

               -- se aplica la propiedad
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.VerticalAlignment',-4107], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)

            WHEN grupo.toUpperCase() = "COM" -- Combinar Celdas
               -- se selecciona el rango
               LET vRango =  "Range("||'"'||lpos||'"'||").Select"
               CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])

               -- se aplica la propiedad
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.HorizontalAlignment',-4108], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.MergeCells',TRUE], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)

            WHEN grupo.toUpperCase() = "TXT" -- Formato de celda Texto
               -- se selecciona el rango
               LET vRango =  "Range("||'"'||lpos||'"'||").Select"
               CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])

               -- se aplica la propiedad
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.NumberFormat','@'], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)

            WHEN grupo.toUpperCase() = "AJU"  -- Ajustar Texto en la celda
               LET vRango =  "Range("||'"'||lpos||'"'||").Select"
               CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)

               -- se aplica la propiedad
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, "Selection.WrapText",TRUE], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)

            OTHERWISE
               DISPLAY "Grupo formato de font no implementado: ", grupo
         END CASE

      -- Tamaño de fonts
      WHEN categ.toUpperCase() = "TAM"
         --- Tamaño de letras Modificado por Hector Cuj 25/06/2009 11:52 AM ---
         --- grupo es valor que trae a la par del texto TAM ej. tam12
         -- se selecciona el rango
         LET vRango =  "Range("||'"'||lpos||'"'||").Select"
         CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])

         -- se aplica la propiedad
         CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Font.Size',grupo], [xlwb])
         CALL CheckErrorwcom(xlwb, __LINE__)

      -- Formato de nùmero: Posiciones de decimales (dec3, dec4, dec5, dec6)
      WHEN categ.toUpperCase() = "DEC"
         CASE
            WHEN grupo.toUpperCase() = "6"
               -- se selecciona el rango
               LET vRango =  "Range("||'"'||lpos||'"'||").Select"
               CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])

               -- se aplica la propiedad
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.NumberFormat','#,##0.000000'], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)

            WHEN grupo.toUpperCase() = "5"
               -- se selecciona el rango
               LET vRango =  "Range("||'"'||lpos||'"'||").Select"
               CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])

               -- se aplica la propiedad
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.NumberFormat','#,##0.00000'], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)

            WHEN grupo.toUpperCase() = "4"
               -- se selecciona el rango
               LET vRango =  "Range("||'"'||lpos||'"'||").Select"
               CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])

               -- se aplica la propiedad
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.NumberFormat','#,##0.0000'], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)

            WHEN grupo.toUpperCase() = "3"
               -- se selecciona el rango
               LET vRango =  "Range("||'"'||lpos||'"'||").Select"
               CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])

               -- se aplica la propiedad
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.NumberFormat','#,##0.000'], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)

            WHEN grupo.toUpperCase() = "2"
               -- se selecciona el rango
               LET vRango =  "Range("||'"'||lpos||'"'||").Select"
               CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])

               -- se aplica la propiedad
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.NumberFormat','#,##0.00'], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)

            WHEN grupo.toUpperCase() = "1"
               -- se selecciona el rango
               LET vRango =  "Range("||'"'||lpos||'"'||").Select"
               CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])

               -- se aplica la propiedad
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.NumberFormat','#,##0.0'], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)

            WHEN grupo.toUpperCase() = "0"
               -- se selecciona el rango
               LET vRango =  "Range("||'"'||lpos||'"'||").Select"
               CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])

               -- se aplica la propiedad
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.NumberFormat','0'], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
            WHEN grupo.toUpperCase() = "%"
               -- se selecciona el rango
               LET vRango =  "Range("||'"'||lpos||'"'||").Select"
               CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])

               -- se aplica la propiedad
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.NumberFormat','0.00%'], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)

            OTHERWISE
               DISPLAY "Grupo decimal no implementado: ", grupo
         END CASE
      WHEN categ.toUpperCase() = "FEC"
         CASE
            WHEN grupo.toUpperCase() = "MMM-YY"
               -- se selecciona el rango
               LET vRango =  "Range("||'"'||lpos||'"'||").Select"
               CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])

               -- se aplica la propiedad
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.NumberFormat','mmm-yy'], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)

            WHEN grupo.toUpperCase() = "YYYY-MM-DD HH:MM:SS"
               -- se selecciona el rango
               LET vRango =  "Range("||'"'||lpos||'"'||").Select"
               CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])

               -- se aplica la propiedad
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.NumberFormat','yyyy-mm-dd hh:mm:ss'], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
            OTHERWISE
               DISPLAY "Formato fecha no implementado: ", grupo
         END CASE

      -- Tamaño de fonts
      WHEN categ.toUpperCase() = "TAM"
         --- Tamaño de letras Modificado por Hector Cuj 25/06/2009 11:52 AM ---
         --- grupo es valor que trae a la par del texto TAM ej. tam12
         -- se selecciona el rango
         LET vRango =  "Range("||'"'||lpos||'"'||").Select"
         CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])

         -- se aplica la propiedad
         CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Font.Size',grupo], [xlwb])
         CALL CheckErrorwcom(xlwb, __LINE__)

      -- Formato de color de texto
      WHEN categ.toUpperCase() = "CLF"
         -- blanco = 2
         -- azul = 5
         -- amarillo = 6
         -- naranja = 45
         -- gris = 15
         --- Color de Texto por Hector Cuj 25/06/2009 11:52 AM ---
         --- grupo es valor que trae a la par del texto CLF ej. clf2
         -- se selecciona el rango
         LET vRango =  "Range("||'"'||lpos||'"'||").Select"
         CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])

         -- se aplica la propiedad
         CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Font.ColorIndex',grupo], [xlwb])
         CALL CheckErrorwcom(xlwb, __LINE__)

      -- Formato de color de relleno de la celda
      WHEN categ.toUpperCase() = "CLR"
         -- azul = 5
         -- amarillo = 6
         -- naranja = 45
         -- gris = 15
         --- Color de Rellena de Celda por Hector Cuj 25/06/2009 11:52 AM ---
         --- grupo es valor que trae a la par del texto CLR ej. clr2
         -- se selecciona el rango
         LET vRango =  "Range("||'"'||lpos||'"'||").Select"
         CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])

         -- se aplica la propiedad
         CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Interior.ColorIndex',grupo], [xlwb])
         CALL CheckErrorwcom(xlwb, __LINE__)


      -- Formato de tamano de columna
      WHEN categ.toUpperCase() = "COL"
         --- Ancho de Columna Modificado por Hector Cuj 25/06/2009 9:30 AM ---
         --- se selecciona el rango
         --- grupo es valor que trae a la par del texto COL ej. col30
         LET vRango =  "Range("||'"'||lpos||'"'||").Select"
         CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])

         -- se aplica la propiedad
         CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.ColumnWidth',grupo], [xlwb])
         CALL CheckErrorwcom(xlwb, __LINE__)

      WHEN categ.toUpperCase() = "FIT"
         CASE
            WHEN grupo.toUpperCase() = "LIB" -- Arriba Continua Normal

               CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp, 'Cells.EntireColumn.Autofit'], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)

            WHEN grupo.toUpperCase() = "COL" -- Arriba Continua Normal
               LET vRango =  "Range("||'"'||lpos||'"'||").Select"
               CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])

               CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp, 'Selection.EntireColumn.Autofit'], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)

         END CASE

      -- ocultar columnas
      WHEN categ.toUpperCase() = "HID"
         LET vRango =  "Range("||'"'||lpos||'"'||").Select"
         display "vrango", vRango
         CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])

         -- se aplica la propiedad
         CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.EntireColumn.Hidden',1], [xlwb])
         --CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Delete Shift:=xlToLeft',1], [xlwb])
         CALL CheckErrorwcom(xlwb, __LINE__)

      -- Subrayado de celda
      -- POSICION : Arriba = 8, Abajo = 9, Izquierda = 7, Derecha = 10
      -- LINEA : Continua = 1, Doble = -4119,  Punteada = -4118
      -- GRUESO : Normal = 2, Resaltado = -4138
      WHEN categ.toUpperCase() = "SUB"
         CASE
            WHEN grupo.toUpperCase() = "ARRCONNOR" -- Arriba Continua Normal
               -- se selecciona el rango
               LET vRango =  "Range("||'"'||lpos||'"'||").Select"
               CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])
               -- se aplica la propiedad
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(8).LineStyle',1], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(8).Weight',2], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)

            WHEN grupo.toUpperCase() = "ARRCONGRU" -- Arriba Continua Gruesa
               -- se selecciona el rango
               LET vRango =  "Range("||'"'||lpos||'"'||").Select"
               CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])
               -- se aplica la propiedad
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(8).LineStyle',1], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(8).Weight',-4138], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)

            WHEN grupo.toUpperCase() = "ABACONNOR" -- Abajo Continua Normal
               -- se selecciona el rango
               LET vRango =  "Range("||'"'||lpos||'"'||").Select"
               CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])
               -- se aplica la propiedad
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(9).LineStyle',1], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(9).Weight',2], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)

            WHEN grupo.toUpperCase() = "ABACONGRU" -- Abajo Continua Gruesa
               -- se selecciona el rango
               LET vRango =  "Range("||'"'||lpos||'"'||").Select"
               CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])
               -- se aplica la propiedad
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(9).LineStyle',1], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(9).Weight',-4138], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)

            WHEN grupo.toUpperCase() = "ABADOBNOR" -- Abajo Doble Normal
               -- se selecciona el rango
               LET vRango =  "Range("||'"'||lpos||'"'||").Select"
               CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])
               -- se aplica la propiedad
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(9).LineStyle',-4119], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)

            WHEN grupo.toUpperCase() = "IZQCONNOR" -- Izquierda Continua Normal
               -- se selecciona el rango
               LET vRango =  "Range("||'"'||lpos||'"'||").Select"
               CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])
               -- se aplica la propiedad
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(7).LineStyle',1], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(7).Weight',1], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)

            WHEN grupo.toUpperCase() = "IZQCONGRU" -- Izquierda Continua Gruesa
               -- se selecciona el rango
               LET vRango =  "Range("||'"'||lpos||'"'||").Select"
               CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])
               -- se aplica la propiedad
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(7).LineStyle',1], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(7).Weight',-4138], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)

            WHEN grupo.toUpperCase() = "DERCONNOR" -- Derecha Continua Normal
               -- se selecciona el rango
               LET vRango =  "Range("||'"'||lpos||'"'||").Select"
               CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])
               -- se aplica la propiedad
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(10).LineStyle',1], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(10).Weight',1], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)

            WHEN grupo.toUpperCase() = "DERCONGRU" -- Derecha Continua Gruesa
               -- se selecciona el rango
               LET vRango =  "Range("||'"'||lpos||'"'||").Select"
               CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])
               -- se aplica la propiedad
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(10).LineStyle',1], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(10).Weight',-4138], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)

            WHEN grupo.toUpperCase() = "TODCONNOR" -- Todo Continua Normal
               -- se selecciona el rango
               LET vRango =  "Range("||'"'||lpos||'"'||").Select"
               CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])
               -- se aplica la propiedad
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(7).LineStyle',1], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(7).Weight',2], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(8).LineStyle',1], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(8).Weight',2], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(9).LineStyle',1], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(9).Weight',2], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(10).LineStyle',1], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(10).Weight',2], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)

            WHEN grupo.toUpperCase() = "TODCONGRU" -- Todo Continua Gruesa
               -- se selecciona el rango
               LET vRango =  "Range("||'"'||lpos||'"'||").Select"
               CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])
               -- se aplica la propiedad
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(7).LineStyle',1], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(7).Weight',-4138], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(8).LineStyle',1], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(8).Weight',-4138], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(9).LineStyle',1], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(9).Weight',-4138], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(10).LineStyle',1], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(10).Weight',-4138], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)

            WHEN grupo.toUpperCase() = "RELLENNOR" -- Relleno Normal
               -- se selecciona el rango
               LET vRango =  "Range("||'"'||lpos||'"'||").Select"
               CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])
               -- se aplica la propiedad
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(7).LineStyle',1], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(7).Weight',2], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(8).LineStyle',1], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(8).Weight',2], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(9).LineStyle',1], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(9).Weight',2], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(10).LineStyle',1], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(10).Weight',2], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(11).LineStyle',1], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(11).Weight',2], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(12).LineStyle',1], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(12).Weight',2], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)

            WHEN grupo.toUpperCase() = "RELLENGRU" -- Relleno Grueso
               -- se selecciona el rango
               LET vRango =  "Range("||'"'||lpos||'"'||").Select"
               CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])
               -- se aplica la propiedad
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(7).LineStyle',1], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(7).Weight',-4138], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(8).LineStyle',1], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(9).LineStyle',1], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(9).Weight',-4138], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(10).LineStyle',1], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(10).Weight',-4138], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
               --CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(11).LineStyle',1], [xlwb])
               --CALL CheckErrorwcom(xlwb, __LINE__)
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(11).Weight',-4138], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
               --CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(12).LineStyle',1], [xlwb])
               --CALL CheckErrorwcom(xlwb, __LINE__)
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'Selection.Borders(12).Weight',-4138], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)

            OTHERWISE
               DISPLAY "Grupo subrayado de celda no implementado: ", grupo
         END CASE

      --- Boquea las linea superiores de la hoja segun la celda que recibe
      --- implementada por Hector Cuj
      WHEN categ.toUpperCase() = "FRE"
         -- se selecciona el rango
         LET vRango =  "Range("||'"'||lpos||'"'||").Select"
         CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])

         -- se aplica la propiedad FREEZE
         CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'ActiveWindow.FreezePanes',True], [xlwb])
         CALL CheckErrorwcom(xlwb, __LINE__)

      -- Insertar imagen desde el path recibido
      WHEN categ.toUpperCase() = "IMG"

         LET vRango =  "Range("||'"'||lpos||'"'||").Select"
         CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])

         CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp, 'ActiveSheet.Pictures.Insert("'||grupo||'").Select'], [xlwb])
         CALL CheckErrorwcom(xlwb, __LINE__)

      -- Configuracion de pagina
      WHEN categ.toUpperCase() = "PAG"
         CASE
            WHEN grupo.toUpperCase() = "TAMOFI" -- Pagina Tamaño oficio
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'ActiveSheet.PageSetup.PaperSize',5], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
            WHEN grupo.toUpperCase() = "TAMCAR" -- Pagina Tamaño carta
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'ActiveSheet.PageSetup.PaperSize',1], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
            WHEN grupo.toUpperCase() = "ORIVER" -- Orientacion Vertical
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'ActiveSheet.PageSetup.Orientation',1], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
            WHEN grupo.toUpperCase() = "ORIHOR" -- Orientacion Horizontal
               CALL ui.Interface.frontCall("WINCOM", "SetProperty", [xlapp, 'ActiveSheet.PageSetup.Orientation',2], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)

         END CASE

      -- Borra el contenido de una seleccion
      WHEN categ.toUpperCase() = "DEL"
         CASE
            WHEN grupo.toUpperCase() = "TOD" -- de todas las celdas
               CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp, 'Cells.Select'], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
               CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp, 'Selection.ClearContents'], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)

            WHEN grupo.toUpperCase() = "SEL" -- de una seleccion especifica
               LET vRango =  "Range("||'"'||lpos||'"'||").Select"
               CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])

               CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp, 'Selection.ClearContents'], [xlwb])
               CALL CheckErrorwcom(xlwb, __LINE__)
         END CASE

      -- Hace la seleccion de una hoja
      WHEN categ.toUpperCase() = "HOJ"
         LET vRango =  "Sheets("||'"'||lpos||'"'||").Select"
         CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])
         CALL CheckErrorwcom(xlwb, __LINE__)

      -- Hace la seleccion de celdas
      WHEN categ.toUpperCase() = "SEL"
         LET vRango =  "Range("||'"'||lpos||'"'||").Select"
         CALL ui.Interface.frontCall("WINCOM", "CallMethod", [xlapp,vRango],[xlwb])
         CALL CheckErrorwcom(xlwb, __LINE__)


   OTHERWISE
      DISPLAY "Categoria no implementada: ", categ.toUpperCase()
   END CASE
END FUNCTION

################################################################################
# Nombre   : ctrlAppOpen
# Recibe   : lcomando
# Devuelve : xlapp
# Descripcion : Abre un archivo existente de excel dejando el id en el xlapp
################################################################################
FUNCTION ctrlAppOpen(ffile)
   DEFINE ffile         STRING --- nombre del archivo que se desea abrir
   DEFINE result        INTEGER --- CONTROL de errores
   DEFINE quita, pone   STRING
   DEFINE cmd           STRING

   -- inicializacion de VARIABLES
   LET xlapp = -1
   LET xlwb = -1

   -- primero creamos una aplicacion de excel
   CALL ui.Interface.frontCall("WINCOM", "CreateInstance", ["Excel.Application"], [xlapp])
   CALL CheckErrorwcom(xlapp, __LINE__)


   --- Colocar la cantidad de diagonales que se necesitan en el path
   LET quita = "\\"
   LET pone  = "\\\\"

   --LET ffile = change_char(ffile,quita, pone)
   -----------------------------------------------------------------

   ---- comando para abrir archivo existente
   LET cmd = 'WorkBooks.Open(Filename:="',ffile.TRIM(),'")'
   -- Abrimos el archivo
   CALL ui.interface.frontCall("WINCOM", "CallMethod", [xlapp,cmd], [xlwb])

   --- verificamos errores
   CALL CheckErrorwcom(xlwb, __LINE__)

   -- despues lo configuramos para que se haga visible
   CALL ui.interface.frontCall("WINCOM", "SetProperty", [xlapp, "Visible", TRUE], [result])
   CALL CheckErrorwcom(result, __LINE__)
END FUNCTION


################################################################################
# Nombre   : wincom_error
# Recibe   : res, lin
# Devuelve :
# Descripcion :
################################################################################
FUNCTION wincom_error(res, lin)
DEFINE res INTEGER
DEFINE lin INTEGER
DEFINE mess STRING

  IF res = -1 THEN
    DISPLAY "COM Error for call at line:", lin
    CALL ui.Interface.frontCall("WinCOM","GetError",[],[mess])
    DISPLAY mess
    --let's release the memory on the GDC side
    CALL excel_freeMem()
    DISPLAY "Exit with COM Error."
    EXIT PROGRAM (-1)
  END IF
END FUNCTION


################################################################################
# Nombre   : excel_freeMem
# Recibe   :
# Devuelve :
# Descripcion :
################################################################################
FUNCTION excel_freeMem()
DEFINE res INTEGER

  IF xpaste <> -1 THEN
    CALL ui.Interface.frontCall("WinCOM","ReleaseInstance", [xpaste], [res] )
  END IF

  IF xlwb <> -1 THEN
    CALL ui.Interface.frontCall("WinCOM","ReleaseInstance", [xlwb], [res] )
  END IF
END FUNCTION

################################################################################
# Nombre   : sendInfo
# Recibe   : lpos, ldata
# Devuelve :
# Descripcion : Funcion que envia los datos a la aplicaci\362n
################################################################################
FUNCTION sendInfon(lpos, ldata)
DEFINE lpos    STRING   -- Posicion o celda a donde se desea enviar la data
DEFINE ldata   STRING   -- Data que se desea escribir
DEFINE vRango  STRING
DEFINE result  INTEGER

   LET vRango = 'activesheet.Range("',lpos CLIPPED,'").Select'
   CALL ui.Interface.frontCall("standard","cbclear",[], [result])
   CALL ui.Interface.frontCall("standard","cbset",[ldata], [result])
   CALL ui.Interface.frontCall("WinCOM", "CallMethod", [xlwb,vRango], [xpaste])
   CALL wincom_error(xpaste, __LINE__)
   CALL ui.Interface.frontCall("WinCOM", "CallMethod", [xpaste,"activesheet.Paste"], [result])
   CALL wincom_error(result, __LINE__)

   --CALL excel_freeMem()

END FUNCTION

################################################################################
# Nombre   : recibedata
# Recibe   : lpos, ldata
# Devuelve :
# Descripcion : Funcion que recibe datos por celda
## erickalvarez
################################################################################
FUNCTION recibeData(sheet,lpos,lcelda)
DEFINE lpos    STRING   -- Posicion o celda a donde se desea recibir la data
DEFINE sheet   STRING   -- Hoja de Excel donde es desea escribir
DEFINE ldata   STRING   -- Data que se desea escribir
DEFINE vRango  STRING
DEFINE result  STRING
DEFINE command STRING
DEFINE lcelda  STRING

   LET command = 'activesheet.range('||'"'||lcelda||lpos||'")'||'.Value'
   display command
   CALL ui.Interface.frontCall("WinCOM", "GetProperty", [xlwb, command], [result])
   CALL CheckErrorwcom(result, __LINE__)
   RETURN result
END FUNCTION

FUNCTION existe_tabla(ctabla)
DEFINE  lexiste_tabla SMALLINT,
                        ctabla VARCHAR(50,0),
                        cquery VARCHAR(500,0),
                        val_tempo VARCHAR(15)

        LET lexiste_tabla = FALSE

   LET cquery = "SELECT COUNT(*) FROM ", ctabla CLIPPED

        WHENEVER ERROR CONTINUE
        PREPARE qry_mestant FROM cquery
        WHENEVER ERROR STOP

        IF SQLCA.SQLCODE <> -206 THEN
                LET lexiste_tabla = TRUE
        ELSE
                -- Para que ignore el ERROR que trae del PREPARE
                WHENEVER ERROR CONTINUE
                EXECUTE qry_mestant
                WHENEVER ERROR STOP
        END IF

        RETURN lexiste_tabla
END FUNCTION


################################################################################
# Nombre   : CuentaHoja
# Recibe   : 
# Devuelve :
# Descripcion : Define el No de Hojas del Libro en Excel
################################################################################
FUNCTION CuentaHoja()
DEFINE NumHojas      SMALLINT -- Numero Total de Hojas Deseadas
DEFINE NumHojasAct   SMALLINT -- Numero Total de Hojas Actualmente
DEFINE HojasAct      SMALLINT -- Hoja Actual
DEFINE command       STRING
DEFINE PosCnt        SMALLINT -- Posicion de Contador

   -- Siempre al inicio tendra 3 (Hoja1,Hoja2 y Hoja3)
   --LET NumHojasAct = 3

   CALL ui.interface.frontCall("WINCOM", "CallMethod",[xlapp, "Sheets.Count"], [xlwb])
   CALL CheckErrorwcom(xlwb, __LINE__)

	RETURN xlwb
	
END FUNCTION

