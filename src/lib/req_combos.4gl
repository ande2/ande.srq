
PUBLIC TYPE tComboNum RECORD
    cantidad  SMALLINT,
    datos DYNAMIC ARRAY OF RECORD
        id     SMALLINT,
        nombre STRING
    END RECORD
END RECORD

PUBLIC TYPE tComboTex RECORD
    cantidad  SMALLINT,
    datos DYNAMIC ARRAY OF RECORD
        id     CHAR(1),
        nombre STRING
    END RECORD
END RECORD

PUBLIC FUNCTION cmb_reqestado_init()
DEFINE cmb_datos tComboTex

    LET cmb_datos.cantidad = 8
    LET cmb_datos.datos[1].id = "1"
    LET cmb_datos.datos[1].nombre = "Pendiente de Autorizar"
    LET cmb_datos.datos[2].id = "2"
    LET cmb_datos.datos[2].nombre = "Rechazada"
    LET cmb_datos.datos[3].id = "3"
    LET cmb_datos.datos[3].nombre = "Cancelada"
    LET cmb_datos.datos[4].id = "4"
    LET cmb_datos.datos[4].nombre = "Autorizada"
    LET cmb_datos.datos[5].id = "5"
    LET cmb_datos.datos[5].nombre = "Autorizada GF"
    LET cmb_datos.datos[6].id = "6"
    LET cmb_datos.datos[6].nombre = "Aprobada"
    LET cmb_datos.datos[7].id = "7"
    LET cmb_datos.datos[7].nombre = "En proceso"
    LET cmb_datos.datos[8].id = "8"
    LET cmb_datos.datos[8].nombre = "Finalizada"

RETURN cmb_datos.*
END FUNCTION

PUBLIC FUNCTION cmb_canestado_init()
DEFINE cmb_datos tComboTex

    LET cmb_datos.cantidad = 8
    LET cmb_datos.datos[1].id = "A"
    LET cmb_datos.datos[1].nombre = "Ingresado"
    LET cmb_datos.datos[2].id = "B"
    LET cmb_datos.datos[2].nombre = "En Investigación"
    LET cmb_datos.datos[3].id = "C"
    LET cmb_datos.datos[3].nombre = "En Entrevista"
    LET cmb_datos.datos[4].id = "D"
    LET cmb_datos.datos[4].nombre = "En Evaluación Médica"
    LET cmb_datos.datos[5].id = "E"
    LET cmb_datos.datos[5].nombre = "En Proceso Contratación"
    LET cmb_datos.datos[6].id = "F"
    LET cmb_datos.datos[6].nombre = "Contratado"
    LET cmb_datos.datos[7].id = "G"
    LET cmb_datos.datos[7].nombre = "Rechazado"
    LET cmb_datos.datos[8].id = "H"
    LET cmb_datos.datos[8].nombre = "Rechazado Clinica"


RETURN cmb_datos.*
END FUNCTION

PUBLIC FUNCTION cmb_usuario_init()
DEFINE cmb_datos tComboNum
    LET cmb_datos.cantidad = 8
    LET cmb_datos.datos[1].id = 1
    LET cmb_datos.datos[1].nombre = "Digitador"
    LET cmb_datos.datos[2].id = 2
    LET cmb_datos.datos[2].nombre = "Solicitante"
    LET cmb_datos.datos[3].id = 3
    LET cmb_datos.datos[3].nombre = "Autorizante"
    LET cmb_datos.datos[4].id = 4
    LET cmb_datos.datos[4].nombre = "Autorizante Funcional"
    LET cmb_datos.datos[5].id = 5
    LET cmb_datos.datos[5].nombre = "Gerente de Nómina"
    LET cmb_datos.datos[6].id = 6
    LET cmb_datos.datos[6].nombre = "Superv / Coord de RRHH"
    LET cmb_datos.datos[7].id = 7
    LET cmb_datos.datos[7].nombre = "Analista de RRHH"
    LET cmb_datos.datos[8].id = 8
    LET cmb_datos.datos[8].nombre = "Clínica Médica"
    --LET glob_usuarios.datos[9].id = 9
    --LET glob_usuarios.datos[9].nombre = "Jefe"
RETURN cmb_datos.*
END FUNCTION

PUBLIC FUNCTION cmb_flu_permiso_init()
DEFINE cmb_datos tComboTex

    LET cmb_datos.cantidad = 3
    LET cmb_datos.datos[1].id = "1"
    LET cmb_datos.datos[1].nombre = "Solo Ver"
    LET cmb_datos.datos[2].id = "2"
    LET cmb_datos.datos[2].nombre = "Procesar"
    LET cmb_datos.datos[3].id = "3"
    LET cmb_datos.datos[3].nombre = "Actualizar"

RETURN cmb_datos.*
END FUNCTION