###################################################################################
## Function  : picklist_2  --controla 2 campos para despliegue de informacion
##
## Parameters:  (Title0, Title1, Title2, Field1, Field2, Tabname, Condicion, OrderNum, opci)
##						opci 1 = Desea que se pida texto a buscar 
##							  0 = No pedira texto para seleccion de datos es decir que solamente 
##									hara la busqueda del segundo campo con la condicion recibida
##									
##
## Returnings: Codigo, descripcion, INT_FLAG
##
## Autor:  Ronald Palencia A. (08-mar-2007)
## Comments  : Utiliza arreglo dinamico y busca interectivamente (version Genero)
###################################################################################
FUNCTION picklist_2(Title0, Title1, Title2, Field1, Field2, Tabname, Condicion, OrderNum, opci)
	DEFINE
		Title0 STRING,  -- Title OF FORM
		Title1,  		 -- Titles to display for the Fields
		Title2 STRING,  
		Field1,           -- Field names in the table 
		Field2,
		Condicion STRING,
		Tabname string, -- Table name to query from
		OrderNum STRING , -- Order by 1 or 2, a Number in a range of 1-2
   retorna RECORD  
		codigo VARCHAR(20),
		descripcion VARCHAR(100)
	END RECORD,
   i, aborta  smallint,
   w ui.Window,
   f ui.Form,
	la_busqueda dynamic ARRAY OF RECORD 	# Detalle de temas relacionados
		codigo VARCHAR(20),
		descripcion VARCHAR(100)
   END RECORD,
	comia CHAR(2),
	sqlstmnt STRING,
	refe VARCHAR(100),
	buscar VARCHAR(105),
	opci SMALLINT,  --1 pedira texto a buscar, 0 NO pedira texto
	scr_cnt SMALLINT

   OPTIONS
      ACCEPT KEY escape

	LET int_flag = 0  
	LET comia = ASCII 34
	WHENEVER ERROR CONTINUE
	--Open the window at the position specified by the parameters Wxpos and Wypos
	OPEN WINDOW find_ayuda WITH FORM "picklist_2"
	WHENEVER ERROR STOP
	IF (STATUS < 0 ) THEN -- The Window could not be opened, 
                         -- the form does not exist
                         -- or the parameters WxPos, WyPos are wrong
      ERROR "Error al abrir forma de busqueda -- Consulte con el Programador --" SLEEP 1
		RETURN -1,1,INT_FLAG
	END IF

	LET w = ui.Window.getcurrent()
	LET f = w.getForm()
	CALL f.setElementText("filtro",title0)
	CALL f.setElementText("formonly.codigo",title1)
	CALL f.setElementText("formonly.descripcion",title2)
	CALL fgl_settitle("Busqueda dinamica")
	CALL la_busqueda.CLEAR()
	LET aborta = 0  
	
	WHILE aborta = 0
		INITIALIZE retorna.* TO NULL 
		LET refe = "Ingrese informacion a buscar..." 
		LET int_flag = FALSE  
		LET buscar = refe 
		LET i = 1

		INPUT BY NAME refe WITHOUT DEFAULTS 
			BEFORE INPUT
				CALL fgl_dialog_setkeylabel("ACCEPT","Detalle")
				CALL fgl_dialog_setkeylabel("INTERRUPT","Cancelar")
					
			BEFORE FIELD refe
				IF opci = 0   THEN
					LET sqlstmnt = " SELECT ",Field1 CLIPPED, ",", Field2 CLIPPED,
										" FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
										" ORDER BY ",OrderNum
					WHENEVER ERROR CONTINUE
					PREPARE ex_sqlst FROM sqlstmnt
					WHENEVER ERROR STOP

					display "query", sqlstmnt

					--If the user supplied wrong values
					IF (SQLCA.SQLCODE <> 0) THEN
						ERROR "Error en query -- Contacte al Programador --" SLEEP 1
						LET aborta = 1
						LET retorna.codigo = -1
						LET retorna.descripcion = ""
						EXIT INPUT
					END IF

					DECLARE estos0 CURSOR FOR ex_sqlst
					CALL la_busqueda.CLEAR()
					LET i =1
					FOREACH estos0 INTO la_busqueda[i].*
						LET i=i+1
					END FOREACH

					FREE estos0
					FREE ex_sqlst
					CALL f.setElementText("registros",'Registros encontrados '||(i-1))
					DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
						BEFORE DISPLAY
							CALL fgl_dialog_setkeylabel("ACCEPT","")
							CALL fgl_dialog_setkeylabel("INTERRUPT","")
							CALL fgl_dialog_setkeylabel("insert", "")
							CALL fgl_dialog_setkeylabel("Append", "")
							CALL fgl_dialog_setkeylabel("Delete", "")

						BEFORE ROW
							EXIT DISPLAY
					END DISPLAY
					LET buscar = refe
					EXIT INPUT
				END IF
				 
			AFTER FIELD refe
				IF refe = "Ingrese informacion a buscar..." THEN
					--LET refe = "*"
               LET refe = "%"
				END IF
                       
				--LET buscar = "*"||refe||"*"
            LET buscar = "%"||refe||"%"

				IF opci = 1  THEN --pedira texto a buscar
					LET sqlstmnt = "SELECT ",Field1 CLIPPED, ",", Field2 CLIPPED,
										" FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
										--" AND upper(", field2 CLIPPED, ") MATCHES upper(",
                              " AND upper(", field2 CLIPPED, ") LIKE upper(", 
										comia CLIPPED, buscar CLIPPED, comia CLIPPED, ")", " ORDER BY ",OrderNum
				ELSE
					LET sqlstmnt = "SELECT ",Field1 CLIPPED, ",", Field2 CLIPPED,
										" FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
										" ORDER BY ",OrderNum
				END IF

				WHENEVER ERROR CONTINUE
				PREPARE ex_sqlst1 FROM sqlstmnt
				WHENEVER ERROR STOP

				--If the user supplied wrong values
				IF (SQLCA.SQLCODE <> 0) THEN
					ERROR "Error en query -- Contacte al Programador --" SLEEP 1
					LET aborta = 1
					LET retorna.codigo = -1
					LET retorna.descripcion = ""
					EXIT INPUT
				END IF

				DECLARE estos1 CURSOR FOR ex_sqlst1
				CALL la_busqueda.CLEAR()
				LET i =1
				FOREACH estos1 INTO la_busqueda[i].*
					LET i=i+1
				END FOREACH
	
				FREE estos1
				FREE ex_sqlst1

				CALL f.setElementText("registros",'Registros encontrados '||(i-1))
				DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
					BEFORE DISPLAY
						CALL fgl_dialog_setkeylabel("ACCEPT","")
						CALL fgl_dialog_setkeylabel("INTERRUPT","")
						CALL fgl_dialog_setkeylabel("insert", "")
						CALL fgl_dialog_setkeylabel("Append", "")
						CALL fgl_dialog_setkeylabel("Delete", "")

					BEFORE ROW
						EXIT DISPLAY
				END DISPLAY
				LET buscar = refe
				EXIT INPUT

			ON IDLE 1
				CALL GET_FLDBUF(refe) RETURNING refe
				IF LENGTH(refe) = 0 THEN
					LET refe = ""
				END IF
				IF buscar <> refe OR opci = 0 THEN
					--LET buscar = "*"||refe||"*"
               LET buscar = "%"||refe||"%"

					IF opci = 1  THEN --pedira texto a buscar
						LET sqlstmnt = "SELECT ",Field1 CLIPPED, ",", Field2 CLIPPED,
											" FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
											--" AND upper(", field2 CLIPPED, ") MATCHES upper(",
                                 " AND upper(", field2 CLIPPED, ") LIKE upper(", 
											comia CLIPPED, buscar CLIPPED, comia CLIPPED, ")", " ORDER BY ",OrderNum
					ELSE
						LET sqlstmnt = "SELECT ",Field1 CLIPPED, ",", Field2 CLIPPED,
											" FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
											" ORDER BY ",OrderNum
					END IF

					WHENEVER ERROR CONTINUE
					PREPARE ex_sqlst2 FROM sqlstmnt
					WHENEVER ERROR STOP
	
					--If the user supplied wrong values
					IF (SQLCA.SQLCODE <> 0) THEN
						ERROR "Error en query -- Contacte al Programador --" SLEEP 1
						LET aborta = 1
						LET retorna.codigo = -1
						LET retorna.descripcion = ""
						EXIT INPUT
					END IF

					DECLARE estos2 CURSOR FOR ex_sqlst2
					CALL la_busqueda.CLEAR()
					LET i =1
					FOREACH estos2 INTO la_busqueda[i].*
						LET i=i+1
					END FOREACH
					FREE estos2
					FREE ex_sqlst2
					CALL f.setElementText("registros",'Registros encontrados '||(i-1))
					LET aborta = 0  
					DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
						BEFORE DISPLAY
							CALL fgl_dialog_setkeylabel("ACCEPT","")
							CALL fgl_dialog_setkeylabel("INTERRUPT","")
							CALL fgl_dialog_setkeylabel("insert", "")
							CALL fgl_dialog_setkeylabel("Append", "")
							CALL fgl_dialog_setkeylabel("Delete", "")
							
						BEFORE ROW
							EXIT DISPLAY
					END DISPLAY
					LET buscar = refe
					IF opci = 0 THEN  --NO debe pedir texto de busqueda
						EXIT INPUT
					END IF
				END IF --si es diferente ya el ingreso

			AFTER INPUT
				IF int_flag THEN
					LET aborta = 1
					EXIT INPUT
				END IF

		END INPUT

		IF aborta THEN
			LET int_flag = TRUE
			EXIT WHILE
		END IF
		OPTIONS
          ACCEPT KEY RETURN

		CALL f.setElementText("registros",'Registros encontrados '||(i-1)|| "    Presione [ENTER] para seleccionar ")
		DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
			BEFORE DISPLAY
				CALL fgl_dialog_setkeylabel("ACCEPT","Seleccionar\nRegistro")
				CALL fgl_dialog_setkeylabel("insert", "")
				CALL fgl_dialog_setkeylabel("Append", "")
				CALL fgl_dialog_setkeylabel("Delete", "")
                    
				IF opci = 1 THEN
					CALL fgl_dialog_setkeylabel("INTERRUPT","Nueva\nBusqueda")
				ELSE
					CALL fgl_dialog_setkeylabel("INTERRUPT","")
				END IF
	
				IF i = 1 THEN
					CALL box_valdato("No encontro o selecciono datos!")
				END IF

			BEFORE ROW
				LET i = arr_curr()
				LET scr_cnt = scr_line()
				LET retorna.* = la_busqueda[i].*
				IF LENGTH(la_busqueda[i].codigo) = 0 THEN
					CALL fgl_dialog_setcurrline(scr_cnt,i-1)
				END IF

			ON KEY(RETURN)
            LET aborta = 1
            EXIT DISPLAY
	
			ON KEY(INTERRUPT)
				IF opci = 1 THEN
					LET aborta = 0
					EXIT DISPLAY
				END IF
							
			ON KEY(ACCEPT)
				LET aborta = 1
				EXIT DISPLAY

			AFTER DISPLAY
				CONTINUE DISPLAY
		END DISPLAY

		IF aborta = 1 THEN
			EXIT WHILE
		END IF

	END WHILE

   CLOSE window find_ayuda
	options 
         ACCEPT KEY ESCAPE

	RETURN retorna.*, INT_FLAG
END FUNCTION

