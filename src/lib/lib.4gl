#########################################################################
## Function  : msg
##
## Parameters: msg
##
## Returnings: none
##
## Comments  : Devuelve un mensaje de Error
#########################################################################
FUNCTION msg(msg)
   DEFINE msg  STRING
   CALL box_valdato(msg)
END FUNCTION

#########################################################################
## Function  : lib_cleanTinyScreen()
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Limpia el background de la ventana desplegada
#########################################################################
FUNCTION lib_cleanTinyScreen()
define screenToBeCleaned   om.DomNode
define doc                 om.DomDocument
define l                   om.NodeList
define att                 STRING
constant nodeName = "Window"
constant nodeAtt  = "screen"

   let doc = ui.Interface.getDocument()
   let screenToBeCleaned = doc.getDocumentElement()
   let l = screenToBeCleaned.selectByTagName(nodeName)
   let screenToBeCleaned = l.item(1)
   let att = screenToBeCleaned.getAttribute("name")
   if att = nodeAtt then
      call doc.removeElement(screenToBeCleaned)
   end if
END FUNCTION

#########################################################################
## Function  : combo_din2
##
## Parameters: inputParameters
##
## Returnings: returnParameters
##
## Comments  : Funcion que genera un combo dinamico
#########################################################################
FUNCTION combo_din2(vl_2_campo, vl_2_query)
define		vl_2_campo		  string
define		vl_2_query		  string
define		vl_2_lista		  char(500)
define		vl_2_lista2	  char(500)
define		vl_2_metodo	  string
define		vl_2_valor		  string
define		vl_2_valor2	  string
define		vl_2_combo      ui.ComboBox
define		vl_2_lista_nodb base.StringTokenizer

	let vl_2_campo = vl_2_campo.trim()
	let vl_2_query = vl_2_query.trim()

	#-- Obtiene el nodo del metodo Combo de la clase ui
	let vl_2_combo = ui.ComboBox.forName(vl_2_campo)
	if vl_2_combo is null then
		return
	end if

	#-- Limpia los registros anteriores del combo
	call vl_2_combo.clear()

	#-- Procesa segun el metodo convirtiendo a mayusculas para mejor tratamiento
	let vl_2_metodo = vl_2_query.toUpperCase()

	if vl_2_metodo matches "SELECT*"   then	
		#--Prepara el query
		prepare q_comboList2 from vl_2_query
		declare c_comboList2 cursor for q_comboList2
		#-- Obtiene la lista de valores de la tabla segun el query enviado
		foreach c_comboList2 into vl_2_lista, vl_2_lista2
			let vl_2_valor = vl_2_lista clipped
			let vl_2_valor2 = vl_2_lista2 clipped
			call vl_2_combo.addItem(vl_2_valor, vl_2_valor2)
		end foreach	
	else
	   #-- Obtiene la lista de valores que no son de base de datos
		let vl_2_lista_nodb = base.StringTokenizer.create(vl_2_query, "|")
		while vl_2_lista_nodb.hasMoreTokens()
         let vl_2_valor = vl_2_lista_nodb.nextToken()
         call vl_2_combo.addItem(vl_2_valor, vl_2_valor)
		end while
	end if
END FUNCTION

#########################################################################
## Function  : writeXML()
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Escribe un arbol AUI en XML
#########################################################################
FUNCTION writeXML()
   DEFINE doc        om.DomDocument
   DEFINE n, t, t2   om.DomNode
   DEFINE w          ui.Window
   DEFINE xxx        STRING
   DEFINE l          om.NodeList
   
   LET doc = ui.Interface.getDocument()
   LET n = ui.Interface.getRootNode()
   CALL n.writeXml("aui.xml")
   ERROR "File aui.xml written"
END FUNCTION

#########################################################################
## Function  : confirma()
##
## Parameters: msg         << Pregunta a confirmar
##
## Returnings: res         >> Respuesta
##
## Comments  : Confirmacion
#########################################################################
FUNCTION confirma(msg)
   DEFINE msg  STRING
   DEFINE res  SMALLINT
   LET res = FALSE
   MENU "Confirmacion..." ATTRIBUTES (STYLE="dialog", COMMENT=msg, IMAGE="question")
      COMMAND "Si"
         LET res = TRUE
         EXIT MENU
      COMMAND "No"
         LET res = FALSE
         EXIT MENU
   END MENU
   RETURN res
END FUNCTION

FUNCTION createStartMenuGroup(p,t)
   DEFINE p om.DomNode
   DEFINE t STRING
   DEFINE s om.DomNode

   LET s = p.createChild("StartMenuGroup")
   CALL s.setAttribute("text",t)
   RETURN s
END FUNCTION

FUNCTION createStartMenuCommand(p,t,c,i)
   DEFINE p om.DomNode
   DEFINE t,c,i STRING
   DEFINE s om.DomNode

   LET s = p.createChild("StartMenuCommand")
   CALL s.setAttribute("text",t)
   CALL s.setAttribute("exec",c)
   CALL s.setAttribute("image",i)
RETURN s
END FUNCTION

#########################################################################
## Function  : createGrid()
##
## Parameters: width    << Ancho del Grid
##             height   << Alto del Grid
##             wName    << Window name
##
## Returnings: none
##
## Comments  : Crea un Grid en la forma
#########################################################################
FUNCTION createGrid(width, height, wName)
   DEFINE w             ui.Window
   DEFINE n, g          om.DomNode
   DEFINE width, height SMALLINT
   DEFINE wName         STRING
   
   LET w = ui.Window.getCurrent()
   LET g = w.findNode("Form", wName)
   LET n = g.createChild("Grid")
      CALL n.setAttribute("width", width)
      CALL n.setAttribute("height", height)
END FUNCTION
