################################################################################
# Funcion     : %M%
# Descripcion : Modulo Principal para umdores 
# Funciones   : main_init(dbname)
#               main_menu()                               
#               dummy() 
#               
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Anderson Garcia  
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################
IMPORT util 

GLOBALS "reqp0308_glob.4gl"
   

MAIN
   DEFINE 
		n_param 	SMALLINT,
		prog_name2 	STRING

   LET prog_name2 = prog_name||".log"

   CALL STARTLOG(prog_name2)
	CALL main_init()

END MAIN 

FUNCTION main_init()
   DEFINE 
	nom_forma 	STRING,
    w           ui.Window,
    f           ui.Form
    
   CALL ui.Interface.loadActionDefaults("actiondefaults_rh1")
   --CALL combo_init()
	  
	INITIALIZE u_reg.* TO NULL
	LET g_reg.* = u_reg.*

   LET nom_forma = prog_name CLIPPED, "_form"
   
	OPEN WINDOW w1 WITH FORM nom_forma
   CALL fgl_settitle("ANDE - Catálogo de Empleados")

	LET w = ui.WINDOW.getcurrent()
	LET f = w.getForm()

   CALL query_init()
   CALL insert_init()
   CALL update_init()
   CALL delete_init()
   CALL combo_init()
   CALL creatmp()
	CALL main_menu()
   
END FUNCTION 

FUNCTION main_menu()

   DEFINE cuantos     SMALLINT,
         runcmd      STRING 
    
    MENU
      BEFORE MENU
         CALL DIALOG.setActionActive("candidatos", 0) --can_user_append() )
         CALL DIALOG.setActionActive("siguiente", 0) --can_user_append() )
         CALL DIALOG.setActionActive("anterior",  0) --can_user_append() )
         CALL DIALOG.setActionActive("verdoc",    0) --can_user_append() )
         CALL DIALOG.setActionActive("modificar", 0) --can_user_append() )
         CALL DIALOG.setActionActive("anular",    0) --can_user_append() )

      ON ACTION agregar
         CALL cleartmp()
         IF ingreso() THEN
         END IF 
         
      ON ACTION buscar
         LET cuantos = consulta(true)
         IF cuantos > 0 THEN 
            CALL DIALOG.setActionActive("candidatos",1) --can_user_append() )
            CALL DIALOG.setActionActive("siguiente", 1) --can_user_append() )
            CALL DIALOG.setActionActive("anterior",  1) --can_user_append() )
            IF numdocs > 0 THEN CALL DIALOG.setActionActive("verdoc",    1) END IF 
            CALL DIALOG.setActionActive("modificar", 1) --can_user_append() )
            CALL DIALOG.setActionActive("anular",    1) --can_user_append() )
            CALL display_reg()
         ELSE
            CALL DIALOG.setActionActive("candidatos",0) --can_user_append() )
            CALL DIALOG.setActionActive("siguiente", 0) --can_user_append() )
            CALL DIALOG.setActionActive("anterior",  0) --can_user_append() )
            CALL DIALOG.setActionActive("verdoc",    0) --can_user_append() )
            CALL DIALOG.setActionActive("modificar", 0) --can_user_append() )
            CALL DIALOG.setActionActive("anular",    0) --can_user_append() )
         END IF 
         CALL encabezado("")  

      ON ACTION candidatos
         LET runcmd = 'fglrun reqp0312.42r ', g_reg.reqId
         RUN runcmd 
         
      ON ACTION siguiente
         CALL sigant(1)
         IF numdocs > 0 THEN 
            CALL DIALOG.setActionActive("verdoc",    1)
         ELSE 
            CALL DIALOG.setActionActive("verdoc",    0)
         END IF 

      ON ACTION anterior
         CALL sigant(-1)
         IF numdocs > 0 THEN 
            CALL DIALOG.setActionActive("verdoc",    1)
         ELSE 
            CALL DIALOG.setActionActive("verdoc",    0)
         END IF

      ON ACTION verdoc
         CALL display_doc('D')

      ON ACTION modificar
         CALL cleartmp()
         IF g_reg.reqId > 0 THEN 
            IF modifica() THEN
               
            END IF   
         END IF 
         CALL encabezado("")
         
      ON ACTION anular
         IF g_reg.reqId > 0 THEN 
            IF anular() THEN
               --Si es anulacion
               --LET reg_det[id].* = g_reg.*
               --DISPLAY reg_det[id].* TO sDet[ids].*
               --Si es eliminacion
   
               INITIALIZE g_reg.* TO NULL
               DISPLAY BY NAME g_reg.*
            END IF   
         END IF 
         
      ON ACTION salir 
         EXIT MENU 
   END MENU 

END FUNCTION 

FUNCTION encabezado(gtit_enc)
DEFINE gtit_enc STRING 

   DISPLAY BY NAME  gtit_enc
END FUNCTION 

FUNCTION combo_init()
   --Empresa contratante
   CALL combo_din2("reqempcon","select id_commemp, trim(iniciales) from commemp WHERE estado=1 ORDER BY 2 ")
   --Locación / Ubicación
   CALL combo_din2("pueLoc","select locId, trim(locNombre) from glbMLoc ORDER BY 2")
   --Motivo
   CALL combo_din2("reqMotivo","select motId, trim(motNombre) from glbMMot ORDER BY 2")
   --Area Funcional del Puesto
   CALL combo_din2("pueAreaF", "SELECT afuId, trim(afuNombre) FROM glbMAreaF ORDER BY 2")
   --Unidad Organizativa
   CALL combo_din2("pueUniOrg","select uorId, trim(uorNombre) from glbUniOrga ORDER BY 2")
   --Posición / Puesto
   CALL combo_din2("puePosicion","select pueId, trim(pueNombre) from glbmpue ORDER BY 2")
   --Centro de Costo
   CALL combo_din2("pueCco","select ccoId, trim(ccoNombre) from glbCco ORDER BY 2")
   --Area Funcional del Puesto
   CALL combo_din2("jefeAreaF", "SELECT afuId, trim(afuNombre) FROM glbMAreaF ORDER BY 2")
   --Digitador
   CALL combo_din2("reqDigitador", "SELECT id_commempl, trim(nombre)||' '||trim(apellido) FROM comMEmpl ORDER BY 2")
   --Solicitante
   CALL combo_din2("reqUsuSol", "SELECT id_commempl, trim(nombre)||' '||trim(apellido) FROM comMEmpl WHERE esSolicitante = '1' ORDER BY 2" )
   --Autorizante
   CALL combo_din2("reqUsuAut", "SELECT id_commempl, trim(nombre)||' '||trim(apellido) FROM comMEmpl WHERE esAutorizante = '1' ORDER BY 2" )
   --Gerencia funcional que autoriza
   CALL combo_din2("gerFunAut", "SELECT id_commempl, trim(nombre)||' '||trim(apellido) FROM comMEmpl WHERE esGerAreaF = '1' ORDER BY 2")
   --Unidad Organizativa Jefe
   CALL combo_din2("jefeUniOrg","select uorId, trim(uorNombre) from glbUniOrga ORDER BY 2")
   --Posición / Puesto Jefe
   CALL combo_din2("puePue","select pueId, trim(pueNombre) from glbmpue ORDER BY 2")
   --CALL combo_din2("cmbccoUniOrga","select uorId, trim(uorNombre) from glbUniOrga")
END FUNCTION 

FUNCTION display_reg()

   --CALL display_doc()
   --DIALOG
      
      DISPLAY BY NAME g_reg.*
      DISPLAY util.Datetime.format(g_reg.fecHorIngreso, "%Y-%m-%d %H:%M:%S") TO fecHorIngreso 
      CALL display_doc('R')

      --DISPLAY ARRAY a_doc TO sa_doc.*
        -- BEFORE DISPLAY 
          --  EXIT DISPLAY
         --DISPLAY BY NAME g_reg.*
      --END DISPLAY
      {ON ACTION siguiente
         CALL sigant(1)

      ON ACTION anterior
         CALL sigant(-1)}
   --END DIALOG 
END FUNCTION 

FUNCTION creatmp()
   CREATE TEMP TABLE tmpdoc
   (
      reqId                 INTEGER,
      rdaId                 SMALLINT,
      rdaNom                VARCHAR(255),
      rdaDoc                BYTE   
   )
END FUNCTION 

FUNCTION cleartmp()
   DELETE FROM tmpdoc WHERE 1=1
END FUNCTION 

FUNCTION display_doc(accion CHAR(1))
   --R = Como parte del display Registro
   --D = Accion ver Documento
   --E = Para Eliminar el documento
   DEFINE x SMALLINT
   DEFINE result INTEGER  
   DEFINE nombre STRING
   --DEFINE sql_stmt STRING 

   IF accion = 'E' THEN
      DECLARE tdoclist CURSOR FOR SELECT rdaId, rdaNom FROM tmpdoc
      CALL a_doc.clear()
      LET x = 1
      FOREACH tdoclist INTO a_doc[x].rdaId, a_doc[x].rdaNom
         LET x = x + 1
      END FOREACH
      LET x = x - 1
      LET numdocs = x
   ELSE
      DECLARE rdoclist CURSOR FOR SELECT rdaId, rdaNom FROM reqDDoc WHERE reqId = g_reg.reqId
      CALL a_doc.clear()
      LET x = 1
      FOREACH rdoclist INTO a_doc[x].rdaId, a_doc[x].rdaNom
         LET x = x + 1
      END FOREACH
      LET x = x - 1
      LET numdocs = x
   END IF 
    
   DISPLAY ARRAY a_doc TO sa_doc.*
      ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE,UNBUFFERED)
      BEFORE DISPLAY 
         --DISPLAY "accion lleva ------------> ", accion 
         IF accion = 'R' THEN 
            EXIT DISPLAY
         END IF 
         IF accion = 'E' THEN
            CALL DIALOG.setActionActive("deldocdel", 1)
         ELSE 
            CALL DIALOG.setActionActive("deldocdel", 0)
         END IF 

      ON ACTION regresar
         EXIT DISPLAY 
         
      ON ACTION deldocdel
         IF (box_pregunta("Eliminar el documento "||a_doc[arr_curr()].rdaNom)) = 'Si' THEN 
            DELETE FROM tmpdoc WHERE rdaId = a_doc[arr_curr()].rdaId
            CALL a_doc.deleteElement(arr_curr())
            
         END IF 
         
      ON ACTION mostrar
         LOCATE g_doc.rdaDoc IN FILE './files/'||nospace(a_doc[arr_curr()].rdaNom)
         SELECT rdaDoc INTO g_doc.rdaDoc FROM reqDDoc WHERE reqId = g_reg.reqId AND rdaId = a_doc[arr_curr()].rdaId
         CALL fgl_putfile('./files/'||nospace(a_doc[arr_curr()].rdaNom), a_doc[arr_curr()].rdaNom)
         CALL ui.interface.frontcall("standard","shellexec",[a_doc[arr_curr()].rdaNom],[result])
          
   END DISPLAY
   
END FUNCTION 



