################################################################################
# Funcion     : %M%
# Descripcion : Ingreso de un nuevo umdor 	
# Funciones   : input_umd() funcion para insertar datos
#              
#              
#               
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################
IMPORT security 
IMPORT util 

GLOBALS "reqp0308_glob.4gl"

FUNCTION captura_datos(operacion)
   DEFINE operacion CHAR (1)
   DEFINE resultado BOOLEAN
   DEFINE   win ui.Window,
            fm ui.Form
   DEFINE gUsuario   LIKE commempl.id_commempl
   DEFINE sql_stmt   STRING 
   DEFINE lFlagTemp, lFlagVac, lFlagAut CHAR(1)
   DEFINE result     INTEGER 


   LET resultado = FALSE 
   LET u_reg.* = g_reg.*
   
   --Código de requerimiento
   SELECT valChr INTO codFormato FROM glb_paramtrs WHERE numPar=2 AND tipPar=1
   DISPLAY BY NAME codFormato

   CALL fgl_settitle("ANDE - Sistema de Requisiciones")
 
   LET win = ui.Window.getCurrent()
   LET fm = win.getForm()

   DIALOG ATTRIBUTES(UNBUFFERED)
   
      INPUT BY NAME g_reg.*
         ATTRIBUTES (WITHOUT DEFAULTS  )
         
      
         BEFORE INPUT 
            --esconder campos
            CALL fm.setElementHidden("reqmreq_reqtmpmes_label1", 1)
            CALL fm.setFieldHidden("reqtmpmes",1)
            
            CALL fm.setFieldHidden("reqvacant",1)
            CALL fm.setElementHidden("reqmreq_reqvacant_label1", 1)
            
            CALL fm.setElementHidden("reqmreq_requsuaut_label1", 1)
            CALL fm.setFieldHidden("gerFunAut",1)
            
            LET lFlagVac   ='N'
            LET lFlagTemp  ='N'
            LET lFlagAut   ='N'
            IF operacion = 'I' THEN 
               INITIALIZE g_reg.* TO NULL
               --Digitador
               LET gUsuario = arg_val(1)
               LET g_reg.reqDigitador = gUsuario
               --numero de req
               --LET g_reg.reqId = 0
               --fecha de ingreso
               LET g_reg.reqFecRec = TODAY          
               --tipo
               LET g_reg.reqTipo = 'I'
               
               --DISPLAY BY NAME g_reg.reqId, g_reg.reqFecRec, g_reg.reqTipo, g_reg.reqDigitador
               CALL display_reg() --DISPLAY BY NAME g_reg.*
            ELSE 
               CALL llenatmpdoc()
            END IF
            
          ON CHANGE reqMotivo
            IF g_reg.reqMotivo IS NULL THEN 
               CALL msg("Motivo no puede ser nulo")
               NEXT FIELD reqMotivo
            ELSE
               SELECT flagTemp, flagVac, motReqAut 
                  INTO lFlagTemp, lFlagVac, lFlagAut
                  FROM glbMMot
                  WHERE motId = g_reg.reqMotivo
               --vacante
               IF lFlagVac = '1' THEN 
                  CALL fm.setElementHidden("reqmreq_reqvacant_label1", 0)
                  CALL fm.setFieldHidden("reqvacant",0)
               ELSE 
                  CALL fm.setElementHidden("reqmreq_reqvacant_label1", 1)
                  CALL fm.setFieldHidden("reqvacant",1)
               END IF 
               --temporal
               IF lFlagTemp = '1' THEN 
                  CALL fm.setElementHidden("reqmreq_reqtmpmes_label1", 0)
                  CALL fm.setFieldHidden("reqtmpmes",0)
               ELSE 
                  CALL fm.setElementHidden("reqmreq_reqtmpmes_label1", 1)
                  CALL fm.setFieldHidden("reqtmpmes",1)
               END IF
               --Requiere Autorizacion
               IF lFlagAut = '1' THEN 
                  CALL fm.setElementHidden("reqmreq_requsuaut_label1", 0)
                  CALL fm.setFieldHidden("gerFunAut",0)
               ELSE
                  CALL fm.setElementHidden("reqmreq_requsuaut_label1", 1)
                  CALL fm.setFieldHidden("gerFunAut",1)
               END IF 
            END IF 

         ON CHANGE pueAreaF
            LET sql_stmt = "SELECT uorId, trim(uorNombre) from glbUniOrga WHERE uorAreaF = ", g_reg.pueAreaF, " ORDER BY 2"
            CALL combo_din2("pueUniOrg", sql_stmt)
            
         ON CHANGE pueUniOrg
            IF g_reg.pueUniOrg IS NOT NULL THEN 
               LET sql_stmt = "select pueId, trim(pueNombre) from glbmpue WHERE pueUniOrga = ", g_reg.pueUniOrg, " ORDER BY 2"
               CALL combo_din2("puePosicion",sql_stmt)

               LET sql_stmt = "select ccoId, trim(ccoNombre) from glbCco WHERE ccoUniOrga = ", g_reg.pueUniOrg, " ORDER BY 2"
               CALL combo_din2("pueCco", sql_stmt)
            END IF 

         ON CHANGE jefeAreaF
            LET sql_stmt = "SELECT uorId, trim(uorNombre) from glbUniOrga WHERE uorAreaF = ", g_reg.jefeAreaF, " ORDER BY 2"
            CALL combo_din2("jefeUniOrg", sql_stmt)

         ON CHANGE jefeUniOrg
            IF g_reg.jefeUniOrg IS NOT NULL THEN 
               LET sql_stmt = "select pueId, trim(pueNombre) from glbmpue WHERE pueUniOrga = ", g_reg.jefeUniOrg, " ORDER BY 2"
               CALL combo_din2("jefePue",sql_stmt)

               LET sql_stmt = "select ccoId, trim(ccoNombre) from glbCco WHERE ccoUniOrga = ", g_reg.pueUniOrg, " ORDER BY 2"
               CALL combo_din2("pueCco", sql_stmt)
            END IF
            
      END INPUT 

      ON ACTION ACCEPT
         
         IF g_reg.reqEmpCon IS NULL THEN
           CALL msg("Debe ingresar Empresa contratante")
           NEXT FIELD reqEmpCon
         END IF  

         IF g_reg.pueLoc IS NULL THEN
           CALL msg("Debe ingresar Locación")
           NEXT FIELD pueLoc
         END IF 
         IF g_reg.reqMotivo IS NULL THEN
           CALL msg("Debe ingresar Motivo")
           NEXT FIELD pueLoc
         END IF 
         {
         IF g_reg.codigo IS NOT NULL THEN 
           IF g_reg.codigo <> u_reg.codigo THEN 
              SELECT * FROM commempl WHERE codigo = g_reg.codigo
              IF sqlca.sqlcode = 0 THEN
                 CALL msg("Este código ya está utilizado, ingrese de nuevo")
                 NEXT FIELD CURRENT 
              END IF
           END IF  
         END IF 
         }
         IF lFlagTemp = '1' AND g_reg.reqTmpMes IS NULL THEN
            CALL msg('Debe ingresar cantidad de meses que la plaza será temporal')
            NEXT FIELD reqTmpMes 
         END IF
         IF lFlagVac='1' AND g_reg.reqVacAnt IS NULL THEN
            CALL msg('Debe ingresar nombre de la personal que reemplazará')
            NEXT FIELD reqVacAnt 
         END IF

         IF g_reg.reqTipo IS NULL THEN
           CALL msg("Debe ingresar Tipo de Reclutamiento")
           NEXT FIELD reqTipo
         END IF
         IF g_reg.pueAreaF IS NULL THEN
           CALL msg("Debe ingresar Area Funcional del Puesto")
           NEXT FIELD pueAreaF
         END IF
         IF g_reg.pueUniOrg IS NULL THEN
           CALL msg("Debe ingresar Unidad Organizativa del puesto")
           NEXT FIELD pueUniOrg
         END IF
         IF g_reg.puePosicion IS NULL THEN
           CALL msg("Debe ingresar Posición del puesto")
           NEXT FIELD puePosicion
         END IF
         IF g_reg.pueCco IS NULL THEN
           CALL msg("Debe ingresar Centro de Costo del puesto")
           NEXT FIELD pueCco
         END IF
         IF g_reg.puenecveh IS NULL THEN
           CALL msg("Debe indicar si el puesto requiere vehículo")
           NEXT FIELD puenecveh
         END IF
         IF g_reg.puehorario IS NULL THEN
           CALL msg("Debe indicar el horario")
           NEXT FIELD puehorario
         END IF
         IF g_reg.jefeAreaF IS NULL THEN
           CALL msg("Debe ingresar Area Funcional del Jefe")
           NEXT FIELD jefeAreaF
         END IF
         IF g_reg.jefeuniorg IS NULL THEN
           CALL msg("Debe ingresar Unidad Organizativa del Jefe")
           NEXT FIELD jefeuniorg
         END IF
         IF g_reg.jefepue IS NULL THEN
           CALL msg("Debe ingresar Posición del Jefe")
           NEXT FIELD jefepue
         END IF
         IF g_reg.jefenom IS NULL THEN
           CALL msg("Debe ingresar Nombre del Jefe")
           NEXT FIELD jefenom
         END IF
         IF g_reg.reqautplatmp IS NULL THEN
           CALL msg("Debe indicar si autoriza a RRHH a crear una plaza temporal")
           NEXT FIELD reqautplatmp
         END IF
         IF g_reg.reqUsuSol IS NULL THEN
           CALL msg("Debe ingresar Usuario Solicitante")
           NEXT FIELD reqUsuSol
         END IF
         IF g_reg.reqUsuAut IS NULL THEN
           CALL msg("Debe ingresar Usuario Autorizante")
           NEXT FIELD reqUsuAut
         END IF
         IF lFlagAut='1' AND g_reg.gerFunAut IS NULL THEN
           CALL msg("Debe ingresar Gerente Funcional")
           NEXT FIELD gerFunAut
         END IF
   
          { 
         --IF (operacion = 'I') OR (g_reg.idBautizo <> u_reg.idBautizo) THEN 
            --IF existe_cat_cod(g_reg.idBautizo) THEN 
               --CALL msg("Este codigo ya esta siendo utilizado")
               --LET g_reg.idBautizo = NULL 
               --NEXT FIELD idBautizo 
            --END IF   
         --END IF
         }
         {IF operacion = 'M' AND g_reg.* = u_reg.* THEN
            CALL msg("No se efectuaron cambios")
            EXIT DIALOG 
         END IF}
         CASE box_gradato("Seguro de grabar")
            WHEN "Si"
               LET resultado = TRUE
               EXIT DIALOG
            WHEN "No"
               EXIT DIALOG 
            OTHERWISE
               CONTINUE DIALOG 
         END CASE 
         LET resultado = TRUE
         EXIT DIALOG 

      ON ACTION docadd
         IF adddoc() THEN --msg("Agregar doc")
            CALL disp_listadoc()
         END IF 
         
      ON ACTION deldocdel
         CALL display_doc('E')
         --CALL msg("Eliminar doc")
         
      {ON ACTION mostrar
         LOCATE g_doc.rdaDoc IN FILE './files/'||a_doc[arr_curr()].rdaNom
         SELECT rdaDoc INTO g_doc.rdaDoc FROM reqDDoc WHERE reqId = g_reg.reqId AND rdaId = a_doc[arr_curr()].rdaId
         CALL fgl_putfile('./files/'||a_doc[arr_curr()].rdaNom, a_doc[arr_curr()].rdaNom)
         CALL ui.interface.frontcall("standard","shellexec",[a_doc[arr_curr()].rdaNom],[result])}
         
      {ON ACTION docver
         CALL msg("Ver doc")}
      
     ON ACTION CLOSE -- CANCEL
        EXIT dialog
   END DIALOG
   
   IF NOT resultado THEN
      LET g_reg.* = u_reg.*
      CALL display_reg()  
   END IF 
   RETURN resultado
   
END FUNCTION

FUNCTION adddoc()
   DEFINE rec RECORD
                path STRING,
                name STRING,
                wildcards STRING,
                caption STRING
           END RECORD
   DEFINE accion CHAR(1)
   DEFINE filename BIGINT  

   OPEN WINDOW w2 WITH FORM "reqp0308_form2"

      MENU 
         BEFORE MENU  
            SELECT MAX(rdaId) INTO g_doc.rdaId FROM tmpdoc
            IF g_doc.rdaId = 0 OR g_doc.rdaId IS NULL THEN
               LET g_doc.rdaId = 1
            ELSE 
               LET g_doc.rdaId = g_doc.rdaId + 1
            END IF

         ON ACTION carga
            LET rec.path = ""
            LET rec.name = "Archivos"
            LET rec.wildcards = ""
            LET rec.caption = "Open file"
            CALL ui.Interface.frontCall("standard","openFile",[rec.*],[g_doc.rdaNom])
            TRY
               --LET filename = security.RandomGenerator.CreateRandomString(20)
               LET filename = security.RandomGenerator.CreateRandomNumber()
  
               --DISPLAY "filename ", filename 
               CALL fgl_getfile(g_doc.rdaNom, './files/'||filename)
               --DISPLAY "traslade el arch ", g_doc.rdaNom, './files/'||filename
               --DISPLAY BY NAME g_doc.rdaId, g_doc.rdaNom
               --DISPLAY "desplegando nombre"
               LET accion = 'G'
               EXIT MENU
            CATCH
               ERROR sqlca.sqlcode, " ", sqlca.sqlerrm  # Catch runtime execution errors from the SQLCA diagnostic record
            END TRY
      
      {ON ACTION guardar
         LET accion = 'G'
         EXIT MENU } 
         
      ON ACTION regresar
         LET accion = 'R'
         EXIT MENU  
         
   END MENU 

   IF accion = 'G' THEN
      LOCATE g_doc.rdaDoc IN FILE './files/'||filename
      INSERT INTO tmpdoc (rdaId, rdaNom,rdaDoc) VALUES(g_doc.rdaId, g_doc.rdaNom, g_doc.rdaDoc)
      CALL msg("Documento cargado ")
      CLOSE WINDOW w2
      RETURN 1
   ELSE 
      CALL msg("Proceso cancelado")
      CLOSE WINDOW w2
      RETURN 0
   END IF 
   
END FUNCTION 

FUNCTION disp_listadoc()
   DEFINE i SMALLINT 
   DECLARE doccur CURSOR FOR 
      SELECT rdaId, rdaNom FROM tmpdoc

   LET i = 1 
   FOREACH doccur INTO a_doc[i].rdaId, a_doc[i].rdaNom
      LET i = i + 1
   END FOREACH  
   LET i = i - 1
   DISPLAY ARRAY a_doc TO sa_doc.*
      BEFORE DISPLAY
         EXIT DISPLAY 
   END DISPLAY 
END FUNCTION 

FUNCTION llenatmpdoc()
   DECLARE curllenadoc CURSOR FOR 
      SELECT rdaId, rdaNom, rdaDoc FROM reqDDoc WHERE reqId = g_reg.reqId
      LOCATE g_doc.rdaDoc IN MEMORY 
      FOREACH curllenadoc INTO g_doc.rdaId, g_doc.rdaNom, g_doc.rdaDoc
         INSERT INTO tmpDoc (rdaId, rdaNom, rdaDoc)
            VALUES (g_doc.rdaId, g_doc.rdaNom, g_doc.rdaDoc)
      END FOREACH
END FUNCTION 