################################################################################
# Funcion     : %M%
# param_id : Funcion para consulta de umdores  
# Funciones   : umd_qry_init()    -> Prepara la seleccion
#               query_umd()       -> Ingreso de criterios de busqueda
#               fetch_umd()       -> Navega sobre seleccion de datos
#               clean_up()        -> Limpia el cursor de trabajo
#               display_umd()     -> Despliega el registro en pantalla
#               inicia_null()     -> inicia param_ides a nulos
#               display_estado()  -> Despliega el estado del umdor
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        param_id de la modificacion
#
################################################################################

GLOBALS "reqp0308_glob.4gl"

FUNCTION query_init()
   DEFINE select_stmt3 CHAR(350)

   LET select_stmt3 = "SELECT reqId, reqFecRec, reqEmpCon, pueLoc, reqMotivo, ", 
               " reqTmpMes, reqVacAnt, reqTipo, ", 
               " pueAreaF, pueUniOrg, puePosicion, pueCco, ", 
               " puenecveh, puehorario, ",
               " jefeAreaF, jefeuniorg, jefepue, jefenom, ", 
               " reqautplatmp, reqDigitador, reqUsuSol, reqUsuAut, gerFunAut, ", 
               " reqEstado, fecHorIngreso ",
               " FROM reqMReq ",
               " WHERE reqId = ? "

   PREPARE ex_stmt3 FROM select_stmt3
END FUNCTION 

FUNCTION consulta(flagConsulta)
DEFINE flagConsulta BOOLEAN 
DEFINE 
   rDet tDet,
   x INTEGER,
   consulta, str_query, where_clause, reg_count  STRING
   DEFINE reg_cnt    SMALLINT  

   IF flagConsulta THEN 
      CALL encabezado("Consulta")
      --CALL info_usuario()
      --Creación de la consulta
      CONSTRUCT where_clause 
         ON reqId, reqFecRec, reqEmpCon, pueLoc, reqMotivo, 
            reqTmpMes, reqVacAnt, reqTipo, 
            pueAreaF, pueUniOrg, puePosicion, pueCco, 
            puenecveh, puehorario, 
            jefeAreaF, jefeuniorg, jefepue, jefenom, 
            reqautplatmp, reqDigitador, reqUsuSol, reqUsuAut, gerFunAut, 
            reqEstado, fecHorIngreso
         FROM reqId, reqFecRec, reqEmpCon, pueLoc, reqMotivo, 
            reqTmpMes, reqVacAnt, reqTipo, 
            pueAreaF, pueUniOrg, puePosicion, pueCco,
            puenecveh, puehorario,
            jefeAreaF, jefeuniorg, jefepue, jefenom,
            reqautplatmp, reqDigitador, reqUsuSol, reqUsuAut, gerFunAut,
            reqEstado, fecHorIngreso

         ON ACTION ACCEPT
            EXIT CONSTRUCT 
         ON ACTION CANCEL 
            --CALL reg_det.clear()
            RETURN 0
         
      END CONSTRUCT 
   ELSE
      LET where_clause = " 1=1 "
   END IF

   IF int_flag = TRUE THEN
       LET int_flag = FALSE
       CLEAR FORM
       ERROR "Consulta abortada."
       RETURN FALSE
   END IF
   
   --Armar la consulta
   LET str_query = "SELECT reqId ",
            " FROM reqMReq WHERE ", where_clause CLIPPED

   LET reg_count = "SELECT COUNT(*) ",
            " FROM reqMReq WHERE ", where_clause CLIPPED

   PREPARE ex_stmt FROM str_query

   PREPARE ex_stmt2 FROM reg_count

   DECLARE cur_ptr SCROLL CURSOR WITH HOLD FOR ex_stmt

   OPEN cur_ptr

   FETCH FIRST cur_ptr INTO g_reg.reqId

   IF SQLCA.SQLCODE = NOTFOUND THEN
      CALL msg ("No se encontraron registros.")
      CLOSE cur_ptr
      RETURN FALSE
   ELSE
      DECLARE cur_all CURSOR FOR ex_stmt3
      OPEN cur_all USING g_reg.reqId
      
   FETCH cur_all INTO g_reg.*
      IF SQLCA.SQLCODE = NOTFOUND THEN
         ERROR "No rows found."
         RETURN FALSE
      ELSE
         DECLARE counter_ptr CURSOR FOR ex_stmt2
         OPEN counter_ptr
         FETCH counter_ptr INTO reg_cnt
         CLOSE counter_ptr
         CALL display_reg()
         MESSAGE "There were "|| reg_cnt USING "<<<<<"||
         " rows found."
      --SLEEP 3
      MESSAGE ""
      RETURN TRUE
      END IF
   END IF

END FUNCTION

FUNCTION sigant(fetch_flag SMALLINT)

   FETCH RELATIVE fetch_flag cur_ptr INTO g_reg.reqId
   IF SQLCA.SQLCODE = NOTFOUND THEN
      IF fetch_flag = 1 THEN
         ERROR "You are at the end of the list."
      ELSE
         ERROR "You are at the beginning of the list."
      END IF
      LET fetch_flag = fetch_flag * -1
      FETCH RELATIVE fetch_flag cur_ptr INTO g_reg.reqId
   ELSE
      OPEN cur_all USING g_reg.reqId

      FETCH cur_all INTO g_reg.*

      IF SQLCA.SQLCODE = NOTFOUND THEN
         CLEAR FORM
         DISPLAY BY NAME g_reg.reqId
         ERROR "Row has been deleted since query."
      ELSE
         CALL display_reg()
      END IF
      CLOSE cur_all
   END IF
   --CALL display_reg()
   
END FUNCTION 