
GLOBALS "reqp0322_glob.4gl"

FUNCTION captura_datos(operacion)
DEFINE operacion CHAR (1)
DEFINE resultado BOOLEAN
--DEFINE w ui.Window
--DEFINE f ui.Form
DEFINE idx SMALLINT 
DEFINE touchDet SMALLINT 
DEFINE cita     DATETIME YEAR TO SECOND
DEFINE requi    LIKE reqmreq.reqid
    
   LET w = ui.Window.getCurrent()
   LET f = w.getForm()

   CALL f.setElementHidden("gridListReg",1) -- 1 Ocultar           
   CALL f.setElementHidden("gridInput",0)   -- 0 Mostrar
   CALL f.setElementHidden("grupo11",1) -- 1 Ocultar  
   CALL f.setElementHidden("grupo9",1) -- SEGUIMEINTO
   CALL f.setElementHidden("lbl_caninfofam",1)
   CALL f.setElementHidden("reqmcan.caninfofam",1)  


   CALL combo_init()
   LET resultado = FALSE 
   LET u_reg.* = g_reg.*
   
  SELECT reqid, canid, canestado, canfecing, canproccant, canobserva,
         cannombre1, cannombre2, cannombre3, canapellido1, canapellido2, canapellidoc,
         cantipiden, canidentif, cangenero, cantrato, canfecnac, canestadocivil,
         canfecprocita, canfecprochora
    INTO requi, g_reg.*, cita
    FROM reqmcan
   WHERE canid = l_reg.canid

  IF disp_requi(requi) THEN
     CALL f.setElementHidden("groupRequi",0)
  END IF
     
  CALL f.setElementHidden("grupo7",0)
  DISPLAY BY NAME g_reg.*
  LET u_reg.* = g_reg.*

   CALL ui.ComboBox.setDefaultInitializer("cmb_canestado_1")


   CALL f.setElementHidden("grupo9",0)

   DIALOG ATTRIBUTES(UNBUFFERED)
     INPUT BY NAME g_reg.cli_result, g_reg.cli_observa 
      ATTRIBUTES (WITHOUT DEFAULTS)

      BEFORE INPUT
         DISPLAY BY NAME g_reg.canid_1, g_reg.canestado_1
         CALL DIALOG.setActionHidden("close",TRUE)

   END INPUT 
   
   ON ACTION ACCEPT

      IF g_reg.cli_observa IS NULL THEN
         CALL msg("Debe ingresar las obsevaciones de la evaluación.")
         NEXT FIELD cli_observa
      END IF 
        
      IF operacion = 'P' AND g_reg.* = u_reg.* THEN
         IF touchDet <> 1 THEN 
            CALL msg("No se efectuaron cambios")
            EXIT DIALOG 
         END IF 
      END IF
      
      CASE box_gradato("Seguro de grabar")
         WHEN "Si"
            LET resultado = TRUE
            EXIT DIALOG
         WHEN "No"
            EXIT DIALOG 
         OTHERWISE
            CONTINUE DIALOG 
      END CASE 
      LET resultado = TRUE
      EXIT DIALOG 

     ON ACTION CANCEL
        EXIT DIALOG
        
   END DIALOG

   CALL f.setElementHidden("grupo9",1)
       
   IF NOT resultado THEN
      LET g_reg.* = u_reg.*
      DISPLAY BY NAME g_reg.* 
   END IF 

    CALL f.setElementHidden("groupRequi",1)
    CALL f.setElementHidden("gridListReg",0)            
    CALL f.setElementHidden("gridInput",1)   
RETURN resultado 
END FUNCTION


FUNCTION disp_requi(num_req)
DEFINE num_req LIKE reqmreq.reqid
DEFINE req     RECORD LIKE reqmreq.*
DEFINE uor     RECORD LIKE glbuniorga.*
DEFINE afu     RECORD LIKE glbmareaf.*
DEFINE pue     RECORD LIKE glbmpue.*
DEFINE eta     RECORD LIKE reqmeta.*

     IF num_req > 0 THEN
         SELECT *
           INTO req.*
          FROM reqmreq
          WHERE reqid = num_req

        IF req.reqestado IS NOT NULL THEN

            LET reqetapa = req.reqetapa
            SELECT *
              INTO eta.*
              FROM reqmeta
             WHERE etaid = reqetapa

            IF eta.etaagregainfo = 2 THEN
                IF usuario = req.reqanalista THEN
                    LET usuanalis = TRUE
                ELSE
                    LET usuanalis = FALSE
                END IF
            END IF

            SELECT *
              INTO uor.*
              FROM glbuniorga
             WHERE uorid = req.pueuniorg

            SELECT * 
              INTO afu.*
              FROM glbmareaf
             WHERE afuid = uor.uorareaf

            SELECT *
              INTO pue.*
              FROM glbmpue
             WHERE pueid = req.pueposicion

            DISPLAY BY NAME req.reqid, req.reqfecrec, uor.uornombre,
                            afu.afunombre, pue.puenombre, req.reqestado,
                            req.jefenom
            RETURN TRUE
        ELSE
            LET uor.uornombre = '*** REQUISICION NO ENCONTRADA***'
             DISPLAY BY NAME req.reqid, uor.uornombre
             RETURN FALSE
        END IF
    ELSE
        LET requi = NULL
        LET w = ui.Window.getCurrent()
        LET f = w.getForm()

        CALL f.setElementHidden("groupRequi",1) -- 1 Ocultar  
        RETURN TRUE
    END IF
      
END FUNCTION