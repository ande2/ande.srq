IMPORT util

GLOBALS "reqp0322_glob.4gl"
 
FUNCTION consulta(flagConsulta)
DEFINE flagConsulta BOOLEAN 
DEFINE 
   rDet tRL,
   rReg  RECORD LIKE reqmcan.*,
   cita  CHAR(25),
   x,i,vcon INTEGER,
   consulta STRING  

   IF flagConsulta THEN 
      CALL encabezado("Consulta")

      CONSTRUCT condicion 
         ON canid, canestado
         FROM canid, canestado

         ON ACTION ACCEPT
            EXIT CONSTRUCT 
         ON ACTION CANCEL 
            CALL reg_det.clear()
            RETURN 0
             
      END CONSTRUCT 

   ELSE
      LET condicion = " 1=1 "
   END IF
   --Armar la consulta
   IF requi > 0 THEN
      LET consulta = 
          "SELECT  * ",
          " FROM reqmcan ",
          " WHERE reqid = ", requi,
          " AND canid > 0 AND ", condicion,
          " ORDER BY 1"
   ELSE
      LET consulta = 
          "SELECT * ",
          " FROM reqmcan ",
          " WHERE canid > 0 AND ", condicion,
          " AND canestado = 'D' ",
          " ORDER BY canfecprocita, canfecprochora"   
   END IF
   
   --definir cursor con consulta de BD
   DECLARE curDet CURSOR FROM consulta
   CALL reg_det.clear()
   LET x = 0
   --Llenar el arreglo con el resultado de la consulta
   FOREACH curDet INTO rReg.*
      LET cita = rReg.canfecprochora
      LET rDet.canid = rReg.canid
      LET rDet.nombre = rReg.cannombre1 CLIPPED, ' ', rReg.cannombre2 CLIPPED, ' ',
                        rReg.canapellido1 CLIPPED, ' ', rReg.canapellido2 CLIPPED
      LET rDet.cita   = rReg.canfecprocita USING "DD/MM/YYYY", cita[11,16]
      LET rDet.canestado = rReg.canestado
      LET x = x + 1 
      LET reg_det [x].* = rDet.*  
   END FOREACH

   
   RETURN x --Cantidad de registros
END FUNCTION
