IMPORT FGL req_combos

GLOBALS "reqp0322_glob.4gl"

MAIN
	DEFINE 
		n_param 	SMALLINT,
		prog_name2 	STRING

	DEFER INTERRUPT

	OPTIONS
		INPUT  WRAP,
		HELP KEY CONTROL-W,
		COMMENT LINE OFF,
		PROMPT LINE LAST - 2,
		MESSAGE LINE LAST - 1,
		ERROR LINE LAST

	LET n_param = num_args()
    IF n_param > 0 THEN
        LET usuario = ARG_VAL(1)
        --IF n_param > 1 THEN
        --    LET requi = ARG_VAL(2)
        --END IF

        LET prog_name2 = prog_name||".log"   -- El progr_name es definido como constante en el arch. globals
        
        CALL STARTLOG(prog_name2)


        CALL update_init()
        CALL combo_init()

        CALL main_init()
    END IF
END MAIN

FUNCTION main_init()
DEFINE 
	nom_forma 	STRING

    CALL ui.Interface.loadActionDefaults("actiondefaults_rh1")

    CLOSE WINDOW SCREEN 

    LET nom_forma = "reqp0322_form"
	OPEN WINDOW w1 WITH FORM nom_forma
    CALL fgl_settitle("ANDE - Clinica Médica")
    CALL ui.Window.getCurrent().setText("ANDE - Clinica Médica - "||nomUsuario(usuario))

	LET w = ui.WINDOW.getcurrent()
	LET f = w.getForm()

    CALL main_menu()
END FUNCTION

FUNCTION main_menu()
DEFINE varg1 CHAR (1),
    cuantos     SMALLINT,
    id, ids     SMALLINT,
    opcion      CHAR(1),
    salir       SMALLINT,
    txt_agregar STRING

    LET varg1 = arg_val(1)

    LET salir = FALSE
    CALL f.setElementHidden("gridInput",1)

    LET txt_agregar = 'Resultado consulta'
    DISPLAY "REGISTRO DE CANDIDATOS EN CLINICA MEDICA" TO gtit_enc

    WHILE NOT salir


        DISPLAY ARRAY reg_det TO sLR.*
           ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE,UNBUFFERED)

          BEFORE DISPLAY
             IF disp_requi(requi) THEN
                 LET cuantos = consulta(FALSE)
                 IF cuantos = 0 THEN 
                    CALL DIALOG.setActionActive("Resultado", 0)
                 END IF 
                 CALL encabezado("")
             ELSE
                CALL DIALOG.setActionActive("Resultado", 0)
             END IF
    
          BEFORE ROW 
             LET id = arr_curr()
             IF id > 0 THEN 
                LET l_reg.* = reg_det[id].*
             END IF 

          ON ACTION Resultado  ATTRIBUTES(TEXT=txt_agregar)
             LET opcion = "P"
             LET id = arr_curr()
             LET ids = scr_line()
             EXIT DISPLAY
      
           ON ACTION Salir
              LET opcion = 'S'
              EXIT DISPLAY 
        END DISPLAY 

        CASE opcion

          WHEN "P"
             DISPLAY "Resultado"
             IF id > 0 THEN 
                IF modifica(opcion) THEN
                   LET reg_det[id].* = l_reg.*
                END IF   
             END IF 
             CALL encabezado("")
           
            WHEN "S"
                LET salir = TRUE
        END CASE 
    END WHILE
END FUNCTION

FUNCTION encabezado(gtit_enc)
DEFINE gtit_enc STRING 

   DISPLAY BY NAME  gtit_enc
END FUNCTION 

FUNCTION combo_init()
   CALL combo_din2("trcid","SELECT trcid, trcnombre FROM reqmtrc ORDER BY trcnombre ")
   CALL combo_din2("trechid","SELECT trechid, trechnombre FROM reqmtrech ORDER BY trechnombre")
   CALL combo_din2("reqmcan.relgid",'SELECT relgid, relgnombre FROM reqrelig ORDER BY relgnombre')
   CALL combo_din2("cantdid",'SELECT cantdid, cantdnombre FROM reqmcantd ORDER BY cantdnombre')
   CALL combo_din2("trechid_h","SELECT trechid, trechnombre FROM reqmtrech ORDER BY trechnombre")
END FUNCTION 

FUNCTION combo_init_act()
   CALL combo_din2("trcid","SELECT trcid, trcnombre FROM reqmtrc WHERE trcactivo = 'S' ")
END FUNCTION 

FUNCTION def_hora(fecha, hora)
DEFINE diahora  LIKE reqmcan.canfecprochora
DEFINE hora_str STRING
DEFINE max_str  SMALLINT
DEFINE dt2 DATETIME YEAR TO MINUTE
DEFINE dt3 CHAR(50)
DEFINE dt4 CHAR(16)
DEFINE fecha    LIKE reqmcan.canfecprocita
DEFINE hora     LIKE reqmcan.canfecprochora

    LET hora_str = hora
    LET max_str  = hora_str.getLength()
    LET dt3 = hora_str
    LET dt4 = fecha USING "YYYY-MM-DD", dt3[11,max_str]
    LET dt2 = dt4
    LET diahora = dt2

RETURN diahora
END FUNCTION


FUNCTION tablas_temp()

    -- TABLA PARA DOCTOS TEMPORALES
    CREATE TEMP TABLE tmp_reqmcandoc
    (
         cdocid SMALLINT,
         cdocfecing DATE,
         cantdid INTEGER,
         cdocobserv VARCHAR(255),
         cdocarchdir VARCHAR(255),
         cdocarchiv BYTE
    )
 
END FUNCTION


FUNCTION cmb_canestado_1(cb)
DEFINE cb     ui.ComboBox
DEFINE cmb_dat tComboTex
DEFINE i SMALLINT

   CALL cmb_canestado_init() RETURNING cmb_dat.*
    
   CALL cb.clear()
   FOR i = 1 TO cmb_dat.cantidad
       CALL cb.addItem(cmb_dat.datos[i].id,cmb_dat.datos[i].nombre)
   END FOR
END FUNCTION

FUNCTION cmb_reqestado(cb)
DEFINE cb     ui.ComboBox
DEFINE cmb_dat tComboTex
DEFINE i SMALLINT

   CALL cmb_reqestado_init() RETURNING cmb_dat.*
    
   CALL cb.clear()
   FOR i = 1 TO cmb_dat.cantidad
       CALL cb.addItem(cmb_dat.datos[i].id,cmb_dat.datos[i].nombre)
   END FOR
END FUNCTION