DATABASE rh

GLOBALS
TYPE tDG RECORD
--Grupo1
    canid_1 LIKE reqmcan.canid,
    canestado_1 LIKE reqmcan.canestado,
    canfecing LIKE reqmcan.canfecing,
    canproccant_1 LIKE reqmcan.canproccant,
    canobserva LIKE reqmcan.canobserva,
--Grupo2
    cannombre1 LIKE reqmcan.cannombre1,
    cannombre2 LIKE reqmcan.cannombre2,
    cannombre3 LIKE reqmcan.cannombre3,
    canapellido1 LIKE reqmcan.canapellido1,
    canapellido2 LIKE reqmcan.canapellido2,
    canapellidoc LIKE reqmcan.canapellidoc,
--Grupo3
    cantipiden LIKE reqmcan.cantipiden,
    canidentif LIKE reqmcan.canidentif,
    cangenero LIKE reqmcan.cangenero,
    cantrato LIKE reqmcan.cantrato,
    canfecnac LIKE reqmcan.canfecnac,
    canestadocivil LIKE reqmcan.canestadocivil,
--Grupo4
    canemail LIKE reqmcan.canemail,
    cantelefono LIKE reqmcan.cantelefono,
    candireccion LIKE reqmcan.candireccion,
--Grupo5
    cannivelacademico LIKE reqmcan.cannivelacademico,
    cancarrera LIKE reqmcan.cancarrera,
    relgid LIKE reqmcan.relgid,
    cantrasporte LIKE reqmcan.cantrasporte,
    canfamilialab LIKE reqmcan.canfamilialab,
    caninfofam    LIKE reqmcan.caninfofam,
    canfamiliapub LIKE reqmcan.canfamiliapub,
    canexperilab LIKE reqmcan.canexperilab,
    cansalud LIKE reqmcan.cansalud,
--Grupo6
    canturnorota LIKE reqmcan.canturnorota,
    canfindesemana LIKE reqmcan.canfindesemana,
    canprocesant LIKE reqmcan.canprocesant,
    trcid LIKE reqmcan.trcid,
---grupo8
    canfecprocita LIKE reqmcan.canfecprocita,
    hora DATETIME HOUR TO MINUTE,
-- SIGUIMIENTO
    seguimiento   SMALLINT,
    trechid       LIKE reqmcan.trechid,
    canmotrechob  LIKE  reqmcan.canmotrechob
END RECORD
TYPE tRL RECORD
    canid LIKE reqmcan.canid,
    nombre VARCHAR(100),
    canestado LIKE reqmcan.canestado,
    cita   VARCHAR(20),
    canproccant LIKE reqmcan.canproccant
END RECORD

TYPE tDG_d RECORD
    cdocid      LIKE reqmcandoc.cdocid,
    cdocfecing  LIKE reqmcandoc.cdocfecing,
    cantdid     LIKE reqmcandoc.cantdid,
    cdocobserv  LIKE reqmcandoc.cdocobserv,
    cdocarchdir LIKE reqmcandoc.cdocarchdir
END RECORD

TYPE tRL_d RECORD
    cdocid     LIKE reqmcandoc.cdocid,
    cdocfecing  LIKE reqmcandoc.cdocfecing,
    cantdid     LIKE reqmcandoc.cantdid,
    cantdnombre LIKE reqmcantd.cantdnombre,
    cdocobserv LIKE reqmcandoc.cdocobserv,
    cdocarchdir LIKE reqmcandoc.cdocarchdir    
END RECORD

TYPE tRL_h RECORD
    rdccanfecoper VARCHAR(20),
    canproccant   LIKE reqdcan.canproccant,
    canestado     LIKE reqdcan.canestado,
    reqid         LIKE reqdcan.reqid,
    reqestado     LIKE reqdcan.reqestado,
    trechid_h     LIKE reqdcan.trechid
END RECORD

    DEFINE dbname STRING,
    w           ui.Window,
    f           ui.Form
    
    CONSTANT    prog_name = "reqp0312"

    ----  CANDIATOS
    DEFINE reg_det DYNAMIC ARRAY OF tRL
    DEFINE l_reg   tRL
    DEFINE u_reg, g_reg             tDG
    
    --- DOCUMENTOS
    DEFINE reg_det_d DYNAMIC ARRAY OF tRL_d
    DEFINE l_reg_d  tRL_d
    DEFINE u_reg_d, g_reg_d             tDG_d

    --- HISTORIAL
    DEFINE reg_det_h DYNAMIC ARRAY OF tRL_h
    
    --- OTROS
    DEFINE condicion STRING
    DEFINE usuario   LIKE commempl.id_commempl
    DEFINE requi     LIKE reqmreq.reqid
    DEFINE reqetapa  LIKE reqmreq.reqetapa
    DEFINE usuanalis SMALLINT
    DEFINE unicocan  SMALLINT

END GLOBALS