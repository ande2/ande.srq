
IMPORT security 
IMPORT util
IMPORT os

GLOBALS "reqp0312_glob.4gl"

FUNCTION insert_init()
DEFINE strSql STRING 

   LET strSql =
      "INSERT INTO reqmcan ( ",
        " canfecing,  canproccant, canobserva, ",
        " cannombre1, cannombre2,   cannombre3, canapellido1,   canapellido2, canapellidoc,",
        " cantipiden, canidentif,   cangenero,  cantrato,       canfecnac, canestadocivil,",
        " canemail,   cantelefono, candireccion, ",
        " cannivelacademico, cancarrera,   relgid, cantrasporte, canfamilialab, caninfofam,  canfamiliapub, canexperilab, cansalud, ",
        " canturnorota, canfindesemana,    canprocesant, trcid, ",
        " canfecprocita, canfecprochora,",
        " canestado, reqid, canusuoper, canfecoper",
        "   ) ",
      " VALUES (",
        "?,?,?,",
        "?,?, ?,?, ?,?,",
        "?,?, ?,?, ?,?,",
        "?,?, ?,",
        "?,?, ?,?,?,?, ?,?,?,",
        "?,?, ?,?,",
        "?,?,",
        "?,?,?,?",
      ") "

   PREPARE st_insertar FROM strSql

END FUNCTION 

FUNCTION ingreso(opcion)
DEFINE opcion CHAR(1)

   CALL encabezado("Ingresar")
   --CALL info_usuario()
   IF captura_datos(opcion) THEN
      RETURN grabar(opcion)
   ELSE 
      RETURN FALSE 
   END IF 
END FUNCTION 

FUNCTION grabar(opcion)
DEFINE opcion  CHAR(1)
DEFINE ahora   LIKE reqmcan.canfecoper
DEFINE cantdoc SMALLINT
DEFINE cita LIKE reqmcan.canfecprochora
DEFINE doc RECORD
         cdocid SMALLINT,
         cdocfecing DATE,
         cantdid INTEGER,
         cdocobserv VARCHAR(255),
         cdocarchdir VARCHAR(255),
         cdocarchiv BYTE
END RECORD

   LET g_reg.canestado_1 = 'A'
   IF requi IS NULL THEN
      LET g_reg.canproccant_1 = 0
   ELSE
      LET g_reg.canproccant_1 = 1
   END IF

   IF g_reg.canfecprocita >= TODAY THEN
      LET cita = def_hora(g_reg.canfecprocita, g_reg.hora)
   ELSE
      DISPLAY g_reg.canfecprocita
   END IF
   
   TRY 
      BEGIN WORK
      LET ahora = CURRENT YEAR TO MINUTE
      EXECUTE st_insertar USING 
            g_reg.canfecing, g_reg.canproccant_1, g_reg.canobserva,
            g_reg.cannombre1, g_reg.cannombre2, g_reg.cannombre3, g_reg.canapellido1, g_reg.canapellido2, g_reg.canapellidoc,
            g_reg.cantipiden, g_reg.canidentif, g_reg.cangenero, g_reg.cantrato, g_reg.canfecnac, g_reg.canestadocivil,
            g_reg.canemail, g_reg.cantelefono, g_reg.candireccion,
            g_reg.cannivelacademico, g_reg.cancarrera, g_reg.relgid, g_reg.cantrasporte, g_reg.canfamilialab, g_reg.caninfofam, g_reg.canfamiliapub, g_reg.canexperilab, g_reg.cansalud,
            g_reg.canturnorota, g_reg.canfindesemana, g_reg.canprocesant, g_reg.trcid,
            g_reg.canfecprocita, cita,
            g_reg.canestado_1, requi, usuario, ahora

       LET g_reg.canid_1 = SQLCA.sqlerrd[2]
       DISPLAY g_reg.canid_1
       LET cantdoc = 0
       SELECT COUNT(cdocid) INTO cantdoc
         FROM tmp_reqmcandoc
       IF cantdoc > 0 THEN

          DECLARE curcargadoc CURSOR FOR 
             SELECT cdocid, cantdid, cdocfecing, cdocobserv, cdocarchdir, cdocarchiv 
               FROM tmp_reqmcandoc
               
          LOCATE doc.cdocarchiv IN MEMORY 
          
          FOREACH curcargadoc INTO doc.cdocid, doc.cantdid, doc.cdocfecing, 
                                   doc.cdocobserv, doc.cdocarchdir, doc.cdocarchiv
                                   
             INSERT INTO reqmcandoc (canid, cantdid, cdocfecing,   
                                  cdocobserv, cdocarchdir,   cdocarchiv)
                VALUES (g_reg.canid_1, doc.cantdid, doc.cdocfecing,   
                        doc.cdocobserv, doc.cdocarchdir,   doc.cdocarchiv)
          END FOREACH 
          
       END IF
       COMMIT WORK
   CATCH 
      ROLLBACK WORK
      CALL msgError(sqlca.sqlcode,"Grabar Registro")
      RETURN FALSE 
   END TRY
   DISPLAY BY NAME g_reg.*
   CALL box_valdato ("Registro agregado")

   RETURN TRUE 
END FUNCTION 


FUNCTION insert_init_d()
DEFINE strSql STRING 

   LET strSql =
      "INSERT INTO reqmcandoc ( ",
        " cantdid, cdocfecing,   cdocobserv, cdocarchdir,   cdocarchiv, canid ",
        "   ) ",
      " VALUES (?,?,  ?,?,  ?,?)"

   PREPARE st_insertar_d FROM strSql

END FUNCTION 

FUNCTION ingreso_d()
   CALL encabezado("Ingresar")
   --CALL info_usuario()
   IF captura_datos_d('I') THEN
      RETURN grabar_d()
   ELSE 
      RETURN FALSE 
   END IF 
END FUNCTION 

FUNCTION grabar_d()

   DEFINE accion CHAR(1)
   DEFINE filename BIGINT 
   DEFINE mensaje STRING 
   DEFINE destino STRING
   DEFINE cdocarchiv LIKE reqmcandoc.cdocarchiv
   DEFINE maximo  SMALLINT

   LET g_reg_d.cdocfecing = TODAY
   ---- CARGA EL ARCHIVO


    TRY
       --LET filename = security.RandomGenerator.CreateRandomString(20)
       LET filename = util.Integer.abs(security.RandomGenerator.CreateRandomNumber())

       --DISPLAY "filename ", filename 
       LET destino = destino_archivo(filename)
       CALL fgl_getfile(g_reg_d.cdocarchdir, destino)
       --DISPLAY "traslade el arch ", g_doc.rdaNom, './files/'||filename
       --DISPLAY BY NAME g_doc.rdaId, g_doc.rdaNom
       --DISPLAY "desplegando nombre"
       LOCATE cdocarchiv IN FILE destino

        IF g_reg.canid_1 IS NOT NULL THEN
              LET g_reg_d.cdocarchdir = nospace(nombre_archivo(g_reg_d.cdocarchdir) CLIPPED)
              INSERT 
                INTO reqmcandoc (canid, cantdid, cdocfecing,   cdocobserv, cdocarchdir,   cdocarchiv)
              VALUES (g_reg.canid_1, g_reg_d.cantdid , g_reg_d.cdocfecing, g_reg_d.cdocobserv, g_reg_d.cdocarchdir,
                          cdocarchiv)
        ELSE

           LET maximo = 0

           SELECT MAX(cdocid) 
             INTO maximo
             FROM tmp_reqmcandoc
             
           IF maximo IS NULL THEN LET maximo = 0 END IF
           LET g_reg_d.cdocid = maximo + 1
           LET g_reg_d.cdocarchdir = nospace(nombre_archivo(g_reg_d.cdocarchdir) CLIPPED)
           
           INSERT
             INTO tmp_reqmcandoc
                    (cdocid, cantdid, cdocfecing, 
                     cdocobserv, cdocarchdir, cdocarchiv)
             VALUES (g_reg_d.cdocid, g_reg_d.cantdid, g_reg_d.cdocfecing, 
                     g_reg_d.cdocobserv, g_reg_d.cdocarchdir, cdocarchiv)
        END IF

    CATCH
       LET mensaje = sqlca.sqlcode, " ", sqlca.sqlerrm  # Catch runtime execution errors from the SQLCA diagnostic record
       ERROR mensaje
       CALL msgError(mensaje,"Grabar Registro")
       RETURN FALSE 
    END TRY

    --DISPLAY BY NAME g_reg_d.*
    CALL box_valdato ("Registro agregado")
RETURN TRUE 
END FUNCTION 

FUNCTION nombre_archivo(archivo)
DEFINE archivo  STRING
DEFINE result STRING

    IF isGDC() THEN
        LET result = os.Path.basename(archivo)
    ELSE
        LET result = archivo 
    END IF
    
RETURN result
END FUNCTION

FUNCTION destino_archivo(archivo)
DEFINE archivo, ruta STRING

    LET ruta = '.',os.Path.separator(),'files',os.Path.separator(),archivo

RETURN ruta
END FUNCTION

FUNCTION mostrar_d()
DEFINE docto   BYTE
DEFINE origen  STRING
DEFINE destino STRING
DEFINE result  SMALLINT

     IF l_reg_d.cdocid > 0 THEN
         LET origen = '.',os.Path.separator(),'files',os.Path.separator(),nospace(l_reg_d.cdocarchdir CLIPPED)

         IF isGDC() THEN
            LET destino = winopendir('C:','')
            LET destino = destino.trim(),'/',nospace(l_reg_d.cdocarchdir CLIPPED)
         ELSE
            LET destino = nospace(l_reg_d.cdocarchdir CLIPPED)
         END IF

         IF destino IS NOT NULL THEN
             TRY
                 LOCATE docto IN FILE origen
                 IF g_reg.canid_1 IS NULL THEN
                    SELECT cdocarchiv INTO docto FROM tmp_reqmcandoc WHERE cdocid = l_reg_d.cdocid
                 ELSE
                    SELECT cdocarchiv INTO docto FROM reqmcandoc WHERE canid = g_reg.canid_1 AND cdocid  = l_reg_d.cdocid
                 END IF
                 CALL fgl_putfile( origen , destino)
                 CALL ui.interface.frontcall("standard","ShellExec",destino,[result])
             CATCH
                CALL msgError(sqlca.sqlcode,"Descargar el archivo")
             END TRY
         END IF
     END IF
END FUNCTION