DATABASE rh

GLOBALS
TYPE tDG RECORD
    cantdid      LIKE reqmcantd.cantdid,
    cantdnombre  LIKE reqmcantd.cantdnombre,
    cantdactivo  LIKE reqmcantd.cantdactivo,
    cantdpriv    LIKE reqmcantd.cantdpriv
END RECORD

    DEFINE dbname STRING
    CONSTANT    prog_name = "reqm0316"

    DEFINE reg_det DYNAMIC ARRAY OF tDG
    DEFINE u_reg, g_reg             tDG
    DEFINE condicion STRING
    
END GLOBALS