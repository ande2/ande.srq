DATABASE rh

GLOBALS
TYPE tDG RECORD
    trechid      LIKE reqmtrech.trechid,
    trechnombre  LIKE reqmtrech.trechnombre,
    trechobserva LIKE reqmtrech.trechobserva
END RECORD

    DEFINE dbname STRING
    CONSTANT    prog_name = "reqm0314"

    DEFINE reg_det DYNAMIC ARRAY OF tDG
    DEFINE u_reg, g_reg             tDG
    DEFINE condicion STRING
    
END GLOBALS