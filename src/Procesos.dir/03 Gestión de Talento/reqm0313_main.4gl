

GLOBALS "reqm0313_glob.4gl"

MAIN
	DEFINE 
		n_param 	SMALLINT,
		prog_name2 	STRING

	DEFER INTERRUPT

	OPTIONS
		INPUT  WRAP,
		HELP KEY CONTROL-W,
		COMMENT LINE OFF,
		PROMPT LINE LAST - 2,
		MESSAGE LINE LAST - 1,
		ERROR LINE LAST

	LET n_param = num_args()

	LET prog_name2 = prog_name||".log"   -- El progr_name es definido como constante en el arch. globals
    
	CALL STARTLOG(prog_name2)

	CALL insert_init()
    CALL update_init()
    CALL delete_init()
    --CALL combo_init()
	CALL main_init()
END MAIN

FUNCTION main_init()
DEFINE 
	nom_forma 	STRING,
    w           ui.Window,
    f           ui.Form

    CALL ui.Interface.loadActionDefaults("actiondefaults_rh1")

    CLOSE WINDOW SCREEN 

    LET nom_forma = "reqm0313_form"
	OPEN WINDOW w1 WITH FORM nom_forma
    CALL fgl_settitle("ANDE - Tipo de Reclutamiento de Candidatos")

	LET w = ui.WINDOW.getcurrent()
	LET f = w.getForm()

    CALL main_menu()
END FUNCTION

FUNCTION main_menu()
DEFINE varg1 CHAR (1),
    cuantos     SMALLINT,
    id, ids     SMALLINT
    
    LET varg1 = arg_val(1)

    DISPLAY "REGISTRO DE TIPOS DE RECLUTAMIENTO" TO gtit_enc

    DISPLAY ARRAY reg_det TO sLR.*
       ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE,UNBUFFERED)

      BEFORE DISPLAY
         LET cuantos = consulta(false)
         IF cuantos > 0 THEN 
            CALL dialog.setCurrentRow("sLR",1)
            LET g_reg.* = reg_det[1].*
            DISPLAY BY NAME g_reg.*
         END IF 
         CALL encabezado("")

      BEFORE ROW 
         LET id = arr_curr()
         IF id > 0 THEN 
            LET g_reg.* = reg_det[id].*
            DISPLAY BY NAME g_reg.*
         END IF 
         
      ON ACTION agregar
         IF ingreso() THEN 
            LET cuantos = consulta(FALSE)
            CALL fgl_set_arr_curr( arr_count() + 1 )
            --Refrescar Pantalla
            DISPLAY ARRAY reg_det TO sDG.*
               BEFORE DISPLAY  EXIT DISPLAY 
            END DISPLAY 
         END IF
         CALL encabezado("")

      ON ACTION modificar
         LET id = arr_curr()
         LET ids = scr_line()
         IF id > 0 THEN 
            IF modifica() THEN
               LET reg_det[id].* = g_reg.*
               DISPLAY reg_det[id].* TO sDG[ids].*
            END IF   
         END IF 
         CALL encabezado("")

      ON ACTION anular
         LET id = arr_curr()
         LET ids = scr_line()         
         IF id > 0 THEN 
            IF anular() THEN
               CALL DIALOG.deleteRow("sLR", id)
               IF id = arr_count() THEN 
                  LET id = id - 1
               END IF 
               IF id > 0 THEN
                  LET g_reg.* = reg_det[id].*
               ELSE 
                  INITIALIZE g_reg.* TO NULL
               END IF 
               DISPLAY BY NAME g_reg.*

            END IF   
         END IF 
         
       ON ACTION salir
          EXIT DISPLAY 
    END DISPLAY 
END FUNCTION

FUNCTION encabezado(gtit_enc)
DEFINE gtit_enc STRING 

   DISPLAY BY NAME  gtit_enc
END FUNCTION 
