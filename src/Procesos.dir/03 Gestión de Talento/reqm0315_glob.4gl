DATABASE rh

GLOBALS
TYPE tDG RECORD
    relgid      LIKE reqrelig.relgid,
    relgnombre  LIKE reqrelig.relgnombre,
    relgobserba LIKE reqrelig.relgobserba
END RECORD

    DEFINE dbname STRING
    CONSTANT    prog_name = "reqm0315"

    DEFINE reg_det DYNAMIC ARRAY OF tDG
    DEFINE u_reg, g_reg             tDG
    DEFINE condicion STRING
    
END GLOBALS