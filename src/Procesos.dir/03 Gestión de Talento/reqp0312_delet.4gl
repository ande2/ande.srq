GLOBALS "reqp0312_glob.4gl"


FUNCTION delete_init()
DEFINE strSql STRING 

   LET strSql =
      "DELETE FROM reqmcan ",
      "WHERE canid = ? "
      
   PREPARE st_delete FROM strSql

   LET strSql =
      "UPDATE reqmcan SET canestado = 0 ",
        " WHERE canid = ? "        

   PREPARE st_anular FROM strSql

END FUNCTION 

FUNCTION eliminar()
DEFINE vresult SMALLINT
DEFINE oper CHAR(1) 
DEFINE mensaje STRING

   LET oper = 'E'
   LET mensaje = 'Al eliminar también se borrarn los documentos agregados. ¿Desea continuar?'
   
   IF NOT box_confirma(mensaje) THEN
      RETURN FALSE
   END IF
   
   TRY
      BEGIN WORK
      IF oper='A' THEN 
         EXECUTE st_anular USING l_reg.canid
      ELSE 
         
         DELETE FROM reqmcandoc WHERE canid = l_reg.canid

         DELETE FROM reqdcan WHERE canid = l_reg.canid
         
         EXECUTE st_delete USING l_reg.canid
      END IF        
      COMMIT WORK
   CATCH 
      ROLLBACK WORK
      CALL msgError(sqlca.sqlcode,"Eliminar Registro")
      RETURN FALSE 
   END TRY
   
   CALL msg("Registro eliminado")
   
   RETURN TRUE 
END FUNCTION 

FUNCTION delete_init_d()
DEFINE strSql STRING 

   LET strSql =
      "DELETE FROM reqmcandoc ",
      "WHERE canid = ? ",
      " AND cdocid = ? "
      
   PREPARE st_delete FROM strSql

   LET strSql =
      "UPDATE reqmcandoc SET estado = 0 ",
        " WHERE cantdid = ? "        

   PREPARE st_anular FROM strSql

END FUNCTION 

FUNCTION eliminar_d()
DEFINE vresult SMALLINT
DEFINE oper CHAR(1) 
DEFINE mensaje STRING


   IF g_reg.canestado_1 = 'A' THEN
       LET oper = 'E'
       LET mensaje = 'Al eliminar se borrará el archivo de la base de datos.'
       
       IF NOT box_confirma(mensaje) THEN
          RETURN FALSE
       END IF
       
       TRY
          IF oper='A' THEN 
             EXECUTE st_anular USING g_reg.canid_1, l_reg_d.cdocid
          ELSE 
             EXECUTE st_delete USING g_reg.canid_1, l_reg_d.cdocid
          END IF 
       CATCH 
          CALL msgError(sqlca.sqlcode,"Eliminar Registro")
          RETURN FALSE 
       END TRY
       
       CALL msg("Registro eliminado")
   ELSE
       CALL msgError("No se puede eliminar por el estado actual del candidato","Eliminar Registro")
       RETURN FALSE
   END IF
   
   RETURN TRUE 
END FUNCTION 
