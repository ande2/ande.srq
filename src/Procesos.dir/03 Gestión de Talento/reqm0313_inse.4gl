GLOBALS "reqm0313_glob.4gl"

FUNCTION insert_init()
DEFINE strSql STRING 

   LET strSql =
      "INSERT INTO reqmtrc ( ",
        " trcnombre, trcactivo ",
        "   ) ",
      " VALUES (?,?)"

   PREPARE st_insertar FROM strSql

END FUNCTION 

FUNCTION ingreso()
   CALL encabezado("Ingresar")
   --CALL info_usuario()
   IF captura_datos('I') THEN
      RETURN grabar()
   ELSE 
      RETURN FALSE 
   END IF 
END FUNCTION 

FUNCTION grabar()
   DEFINE i SMALLINT
   DEFINE mensaje STRING
   
   TRY 
      EXECUTE st_insertar USING 
            g_reg.trcnombre, g_reg.trcactivo
   CATCH 
      LET mensaje = sqlca.sqlcode, ' ', sqlca.sqlerrm
      CALL msgError(mensaje,"Grabar Registro")
      RETURN FALSE 
   END TRY
   DISPLAY BY NAME g_reg.*
   CALL box_valdato ("Registro agregado")

   RETURN TRUE 
END FUNCTION 