GLOBALS "rvtaporcli_glob.4gl"

FUNCTION run_report10_to_handler(handler)
    DEFINE
        data report10_data,
        handler om.SaxDocumentHandler,
        qrytxt STRING,
        qrytxt_header STRING,        
        nommes VARCHAR(20,1),
        existe INTEGER

   LET qrytxt_header = "SELECT YEAR(e1.fecemi) aniofac,MONTH(e1.fecemi) mesfac, ' ' nommes,",
                 "DAY(e1.fecemi) diafac ,e1.fecemi,TRIM(e1.nserie)||'-'||e1.numdoc factura, ",
                 "e1.totdoc, ",
                 "e1.codcli, e1.numnit, e1.nomcli, ",
                 "c1.dircli,c1.numtel"
   LET qrytxt =
                 "FROM fac_mtransac e1, OUTER fac_clientes c1 ",
                 "WHERE e1.fecemi between '",w_datos.fecini,"' and '",w_datos.fecfin,"' ",
                 "and e1.estado = 'V' "
    --Para NIT             
    IF w_datos.numnit IS NOT NULL THEN
        LET qrytxt = qrytxt CLIPPED,
                    " AND e1.numnit matches '",w_datos.numnit CLIPPED,"' "
    END IF

    --Para Nombre
    IF w_datos.nomcli IS NOT NULL THEN
        LET qrytxt = qrytxt CLIPPED,
                    " AND e1.nomcli matches '*", w_datos.nomcli CLIPPED,"*' "
    END IF
    

    LET qrytxt = qrytxt CLIPPED,
                 " and c1.codcli = e1.codcli "

   

   LET data.parametros.fecini = w_datos.fecini
   LET data.parametros.fecfin = w_datos.fecfin
   LET data.parametros.numnit = w_datos.numnit
   LET data.parametros.nomcli = w_datos.nomcli
   LET data.parametros.tipsal = w_datos.tipsal

   IF data.parametros.numnit IS NULL THEN
      LET data.parametros.numnit = "TODOS"
   END IF

   LET data.parametros.fecha_rep = TODAY;
   LET data.parametros.hora_rep = TIME;
   LET existe = 0;

   PREPARE c_rep010_count FROM "SELECT COUNT(*) "||qrytxt CLIPPED
   EXECUTE c_rep010_count INTO existe;

   IF existe > 0 THEN

      LET nombre = "rvtapordoc"
    
      WHENEVER ERROR CONTINUE
      IF fgl_report_loadCurrentSettings(nombre||".4rp") THEN
         CALL fgl_report_selectDevice(w_datos.tipsal)
         CALL fgl_report_selectPreview(1)
         IF w_datos.tipsal <> "Image" THEN
            CALL fgl_report_setOutputFileName("/tmp/"||nombre CLIPPED||"."||w_datos.tipsal)
         ELSE
            CALL fgl_report_configureImageDevice(0,0,0,1,NULL,"jpg","/tmp",nombre,300)
         END IF
         LET handler = fgl_report_commitCurrentSettings()
      ELSE
         ERROR "No se encuentra el formato "||nombre CLIPPED ||".4rp"
      END IF
      WHENEVER ERROR STOP
      IF handler IS NOT NULL THEN
   
         LET qrytxt = qrytxt CLIPPED, 
               " ORDER BY e1.numnit, 1, 2 "
         PREPARE c_rep010 FROM qrytxt_header CLIPPED ||" "||qrytxt CLIPPED 
         DECLARE c_crep010 CURSOR FOR c_rep010
   
         START REPORT report10_report TO XML HANDLER HANDLER
         FOREACH c_crep010 INTO data.imp1.*
            LET existe = TRUE
            LET data.imp1.nomanio = "A�O: ",DATA.imp1.aniofac
            LET data.imp1.nommes = "MES: "|| nomMes(data.imp1.mesfac)
            IF data.imp1.numtel IS NULL THEN
               LET data.imp1.numtel = "SIN TELEFONO"
            END IF
            IF data.imp1.dircli IS NULL THEN
               LET data.imp1.dircli = "."
            END IF
            LET data.imp1.titcli = "CLIENTE: "||data.imp1.numnit CLIPPED ||" / "||data.imp1.nomcli CLIPPED||" / "||data.imp1.dircli CLIPPED||" / "||data.imp1.numtel CLIPPED
            OUTPUT TO REPORT report10_report(data.*)
         END FOREACH
         FINISH REPORT report10_report
         CLOSE c_crep010 
         FREE  c_crep010
      END IF
   ELSE 
    
      ERROR "" 
      CALL fgl_winmessage(" Atencion","No existen datos con el filtro seleccionado.","stop") 
   END IF
   
END FUNCTION 

REPORT report10_report(data)
    DEFINE
        data report10_data,
        totalcli LIKE fac_mtransac.totdoc,
        totalanio LIKE fac_mtransac.totdoc,
        totalmes LIKE fac_mtransac.totdoc,
        totalgen LIKE fac_mtransac.totdoc
        
    ORDER EXTERNAL BY 
        data.imp1.numnit , data.imp1.aniofac, data.imp1.mesfac
   
    FORMAT
      FIRST PAGE HEADER
         LET totalgen = 0;
         LET totalcli = 0;
         LET totalanio = 0;
         LET totalmes = 0;

      BEFORE GROUP OF data.imp1.numnit
         LET totalcli = 0;

      BEFORE GROUP OF data.imp1.aniofac
         LET totalanio = 0;

      BEFORE GROUP OF data.imp1.mesfac
         LET totalmes = 0;
         
      ON EVERY ROW
         LET totalmes = totalmes + data.imp1.valfac;
         LET totalanio = totalanio + data.imp1.valfac;
         LET totalcli = totalcli + data.imp1.valfac;
         LET totalgen = totalgen + data.imp1.valfac;
         LET data.imp1.totalmes = totalmes;
         LET data.imp1.totalanio = totalanio;
         LET data.imp1.totalcli = totalcli;
         LET data.imp1.totalgen = totalgen;
         PRINTX data.*
END REPORT