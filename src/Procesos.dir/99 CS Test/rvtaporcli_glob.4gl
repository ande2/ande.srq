DATABASE segovia 

{ Definicion de variables globales }
GLOBALS
TYPE report10_data RECORD
   parametros RECORD
      fecini DATE,
      fecfin DATE,
      numnit  VARCHAR(20,1),
      nomcli  VARCHAR(100,1),
      tipsal  VARCHAR(20,1),
      fecha_rep DATE,
      hora_rep DATETIME HOUR TO SECOND
   END RECORD,
   imp1  RECORD
        aniofac SMALLINT,
        mesfac SMALLINT,
        nommes VARCHAR(20,1),
        diafac SMALLINT,
        fecemi  DATE,
        factura VARCHAR(30,1),
        valfac LIKE fac_mtransac.totdoc,
        codcli LIKE fac_mtransac.codcli, 
        numnit LIKE fac_mtransac.numnit, 
        nomcli LIKE fac_mtransac.nomcli,
        dircli LIKE fac_clientes.dircli,
        numtel LIKE fac_clientes.numtel,
        nomanio STRING,
        titcli STRING,
        totalcli LIKE fac_mtransac.totdoc,
        totalanio LIKE fac_mtransac.totdoc,
        totalmes LIKE fac_mtransac.totdoc,
        totalgen LIKE fac_mtransac.totdoc
    END RECORD
       
END RECORD

DEFINE w_datos RECORD
   fecini DATE,
   fecfin DATE,
   numnit  VARCHAR(20,1),
   nomcli  VARCHAR(100,1),
   tipsal  VARCHAR(20,1)
END RECORD

DEFINE nombre   STRING

END GLOBALS