
GLOBALS "reqm0317_glob.4gl"
  
FUNCTION captura_datos(operacion)
DEFINE operacion CHAR (1)
DEFINE resultado BOOLEAN
DEFINE w ui.Window
DEFINE f ui.Form
DEFINE idx SMALLINT 
DEFINE touchDet SMALLINT 
DEFINE cuantos  SMALLINT
DEFINE cambios  SMALLINT
DEFINE rflu_ant DYNAMIC ARRAY OF tDGF
LET w = ui.Window.getCurrent()
LET f = w.getForm()

   LET resultado = FALSE 
   LET cambios   = FALSE
   LET u_reg.* = g_reg.*
   IF operacion = 'I' THEN 
      
      INITIALIZE g_reg.* TO NULL
      DISPLAY BY NAME g_reg.*
      CALL reg_flu.clear()
   ELSE
      LET cuantos = consulta_f()
   END IF
   
    DISPLAY ARRAY reg_flu TO sDGF.*
      BEFORE DISPLAY EXIT DISPLAY
    END DISPLAY

   
   DIALOG ATTRIBUTES(UNBUFFERED)
     INPUT BY NAME  
      g_reg.etanombre, g_reg.etaorden, g_reg.etaestadoreq, g_reg.etaabccandid, 
      g_reg.etaagregainfo, g_reg.etatipo, g_reg.dias, g_reg.horas
      ATTRIBUTES (WITHOUT DEFAULTS)

      BEFORE INPUT
         CALL DIALOG.setActionHidden("close",TRUE)

      ON ACTION flujo ATTRIBUTES(TEXT="Flujo")
         CALL rflu_ant.clear()
         CALL reg_flu.copyTo(rflu_ant)
         IF captura_datos_f(operacion) THEN
            LET cambios = TRUE
         ELSE
            CALL reg_flu.clear()
            CALL rflu_ant.copyTo(reg_flu)
            DISPLAY ARRAY reg_flu TO sDGF.*
                BEFORE DISPLAY EXIT DISPLAY
            END DISPLAY
         END IF
      
   END INPUT 
   
   ON ACTION ACCEPT
      
        IF g_reg.etanombre IS NULL THEN
           CALL msg("Debe ingresar nombre")
           NEXT FIELD CURRENT
        END IF  

      IF operacion = 'M' AND g_reg.* = u_reg.* AND NOT cambios THEN
         IF touchDet <> 1 THEN 
            CALL msg("No se efectuaron cambios")
            EXIT DIALOG 
         END IF 
      END IF
      
      CASE box_gradato("Seguro de grabar")
         WHEN "Si"
            LET resultado = TRUE
            EXIT DIALOG
         WHEN "No"
            EXIT DIALOG 
         OTHERWISE
            CONTINUE DIALOG 
      END CASE 
      LET resultado = TRUE
      EXIT DIALOG 

     ON ACTION CANCEL
        EXIT DIALOG
        
   END DIALOG
   
   IF NOT resultado THEN
      LET g_reg.* = u_reg.*
      DISPLAY BY NAME g_reg.* 
   END IF 
   
RETURN resultado 
END FUNCTION


FUNCTION captura_datos_f(operacion)
DEFINE operacion CHAR (1)
DEFINE resultado BOOLEAN

    LET resultado = FALSE

    DIALOG ATTRIBUTES(UNBUFFERED)

    INPUT ARRAY reg_flu FROM sDGF.* ATTRIBUTES(INSERT ROW=FALSE)

    END INPUT
    
    ON ACTION ACCEPT
        LET resultado = TRUE
        EXIT DIALOG
        
    ON ACTION CANCEL
        EXIT DIALOG
        
    END DIALOG
    
RETURN resultado
END FUNCTION 