GLOBALS "reqm0319_glob.4gl"

FUNCTION insert_init()
DEFINE strSql STRING 

   LET strSql =
      "INSERT INTO reqmuap ( ",
        " etaid, uapusuario, uaptipo ",
        "   ) ",
      " VALUES (?,?,?)"

   PREPARE st_insertar FROM strSql

END FUNCTION 

FUNCTION ingreso()
   CALL encabezado("Ingresar")
   --CALL info_usuario()
   IF captura_datos('I') THEN
      RETURN grabar()
   ELSE 
      RETURN FALSE 
   END IF 
END FUNCTION 

FUNCTION grabar()
   
   TRY 
      EXECUTE st_insertar USING 
            g_reg.etaid, g_reg.uapusuario, g_reg.uaptipo
   CATCH 
      CALL msgError(sqlca.sqlcode,"Grabar Registro")
      RETURN FALSE 
   END TRY
   DISPLAY BY NAME g_reg.*
   CALL box_valdato ("Registro agregado")

   RETURN TRUE 
END FUNCTION 