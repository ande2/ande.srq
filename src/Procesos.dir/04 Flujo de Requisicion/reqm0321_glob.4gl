DATABASE rh

GLOBALS
TYPE tDG RECORD
    feriaid      LIKE glbmfer.feriaid,
    ferianombre  LIKE glbmfer.ferianombre,
    feriatiempo LIKE glbmfer.feriatiempo
END RECORD

    DEFINE dbname STRING
    CONSTANT    prog_name = "reqm0321"

    DEFINE reg_det DYNAMIC ARRAY OF tDG
    DEFINE u_reg, g_reg             tDG
    DEFINE condicion STRING
    
END GLOBALS