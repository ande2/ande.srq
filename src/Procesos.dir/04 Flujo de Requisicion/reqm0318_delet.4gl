GLOBALS "reqm0318_glob.4gl"


FUNCTION delete_init()
DEFINE strSql STRING 

   LET strSql =
      "DELETE FROM reqmacc ",
      "WHERE accid = ? "
      
   PREPARE st_delete FROM strSql

   LET strSql =
      "UPDATE reqmacc SET estado = 0 ",
        " WHERE accid = ? "        

   PREPARE st_anular FROM strSql

END FUNCTION 

FUNCTION anular()
DEFINE vresult SMALLINT
DEFINE oper CHAR(1) 
DEFINE mensaje STRING


   LET oper = 'E'
   LET mensaje = 'Al anular, eliminará el registro'
   
   IF NOT box_confirma(mensaje) THEN
      RETURN FALSE
   END IF
   
   TRY
      IF oper='A' THEN 
         EXECUTE st_anular USING g_reg.accid
      ELSE 
         EXECUTE st_delete USING g_reg.accid
      END IF 
   CATCH 
      CALL msgError(sqlca.sqlcode,"Eliminar Registro")
      RETURN FALSE 
   END TRY
   
   CALL msg("Registro eliminado")
   
   RETURN TRUE 
END FUNCTION 
