
GLOBALS "reqp0309_glob.4gl"

FUNCTION consulta(flagConsulta)
DEFINE flagConsulta BOOLEAN 
DEFINE consulta  STRING
DEFINE x         SMALLINT
DEFINE rDet tDGR
DEFINE condicion STRING
DEFINE fdl, fal  DATE
   LET x = 0

   IF flagConsulta THEN
       LET fdl = condi_fdl
       LET fal = condi_fal
       
       DIALOG ATTRIBUTES(UNBUFFERED)
         INPUT BY NAME  
          condi_fdl, condi_fal
          ATTRIBUTES (WITHOUT DEFAULTS)

          BEFORE INPUT
             CALL DIALOG.setActionHidden("close",TRUE)          
       END INPUT 
       
       ON ACTION ACCEPT

          IF condi_fdl >  condi_fal THEN
            CALL msg("La fecha 'DEL' debe ser menor a la fecha de 'AL' ")
            CONTINUE DIALOG
          END IF
          CALL reg_det.clear()
          CALL reg_req.clear()
          INITIALIZE gb_reg.* TO NULL
          INITIALIZE u_reg.* TO NULL
          INITIALIZE g_reg.* TO NULL
          INITIALIZE ur_reg.* TO NULL
          INITIALIZE gr_reg.* TO NULL
          LET permiso.vActualiza = FALSE
          LET permiso.vProcesar = FALSE
          LET permiso.vSoloVer = FALSE
          EXIT DIALOG 

         ON ACTION CANCEL
            LET condi_fdl = fdl
            LET condi_fal = fal
            EXIT DIALOG
            
       END DIALOG
   
   END IF
   
   LET condicion = ' 1 = 1 '
   --Armar la consulta
   LET consulta = 
      "SELECT  r.reqid, r.reqfecrec, r.reqmotivo, r.reqetapa, r.reqestado, ", 
      " u.uornombre, a.afunombre, p.puenombre, ", 
      " r.reqdigitador, r.reqususol, r.requsuaut, r.gerfunaut ", 
      " FROM reqmreq r, glbuniorga u, glbmareaf a, glbmpue p", 
      " WHERE r.reqid > 0 ", 
      " AND r.reqfecrec BETWEEN '", condi_fdl USING "DD/MM/YYYY", "' AND '", condi_fal USING "DD/MM/YYYY", "' ", 
      " AND p.pueid = r.pueposicion AND u.uorid = r.pueuniorg AND a.afuid = u.uorareaf ", 
      " AND (", usuario.ID, " IN (r.reqdigitador, r.reqususol, r.requsuaut, r.gerfunaut) ",
      "       OR ", usuario.ID, " IN (r.reqanalista) ) ", 
      " UNION ", 
      " SELECT  r.reqid, r.reqfecrec, r.reqmotivo, r.reqetapa, r.reqestado, ", 
      " u.uornombre, a.afunombre, p.puenombre, ", 
      " r.reqdigitador, r.reqususol, r.requsuaut, r.gerfunaut ", 
      " FROM reqmreq r, glbuniorga u, glbmareaf a, glbmpue p", 
      " WHERE r.reqid > 0 ", 
      " AND r.reqfecrec BETWEEN '", condi_fdl USING "DD/MM/YYYY", "' AND '", condi_fal USING "DD/MM/YYYY", "' ", 
      " AND p.pueid = r.pueposicion AND u.uorid = r.pueuniorg AND a.afuid = u.uorareaf ", 
      " AND ( r.gerfunaut IS NULL OR ", usuario.ID, " NOT IN (r.gerfunaut) ) ", 
      " AND ( r.requsuaut IS NULL OR ", usuario.ID, " NOT IN (r.requsuaut) ) ", 
      " AND ",usuario.ID, " NOT IN (r.reqdigitador, r.reqususol) ", 
      " AND r.reqetapa IN ( ", condi_eta ,") ", 
      " ORDER BY 1" 
      --"SELECT  r.reqid, r.reqfecrec, r.reqmotivo, r.reqetapa, r.reqestado, ",
      --" u.uornombre, a.afunombre, p.puenombre, ",
      --" r.reqdigitador, r.reqususol, r.requsuaut, r.gerfunaut ",
      --" FROM reqmreq r, glbuniorga u, glbmareaf a, glbmpue p",
      --" WHERE r.reqid > 0 ",
      --" AND r.reqfecrec BETWEEN '", condi_fdl USING "DD/MM/YYYY", "' AND '", condi_fal USING "DD/MM/YYYY", "' ",
      --" AND p.pueid = r.pueposicion AND u.uorid = r.pueuniorg AND a.afuid = u.uorareaf ",
      --" AND ( ", usuario.ID, " IN (r.reqdigitador, r.reqususol, r.requsuaut, r.gerfunaut) ",
      --    " OR ( ",usuario.ID, " NOT IN (r.reqdigitador, r.reqususol, r.requsuaut, r.gerfunaut) ",
      --           " AND r.reqetapa IN ( ", condi_eta ,") ",
      --         " ) ",
      --      " ) ",
      --" ORDER BY 1"
   
   --definir cursor con consulta de BD
   DECLARE curDetR CURSOR FROM consulta
   CALL reg_req.clear()
   CALL reg_det.clear()
   LET x = 0
   --Llenar el arreglo con el resultado de la consulta
   FOREACH curDetR INTO rDet.*
      LET x = x + 1 
      LET reg_req[x].* = rDet.*  
   END FOREACH

   
   RETURN x --Cantidad de registros
END FUNCTION
 
FUNCTION consulta_b(flagConsulta)
DEFINE flagConsulta BOOLEAN 
DEFINE 
   rDet tLR,
   x,i,vcon INTEGER,
   consulta STRING  

   IF flagConsulta THEN 
      CALL encabezado("Consulta")

      CONSTRUCT condicion 
         ON bitid, etaidini, accid
         FROM bitid, etaidini, accid

         ON ACTION ACCEPT
            EXIT CONSTRUCT 
         ON ACTION CANCEL 
            CALL reg_det.clear()
            RETURN 0
             
      END CONSTRUCT 

   ELSE
      LET condicion = " 1=1 "
   END IF
   --Armar la consulta
   LET consulta = 
      "SELECT  bitid, etaidfin, bitfecoper, accid, bitobserva ",
      " FROM reqmbit ",
      " WHERE reqid = ", gr_reg.reqid,
      " AND ", condicion,
      " ORDER BY 1 DESC "
   
   --definir cursor con consulta de BD
   DECLARE curDet CURSOR FROM consulta
   CALL reg_det.clear()
   LET x = 0
   --Llenar el arreglo con el resultado de la consulta
   FOREACH curDet INTO rDet.*
      LET x = x + 1 
      LET reg_det [x].* = rDet.* 
   END FOREACH

   DISPLAY ARRAY reg_det TO sLRB.*
       BEFORE DISPLAY EXIT DISPLAY
   END DISPLAY
   
   RETURN x --Cantidad de registros
END FUNCTION

