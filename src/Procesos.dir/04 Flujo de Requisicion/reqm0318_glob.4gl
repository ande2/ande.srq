DATABASE rh

GLOBALS
TYPE tDG RECORD
    accid      LIKE reqmacc.accid,
    accnombre  LIKE reqmacc.accnombre,
    accorden LIKE reqmacc.accorden
END RECORD

    DEFINE dbname STRING
    CONSTANT    prog_name = "reqm0318"

    DEFINE reg_det DYNAMIC ARRAY OF tDG
    DEFINE u_reg, g_reg             tDG
    DEFINE condicion STRING
    
END GLOBALS