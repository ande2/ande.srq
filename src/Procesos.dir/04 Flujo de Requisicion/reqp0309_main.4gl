IMPORT FGL req_combos

GLOBALS "reqp0309_glob.4gl"

MAIN
	DEFINE 
		n_param 	SMALLINT,
		prog_name2 	STRING

	DEFER INTERRUPT

	OPTIONS
		INPUT  WRAP,
		HELP KEY CONTROL-W,
		COMMENT LINE OFF,
		PROMPT LINE LAST - 2,
		MESSAGE LINE LAST - 1,
		ERROR LINE LAST

	LET n_param = num_args()
    IF n_param > 0 THEN
        LET usuario.ID = ARG_VAL(1)
        CALL req_permisos_usuario()
        LET prog_name2 = prog_name||".log"   -- El progr_name es definido como constante en el arch. globals
        
        CALL STARTLOG(prog_name2)

        CALL insert_init()
        CALL update_init()
        CALL delete_init()
        CALL main_init()
    END IF
END MAIN

FUNCTION main_init()
DEFINE 
	nom_forma 	STRING

    CALL ui.Interface.loadActionDefaults("actiondefaults_rh1")

    CLOSE WINDOW SCREEN 

    LET nom_forma = "reqp0309_form"
	OPEN WINDOW w1 WITH FORM nom_forma
    CALL fgl_settitle("ANDE - Proceso de Requisiciones")
    CALL ui.Window.getCurrent().setText("ANDE - Proceso de Requisiciones "||nomUsuario(usuario.ID))

	LET w = ui.WINDOW.getcurrent()
	LET f = w.getForm()

    CALL main_menu()
END FUNCTION

FUNCTION main_menu()
DEFINE varg1 CHAR (1),
    cuantos     SMALLINT,
    id, ids     SMALLINT,
    salir       SMALLINT,
    opcion      CHAR(1)
    
    LET varg1 = arg_val(1)
    LET salir = FALSE
    CALL f.setElementHidden("gridInput",1)
    CALL combo_init()
    LET permiso.vProcesar = FALSE
    LET permiso.vSoloVer = FALSE
    LET permiso.vActualiza = FALSE
    LET condi_fal = TODAY
    LET condi_fdl = TODAY - 90

    WHILE NOT salir
        DISPLAY "PROCESO DE REQUISICIONES" TO gtit_enc
        DISPLAY BY NAME condi_fdl, condi_fal

        DISPLAY ARRAY reg_req TO sLR.*
           ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE,UNBUFFERED)

          BEFORE DISPLAY
             LET cuantos = consulta(false)
             IF cuantos > 0 THEN 
                CALL dialog.setCurrentRow("sLR",1)
                LET gr_reg.* = reg_req[1].*
                CALL main_acceso()
                CALL DIALOG.setActionActive("ver", permiso.vSoloVer)
                CALL DIALOG.setActionActive("procesar", permiso.vProcesar)
                CALL DIALOG.setActionActive("actualizar", permiso.vActualiza)
                LET cuantos = consulta_b(FALSE)
             ELSE
                CALL DIALOG.setActionActive("ver", permiso.vSoloVer)
                CALL DIALOG.setActionActive("procesar", permiso.vProcesar)
                CALL DIALOG.setActionActive("actualizar", permiso.vActualiza)
             END IF 
             CALL encabezado("")

          BEFORE ROW 
             LET id = arr_curr()
             IF id > 0 THEN 
                LET gr_reg.* = reg_req[id].*
                LET cuantos = consulta_b(FALSE)
                CALL main_acceso()
                CALL DIALOG.setActionActive("ver", permiso.vSoloVer)
                CALL DIALOG.setActionActive("procesar", permiso.vProcesar)
                CALL DIALOG.setActionActive("actualizar", permiso.vActualiza)
             END IF 

          ON ACTION buscar
             LET id = arr_curr()
             LET ids = scr_line()
             LET opcion = 'B'
             EXIT DISPLAY
             
          ON ACTION ver ATTRIBUTES(TEXT="Ver")
             LET id = arr_curr()
             LET ids = scr_line()
             LET opcion = 'V'
             EXIT DISPLAY

          ON ACTION actualizar ATTRIBUTES(TEXT="Actualizar")
             LET id = arr_curr()
             LET ids = scr_line()
             LET opcion = 'V'
             EXIT DISPLAY
             
          ON ACTION procesar ATTRIBUTES(TEXT="Procesar")
             LET id = arr_curr()
             LET ids = scr_line()
             LET opcion = 'P'
             EXIT DISPLAY

      
           ON ACTION Salir
              LET opcion = 'S'
              EXIT DISPLAY 
        END DISPLAY

        CASE opcion

           WHEN "B"
             LET cuantos = consulta(TRUE)
             DISPLAY BY NAME condi_fdl, condi_fal
             DISPLAY ARRAY reg_det TO sLRB.*
                  BEFORE DISPLAY EXIT DISPLAY
             END DISPLAY
             CALL encabezado("")
              
           WHEN "V"
              DISPLAY "Ver" --- LLAMA A reqp0308, usuario, reqid, premiso? 0 - limitado 1 - total

           WHEN "P"
              DISPLAY "Procesar"
              IF ingreso() THEN 
                 LET cuantos = consulta(FALSE)
                 CALL fgl_set_arr_curr( arr_count() + 1 )
                 --Refrescar Pantalla
                 DISPLAY ARRAY reg_det TO sLRB.*
                    BEFORE DISPLAY  EXIT DISPLAY 
                 END DISPLAY 
              END IF
              CALL encabezado("")

            WHEN "S"
                LET salir = TRUE
        END CASE 
    END WHILE
    
END FUNCTION

FUNCTION encabezado(gtit_enc)
DEFINE gtit_enc STRING 

   DISPLAY BY NAME  gtit_enc
END FUNCTION 

FUNCTION combo_init()
   CALL combo_din2("reqetapa","SELECT etaid, etanombre FROM reqmeta ORDER BY etanombre ")
   CALL combo_din2("reqmotivo","select motId, trim(motNombre) from glbMMot ORDER BY 2")
   CALL combo_din2("etaidfin_1","SELECT etaid, etanombre FROM reqmeta ORDER BY etanombre ")
   CALL combo_din2("accid_1","SELECT accid, accnombre FROM reqmacc ORDER BY accnombre")
END FUNCTION 

FUNCTION cmb_reqestado(cb)
DEFINE cb     ui.ComboBox
DEFINE cmb_dat tComboTex
DEFINE i SMALLINT

   CALL cmb_reqestado_init() RETURNING cmb_dat.*
    
   CALL cb.clear()
   FOR i = 1 TO cmb_dat.cantidad
       CALL cb.addItem(cmb_dat.datos[i].id,cmb_dat.datos[i].nombre)
   END FOR
   
END FUNCTION

FUNCTION req_permisos_usuario()
DEFINE empl        RECORD LIKE commempl.*

# Gerente de Nómina      -> esnomina
# Superv / Coord de RRHH -> escoorrhh
# Analista de RRHH       -> esrrhh
# Clínica Médica         -> esclimed

    LET usuario.vDigita   = 0  -- 1
    LET usuario.vSolicita = 0  -- 2
    LET usuario.vAutoriza = 0  -- 3
    LET usuario.vAutorFun = 0  -- 4
    LET usuario.vNomina   = 0  -- 5
    LET usuario.vCooRRHH  = 0  -- 6
    LET usuario.vRRHH     = 0  -- 7
    LET usuario.vCliMed   = 0  -- 8
    
    SELECT * INTO empl.*
      FROM commempl
     WHERE id_commempl = usuario.ID

    IF empl.essolicitante = '1' THEN
        LET usuario.vSolicita =  2 
    END IF
    IF empl.esautorizante = '1' THEN
        LET usuario.vAutoriza = 3
    END IF
    IF empl.esgerareaf = '1' THEN
         LET usuario.vAutorFun = 4
    END IF
    IF empl.esnomina = '1' THEN  
        LET usuario.vEsNomina = TRUE 
        LET usuario.vNomina   = 5
    ELSE 
        LET usuario.vEsNomina = 
    FALSE END IF
    IF empl.escoorrhh = '1' THEN 
        LET usuario.vEsCooRRHH = TRUE  
        LET usuario.vCooRRHH   = 6
    ELSE 
        LET usuario.vEsCooRRHH = FALSE 
    END IF
    IF empl.esrrhh = '1' THEN    
        LET usuario.vEsRRHH = TRUE    
        LET usuario.vRRHH   = 7 
    ELSE 
        LET usuario.vEsRRHH = FALSE 
    END IF
    IF empl.esclimed = '1' THEN  
        LET usuario.vEsCliMed = TRUE   
        LET usuario.vCliMed   = 5
    ELSE 
        LET usuario.vEsCliMed = FALSE
    END IF

    LET condi_eta = etapas_acceso()

END FUNCTION

FUNCTION etapas_acceso()
DEFINE etapas STRING
DEFINE existe SMALLINT

    IF usuario.vEsNomina THEN
        CALL etapas_acceso_lista(5) RETURNING existe, etapas
    END IF
    IF usuario.vEsCooRRHH THEN
        CALL etapas_acceso_lista(6) RETURNING existe, etapas
    END IF
    IF usuario.vEsRRHH THEN
        CALL etapas_acceso_lista(7) RETURNING existe, etapas
    END IF
    IF usuario.vEsCliMed THEN
        CALL etapas_acceso_lista(8) RETURNING existe, etapas
    END IF

    IF NOT existe THEN
        LET etapas = '0'
    END IF
    
RETURN etapas
END FUNCTION

FUNCTION etapas_acceso_lista(id_etapa)
DEFINE etapas   STRING
DEFINE existe   SMALLINT
DEFINE id_etapa LIKE reqmeta.etaid
DEFINE i,j      SMALLINT
DEFINE r        RECORD LIKE reqmuap.*
DEFINE ra       DYNAMIC ARRAY OF RECORD LIKE reqmuap.*

    LET etapas = ' '
    LET existe = FALSE

    CALL ra.clear()
    DECLARE cur_uap CURSOR FOR
        SELECT *
          FROM reqmuap
         WHERE uapusuario = id_etapa

    LET i = 0
    FOREACH cur_uap INTO r.*
        LET i = i + 1
        LET ra[i].* = r.*
    END FOREACH
    FOR j = 1 TO i
        LET existe = TRUE
        LET etapas = etapas, ra[j].etaid USING "##&"
        IF j < i THEN
            LET etapas = etapas,','
        END IF
    END FOR
 
RETURN existe, etapas
END FUNCTION

FUNCTION main_acceso()
DEFINE contar      SMALLINT
DEFINE ut         tUsuario

    LET ut.* = usuario.*
    LET permiso.vSoloVer = FALSE
    LET permiso.vProcesar = FALSE
    LET permiso.vActualiza = FALSE
    
    CASE usuario.ID
        WHEN gr_reg.reqdigitador LET permiso.vSoloVer = TRUE
        WHEN gr_reg.reqususol LET permiso.vSoloVer = TRUE
        WHEN gr_reg.requsuaut LET permiso.vSoloVer = TRUE
        WHEN gr_reg.gerfunaut LET permiso.vSoloVer = TRUE
    END CASE
    IF NOT permiso.vSoloVer THEN
        IF (usuario.vEsCliMed +  usuario.vEsCooRRHH + usuario.vEsNomina + usuario.vEsRRHH) > 0 THEN
            LET permiso.vSoloVer = TRUE
        END IF
    END IF

    IF gr_reg.reqdigitador = usuario.ID THEN LET ut.vDigita = 1   ELSE LET ut.vDigita = 0 END IF
    IF gr_reg.reqususol = usuario.ID THEN    LET ut.vSolicita = 2 ELSE LET ut.vSolicita = 0 END IF
    IF gr_reg.requsuaut = usuario.ID THEN    LET ut.vAutoriza = 3 ELSE LET ut.vAutoriza = 0 END IF
    IF gr_reg.gerfunaut = usuario.ID THEN   LET ut.vAutorFun = 4  ELSE LET ut.vAutorFun = 0 END IF
    

    LET contar = 0
    SELECT COUNT(*) INTO contar
      FROM reqmuap
     WHERE etaid = gr_reg.reqetapa
       AND uapusuario IN ( ut.vDigita, ut.vSolicita, ut.vAutoriza, ut.vAutorFun,
                           ut.vNomina, ut.vCooRRHH,  ut.vRRHH,     ut.vCliMed)
       AND uaptipo = '1'
    IF contar IS NULL THEN LET contar = 0 END IF
    IF contar > 0 THEN
        LET permiso.vProcesar = TRUE
    END IF

    LET contar = 0
    SELECT COUNT(*) INTO contar
      FROM reqmuap
     WHERE etaid = gr_reg.reqetapa
       AND uapusuario IN ( ut.vDigita, ut.vSolicita, ut.vAutoriza, ut.vAutorFun,
                           ut.vNomina, ut.vCooRRHH,  ut.vRRHH,     ut.vCliMed)
       AND uaptipo = '2'
    IF contar IS NULL THEN LET contar = 0 END IF
    IF contar > 0 THEN
        LET permiso.vActualiza = TRUE
    END IF
    
END FUNCTION
