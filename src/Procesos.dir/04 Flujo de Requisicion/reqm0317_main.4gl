IMPORT FGL req_combos

GLOBALS "reqm0317_glob.4gl"

MAIN
	DEFINE 
		n_param 	SMALLINT,
		prog_name2 	STRING

	DEFER INTERRUPT

	OPTIONS
		INPUT  WRAP,
		HELP KEY CONTROL-W,
		COMMENT LINE OFF,
		PROMPT LINE LAST - 2,
		MESSAGE LINE LAST - 1,
		ERROR LINE LAST

	LET n_param = num_args()

	LET prog_name2 = prog_name||".log"   -- El progr_name es definido como constante en el arch. globals
    
	CALL STARTLOG(prog_name2)

	CALL insert_init()
    CALL update_init()
    CALL delete_init()
    CALL combo_init()
	CALL main_init()
END MAIN

FUNCTION main_init()
DEFINE 
	nom_forma 	STRING,
    w           ui.Window,
    f           ui.Form

    CALL ui.Interface.loadActionDefaults("actiondefaults_rh1")

    CLOSE WINDOW SCREEN 

    LET nom_forma = "reqm0317_form"
	OPEN WINDOW w1 WITH FORM nom_forma
    CALL fgl_settitle("ANDE - Etapas de las requisiciones")

	LET w = ui.WINDOW.getcurrent()
	LET f = w.getForm()

    CALL main_menu()
END FUNCTION

FUNCTION main_menu()
DEFINE varg1 CHAR (1),
    cuantos     SMALLINT,
    id, ids     SMALLINT,
    runcmd      STRING
    
    LET varg1 = arg_val(1)

    DISPLAY "REGISTRO DE ETAPAS DE LAS REQUISICIONES" TO gtit_enc

    DISPLAY ARRAY reg_det TO sLR.*
       ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE,UNBUFFERED)

      BEFORE DISPLAY
         LET cuantos = consulta(false)
         IF cuantos > 0 THEN 
            CALL dialog.setCurrentRow("sLR",1)
            LET g_reg.* = reg_det[1].*
            DISPLAY BY NAME g_reg.*
         END IF 
         CALL encabezado("")

      BEFORE ROW 
         LET id = arr_curr()
         IF id > 0 THEN 
            LET g_reg.* = reg_det[id].*
            DISPLAY BY NAME g_reg.*
         END IF 
         LET cuantos = consulta_f()
         
      ON ACTION agregar
         IF ingreso() THEN 
            LET cuantos = consulta(FALSE)
            CALL fgl_set_arr_curr( arr_count() + 1 )
            --Refrescar Pantalla
            DISPLAY ARRAY reg_det TO sDG.*
               BEFORE DISPLAY EXIT DISPLAY 
            END DISPLAY 
            CALL dialog.setCurrentRow("sLR",cuantos)
            LET g_reg.* = reg_det[cuantos].*
            DISPLAY BY NAME g_reg.*
         END IF
         LET cuantos = consulta_f()
         CALL encabezado("")

      ON ACTION modificar
         LET id = arr_curr()
         LET ids = scr_line()
         IF id > 0 THEN 
            IF modifica() THEN
               LET reg_det[id].* = g_reg.*
               DISPLAY reg_det[id].* TO sDG[ids].*
            END IF   
         END IF 
         LET cuantos = consulta_f()
         CALL encabezado("")

      ON ACTION anular
         LET id = arr_curr()
         LET ids = scr_line()         
         IF id > 0 THEN 
            IF anular() THEN
               CALL DIALOG.deleteRow("sLR", id)
               IF id = arr_count() THEN 
                  LET id = id - 1
               END IF 
               IF id > 0 THEN
                  LET g_reg.* = reg_det[id].*
               ELSE 
                  INITIALIZE g_reg.* TO NULL
               END IF 
               DISPLAY BY NAME g_reg.*

            END IF   
         END IF 
         LET cuantos = consulta_f()

       ON ACTION permisos ATTRIBUTES(TEXT="Permisos")
         LET runcmd = 'fglrun reqm0319.42r ', g_reg.etaid
         RUN runcmd 
  
       ON ACTION salir
          EXIT DISPLAY 
    END DISPLAY 
END FUNCTION

FUNCTION encabezado(gtit_enc)
DEFINE gtit_enc STRING 

   DISPLAY BY NAME  gtit_enc
END FUNCTION 

FUNCTION combo_init()
   CALL combo_din2("accid","SELECT accid, accnombre, accorden FROM reqmacc ORDER BY accorden ")
   CALL combo_din2("flusigetapa","SELECT etaid, etanombre, etaorden FROM reqmeta ORDER BY etaorden ")
END FUNCTION 

FUNCTION cmb_flutiporeq(cb)
DEFINE cb     ui.ComboBox
DEFINE mmot   RECORD LIKE glbMMot.*

   CALL cb.clear()
   CALL cb.addItem('0',"Todas")
   CALL cb.addItem('1',"Segun Motivo")
   
END FUNCTION

FUNCTION cmb_motid(cb)
DEFINE cb     ui.ComboBox
DEFINE mmot   RECORD LIKE glbMMot.*

   CALL cb.clear()
   CALL cb.addItem(NULL,"Todas")

   DECLARE cur_tiporeq CURSOR FOR
      SELECT *
        FROM glbMMot 
       ORDER BY 2
   FOREACH cur_tiporeq INTO mmot.*
       CALL cb.addItem(mmot.motid,mmot.motnombre)
   END FOREACH
END FUNCTION

FUNCTION cmb_reqestado(cb)
DEFINE cb     ui.ComboBox
DEFINE cmb_dat tComboTex
DEFINE i SMALLINT

   CALL cmb_reqestado_init() RETURNING cmb_dat.*
    
   CALL cb.clear()
   FOR i = 1 TO cmb_dat.cantidad
       CALL cb.addItem(cmb_dat.datos[i].id,cmb_dat.datos[i].nombre)
   END FOR

END FUNCTION

FUNCTION cmb_tipousuario(cb)
DEFINE cb     ui.ComboBox
DEFINE cmb_dat tComboNum
DEFINE i SMALLINT

   CALL cmb_usuario_init() RETURNING cmb_dat.*
    
   CALL cb.clear()
   FOR i = 1 TO cmb_dat.cantidad
       CALL cb.addItem(cmb_dat.datos[i].id,cmb_dat.datos[i].nombre)
   END FOR
END FUNCTION
