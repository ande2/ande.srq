GLOBALS "reqp0309_glob.4gl"

FUNCTION insert_init()
DEFINE strSql STRING 

   LET strSql =
      "INSERT INTO reqmbit ( ",
        " etaidini, etaidfin, accid, bitobserva, ",
        " reqid, etaestadoini, etaestadofin, bitusuoper, ",
        " bitfecoper ",
        "   ) ",
      " VALUES (?,?,?,?,  ?,?,?,?, ?)"

   PREPARE st_insertar FROM strSql

END FUNCTION 

FUNCTION ingreso()
   CALL encabezado("Ingresar")
   --CALL info_usuario()
   IF captura_datos('I') THEN
      RETURN grabar()
   ELSE 
      RETURN FALSE 
   END IF 
END FUNCTION 

FUNCTION grabar()
   DEFINE i SMALLINT 
   DEFINE bitfecoper LIKE reqmbit.bitfecoper
   DEFINE etapa    RECORD LIKE reqmeta.*
   DEFINE requi    RECORD LIKE reqmreq.*
   DEFINE estado   SMALLINT
   DEFINE cantidad SMALLINT
   DEFINE infoadd  SMALLINT
   
   LET estado   = 1
   LET cantidad = 0

   SELECT * INTO etapa.*
     FROM reqmeta
    WHERE etaid = g_reg.etaidfin

   SELECT * INTO requi.*
     FROM reqmreq
    WHERE reqid = gr_reg.reqid


   SELECT COUNT(*) INTO cantidad
     FROM reqmbit
    WHERE reqid = requi.reqid
   IF cantidad IS NULL THEN LET cantidad = 0 END IF

   LET bitfecoper = CURRENT YEAR TO SECOND
   TRY 
      BEGIN WORK
          IF cantidad = 0 THEN
              INSERT INTO reqmbit ( reqid, etaidfin, etaestadofin, bitusuoper, bitfecoper )
                 VALUES( requi.reqid, requi.reqetapa, requi.reqestado, requi.reqdigitador, 
                   requi.fechoringreso)
          END IF
          
          EXECUTE st_insertar USING 
               g_reg.etaidini, g_reg.etaidfin, g_reg.accid, g_reg.bitobserva,
               gr_reg.reqid, gr_reg.reqestado, etapa.etaestadoreq, usuario.ID, 
               bitfecoper

          UPDATE reqmreq
             SET reqetapa = etapa.etaid,
                 reqestado = etapa.etaestadoreq
            WHERE reqid = gr_reg.reqid

                LET infoadd = 0
                SELECT etaagregainfo INTO infoadd
                  FROM reqmeta
                 WHERE etaid = etapa.etaid
                IF infoadd > 0 THEN
                    CASE infoadd
                      WHEN 1 
                          UPDATE reqmreq
                             SET reqsalario  = g_datos1.reqsalario,
                                 reqcondcomp = g_datos1.reqcondcomp,
                                 reqhrsext   = g_datos1.reqhrsext,
                                 reqobs      = g_datos1.reqobs,
                                 fechaing    = g_datos1.fechaing
                           WHERE reqid = gr_reg.reqid
                      WHEN 2 
                          UPDATE reqmreq
                             SET reqanalista  = g_datos2.reqanalista
                           WHERE reqid = gr_reg.reqid
                    END CASE
                END IF
      COMMIT WORK
   CATCH 
      ROLLBACK WORK
      CALL msgError(sqlca.sqlcode,"Grabar Registro")
      RETURN FALSE 
   END TRY
   DISPLAY BY NAME g_reg.*
   CALL box_valdato ("Registro agregado")

   RETURN TRUE 
END FUNCTION 