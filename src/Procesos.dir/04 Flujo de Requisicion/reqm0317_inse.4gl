GLOBALS "reqm0317_glob.4gl"

FUNCTION insert_init()
DEFINE strSql STRING 

   LET strSql =
      "INSERT INTO reqmeta ( ",
        " etanombre, etaorden,  etaestadoreq, etaabccandid, ",
        " etaagregainfo, etatipo, etatiempo ",
        "   ) ",
      " VALUES (?,?,?,?)"

   PREPARE st_insertar FROM strSql

   LET strSql =
      "INSERT INTO reqmflu ( ",
        " etaid, accid, flusigetapa, flutiporeq ",
        "   ) ",
      " VALUES (?,?,?.?)"

   PREPARE st_insertar_f FROM strSql
   
END FUNCTION 

FUNCTION ingreso()
   CALL encabezado("Ingresar")
   --CALL info_usuario()
   IF captura_datos('I') THEN
      RETURN grabar()
   ELSE 
      RETURN FALSE 
   END IF 
END FUNCTION 

FUNCTION grabar()
DEFINE tiempo LIKE reqmeta.etatiempo

DEFINE txtiempo CHAR(10)
DEFINE txthoras CHAR(10)

   LET txtiempo = '0 0'
   IF g_reg.dias > 0 THEN
      LET txtiempo = g_reg.dias USING "##&"
   ELSE
      LET txtiempo = ' 0'
   END IF
   IF g_reg.horas > 0 THEN
      LET txthoras =  g_reg.horas USING "#&&"
      LET txtiempo = txtiempo CLIPPED, txthoras
   ELSE
      LET txtiempo = txtiempo CLIPPED,' 0'
   END IF
   DISPLAY txtiempo
   LET tiempo = txtiempo
   
   TRY 
      BEGIN WORK
          EXECUTE st_insertar USING 
                g_reg.etanombre, g_reg.etaorden, g_reg.etaestadoreq, g_reg.etaabccandid,
                g_reg.etaagregainfo, g_reg.etatipo, tiempo
          LET g_reg.etaid = sqlca.sqlerrd[2]
          CALL grabar_f()
      COMMIT WORK
   CATCH 
      ROLLBACK WORK
      CALL msgError(sqlca.sqlcode,"Grabar Registro")
      RETURN FALSE 
   END TRY
   DISPLAY BY NAME g_reg.*
   CALL box_valdato ("Registro agregado")

   RETURN TRUE 
END FUNCTION 


FUNCTION grabar_f()
   DEFINE i,j SMALLINT 
   
  LET j = reg_flu.getLength()
  FOR i = 1 TO j
     IF reg_flu[i].flutiporeq = '0' THEN LET reg_flu[i].motid = NULL END IF
     
     IF reg_flu[i].accid > 0 AND reg_flu[i].flusigetapa > 0 AND reg_flu[i].flutiporeq IS NOT NULL THEN
         INSERT INTO reqmflu (etaid, accid, flusigetapa, flutiporeq, motid)
            VALUES (g_reg.etaid, reg_flu[i].accid, 
                    reg_flu[i].flusigetapa, reg_flu[i].flutiporeq,
                    reg_flu[i].motid)
     END IF
  END FOR
END FUNCTION 