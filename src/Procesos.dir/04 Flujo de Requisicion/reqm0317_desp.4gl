
GLOBALS "reqm0317_glob.4gl"
 
FUNCTION consulta(flagConsulta)
DEFINE flagConsulta BOOLEAN 
DEFINE tiempo       LIKE reqmeta.etatiempo
DEFINE txtiempo     CHAR(25)
DEFINE 
   rDet tDG,
   x,i,vcon INTEGER,
   consulta STRING  

   IF flagConsulta THEN 
      CALL encabezado("Consulta")

      CONSTRUCT condicion 
         ON etaid, etanombre, etaorden
         FROM etaid, etanombre, etaorden

         ON ACTION ACCEPT
            EXIT CONSTRUCT 
         ON ACTION CANCEL 
            CALL reg_det.clear()
            RETURN 0
             
      END CONSTRUCT 

   ELSE
      LET condicion = " 1=1 "
   END IF
   --Armar la consulta
   LET consulta = 
      "SELECT  etaid, etanombre, etaorden, etaestadoreq, etaabccandid, etaagregainfo, etatipo, 0, 0, etatiempo ",
      " FROM reqmeta ",
      " WHERE etaid > 0 AND ", condicion,
      " ORDER BY 1"
   
   --definir cursor con consulta de BD
   DECLARE curDet CURSOR FROM consulta
   CALL reg_det.clear()
   LET x = 0
   --Llenar el arreglo con el resultado de la consulta
   FOREACH curDet INTO rDet.*, tiempo
      LET x = x + 1 
      
      IF tiempo IS NULL THEN 
         LET rDet.dias = 0 
         LET rDet.horas = 0
      ELSE
         LET txtiempo = tiempo
         LET rDet.dias = txtiempo[1,10] 
         LET rDet.horas = txtiempo[11,25]    
      END IF
      IF rDet.etaagregainfo IS NULL THEN LET rDet.etaagregainfo = 0 END IF
      IF rDet.etatipo IS NULL THEN LET rDet.etatipo = 0 END IF
      
      LET reg_det [x].* = rDet.*  
   END FOREACH

   CALL combo_init()
   
   RETURN x --Cantidad de registros
END FUNCTION


FUNCTION consulta_f()
DEFINE 
   rDet tDGF,
   x,i,vcon INTEGER,
   consulta STRING  

   --Armar la consulta
   LET x = 0
   IF g_reg.etaid > 0 THEN
   LET consulta = 
      "SELECT  accid, flusigetapa, flutiporeq, motid, fluid ",
      " FROM reqmflu ",
      " WHERE etaid = ", g_reg.etaid,
      " ORDER BY 1"
   
   --definir cursor con consulta de BD
   DECLARE curFlujo CURSOR FROM consulta
   CALL reg_flu.clear()
   LET x = 0
   --Llenar el arreglo con el resultado de la consulta
   FOREACH curFlujo INTO rDet.*
      LET x = x + 1 
      LET reg_flu[x].* = rDet.*  
   END FOREACH

   DISPLAY ARRAY reg_flu TO sDGF.*
       BEFORE DISPLAY EXIT DISPLAY
   END DISPLAY
   ELSE
       DISPLAY "No hay etaid"
   END IF
   
   RETURN x --Cantidad de registros
END FUNCTION