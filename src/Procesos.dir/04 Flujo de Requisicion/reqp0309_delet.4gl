GLOBALS "reqp0309_glob.4gl"


FUNCTION delete_init()
DEFINE strSql STRING 

   LET strSql =
      "DELETE FROM reqmbit ",
      "WHERE bitid = ? "
      
   PREPARE st_delete FROM strSql

   LET strSql =
      "UPDATE reqmbit SET estado = 0 ",
        " WHERE bitid = ? "        

   PREPARE st_anular FROM strSql

END FUNCTION 

FUNCTION anular()
DEFINE vresult SMALLINT
DEFINE oper CHAR(1) 
DEFINE mensaje STRING


   LET oper = 'E'
   LET mensaje = 'Al anular, eliminará el registro'
   
   IF NOT box_confirma(mensaje) THEN
      RETURN FALSE
   END IF
   
   TRY
      IF oper='A' THEN 
         EXECUTE st_anular USING gb_reg.bitid_1
      ELSE 
         EXECUTE st_delete USING gb_reg.bitid_1
      END IF 
   CATCH 
      CALL msgError(sqlca.sqlcode,"Eliminar Registro")
      RETURN FALSE 
   END TRY
   
   CALL msg("Registro eliminado")
   
   RETURN TRUE 
END FUNCTION 
