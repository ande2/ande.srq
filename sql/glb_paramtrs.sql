






{ TABLE "informix".glb_paramtrs row size = 349 number of columns = 7 index size = 13 }

create table "informix".glb_paramtrs 
  (
    numpar integer not null ,
    tippar integer not null ,
    nompar varchar(60,1),
    valchr varchar(255,1) not null ,
    userid varchar(15,1) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (numpar,tippar)  constraint "informix".pkglbparamtrs
  );

revoke all on "informix".glb_paramtrs from "public" as "informix";




