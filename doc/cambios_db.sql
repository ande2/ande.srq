------------------------------------------------------------------------------
--****************************************************************************
-- FLUJO DE REQUISICIONES*****************************************************
-- MQ************************************************************************
-- 17/11/2022
-- fglschema rh
------------------------------------------------------------------------------

------------------------------------------------------------------------------1
CREATE SEQUENCE IF NOT EXISTS reqmeta_etaid_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 32765   --- SMALLINT
    CACHE 1;
	
create table reqmeta -- ETAPAS DEL REQUERIMIENTO
(
    etaid smallint NOT NULL DEFAULT nextval('reqmeta_etaid_seq'::regclass),
    etanombre character(30) COLLATE pg_catalog."default" NOT NULL,
    etaorden smallint NOT NULL,
    etaestadoreq character(1) COLLATE pg_catalog."default" NOT NULL,
    etaabccandid character(1) COLLATE pg_catalog."default",
    CONSTRAINT reqmeta_pkey PRIMARY KEY (etaid)
)

ALTER SEQUENCE reqmeta_etaid_seq OWNED BY reqmeta.etaid;
-------------------------------------------------------------------------------1

------------------------------------------------------------------------------2
CREATE SEQUENCE IF NOT EXISTS reqmacc_etaid_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 32765   --- SMALLINT
    CACHE 1;
	
create table reqmacc -- ACCIONES PARA ETAPAS DEL REQUERIMIENTO
(
 accid smallint not null default nextval('reqmacc_etaid_seq'::regclass),
 accnombre char(25) not null,
 accorden smallint not null,
 CONSTRAINT reqmacc_pkey PRIMARY KEY (accid)
);

ALTER SEQUENCE reqmacc_etaid_seq OWNED BY reqmacc.accid;
-------------------------------------------------------------------------------2

------------------------------------------------------------------------------3
	
CREATE SEQUENCE IF NOT EXISTS reqmflu_fluid_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 32765   --- SMALLINT
    CACHE 1;

CREATE TABLE IF NOT EXISTS public.reqmflu-- ACCIONES PARA ETAPAS DEL REQUERIMIENTO
(
	fluid smallint NOT NULL default nextval('reqmflu_fluid_seq'::regclass),
    etaid smallint NOT NULL,
    accid smallint NOT NULL,
    flutiporeq character(1) NOT NULL,
	motid smallint,
    flusigetapa smallint NOT NULL,
    CONSTRAINT reqmflu_pkey PRIMARY KEY (fluid),
    CONSTRAINT reqmflu_accid_fkey FOREIGN KEY (accid)
        REFERENCES public.reqmacc (accid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT reqmflu_etaid_fkey FOREIGN KEY (etaid)
        REFERENCES public.reqmeta (etaid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT reqmflu_flusigetapa_fkey FOREIGN KEY (flusigetapa)
        REFERENCES public.reqmeta (etaid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT reqmflu_motid_fkey FOREIGN KEY (motid)
        REFERENCES public.glbmmot (motid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
 
ALTER SEQUENCE reqmflu_fluid_seq OWNED BY reqmflu.fluid;

-------------------------------------------------------------------------------3


-------------------------------------------------------------------------------4
CREATE SEQUENCE IF NOT EXISTS reqmuap_uapid_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647   --- INTEGER
    CACHE 1;
	
CREATE TABLE IF NOT EXISTS reqmuap -- Usuarios autorizados para procesos
(
    uapid integer NOT NULL DEFAULT nextval('reqmuap_uapid_seq'::regclass),
    etaid smallint NOT NULL,
    uapusuario integer NOT NULL,
    uaptipo character(1) NOT NULL,
    CONSTRAINT reqmuap_pkey PRIMARY KEY (uapid),
    CONSTRAINT reqmuap_etaid_fkey FOREIGN KEY (etaid)
        REFERENCES public.reqmeta (etaid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

ALTER SEQUENCE reqmuap_uapid_seq OWNED BY reqmuap.uapid;

------------------------------------------------------------------------------4

-------------------------------------------------------------------------------5
	
CREATE SEQUENCE IF NOT EXISTS reqmbit_bitid_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647   --- INTEGER
    CACHE 1;

CREATE TABLE IF NOT EXISTS reqmbit-- BITACORA DE REQUERIMIENTOS
(
    bitid integer NOT NULL DEFAULT nextval('reqmbit_bitid_seq'::regclass),
    reqid integer NOT NULL,
    etaidini smallint,
    etaidfin smallint NOT NULL,
    accid smallint,
    etaestadoini character(1) COLLATE pg_catalog."default",
    etaestadofin character(2) COLLATE pg_catalog."default" NOT NULL,
    bitobserva character varying(250) COLLATE pg_catalog."default",
    bitusuoper integer,
    bitfecoper timestamp without time zone,
    CONSTRAINT reqmbit_pkey PRIMARY KEY (bitid),
    CONSTRAINT reqmbit_accid_fkey FOREIGN KEY (accid)
        REFERENCES public.reqmacc (accid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT reqmbit_etaidfin_fkey FOREIGN KEY (etaidfin)
        REFERENCES public.reqmeta (etaid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT reqmbit_etaidini_fkey FOREIGN KEY (etaidini)
        REFERENCES public.reqmeta (etaid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT reqmbit_reqid_fkey FOREIGN KEY (reqid)
        REFERENCES public.reqmreq (reqid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
 
ALTER SEQUENCE reqmbit_bitid_seq OWNED BY reqmbit.bitid;

-------------------------------------------------------------------------------5

-------------------------------------------------------------------------------6
ALTER TABLE reqmreq ADD COLUMN reqetapa INTEGER;

UPDATE reqmreq SET reqetapa = 1;

ALTER TABLE eqmreq ALTER COLUMN reqetapa SET NOT NULL;

ALTER TABLE IF EXISTS reqmreq
    ADD CONSTRAINT reqmreq_reqmeta_reqetapa FOREIGN KEY (reqetapa)
    REFERENCES reqmeta (etaid) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;
	

ALTER TABLE commempl ADD COLUMN esrrhh character(1);
ALTER TABLE commempl ADD COLUMN esnomina character(1);
ALTER TABLE commempl ADD COLUMN escoorrhh character(1);
ALTER TABLE commempl ADD COLUMN esclimed character(1);

-------------------------------------------------------------------------------6



















------------------------------------------------------------------------------
--****************************************************************************
-- CANDIDATOS****************************************************************
-- MQ************************************************************************
-- 20/10/2022
-- fglschema rh
------------------------------------------------------------------------------1
CREATE SEQUENCE IF NOT EXISTS reqmtrc_trcid_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 32765   --- SMALLINT
    CACHE 1;
	
create table reqmtrc -- TIPO DE RECLUTAMIENTO CANDIDATOS
(
 trcid smallint not null default nextval('reqmtrc_trcid_seq'::regclass),
 trcnombre char(25) not null,
 trcactivo char(1) not null,
 CONSTRAINT reqmtrc_pkey PRIMARY KEY (trcid)
);

ALTER SEQUENCE reqmtrc_trcid_seq OWNED BY reqmtrc.trcid;
-------------------------------------------------------------------------------1


-------------------------------------------------------------------------------2
CREATE SEQUENCE IF NOT EXISTS reqmtrech_trechid_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 32765   --- SMALLINT
    CACHE 1;
	
create table reqmtrech --- TIPOS DE RECHAZO DE CANDIDATOS
(
  trechid smallint not null default nextval('reqmtrech_trechid_seq'::regclass),
  trechnombre varchar(30) not null,
  trechobserva varchar(255),
  CONSTRAINT reqmtrech_pkey PRIMARY KEY (trechid)
);

ALTER SEQUENCE reqmtrc_trcid_seq OWNED BY reqmtrech.trechid;
-------------------------------------------------------------------------------2


-------------------------------------------------------------------------------3
CREATE SEQUENCE IF NOT EXISTS reqrelig_relgid_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 32765   --- SMALLINT
    CACHE 1;

create table reqrelig --- LISTADO DE RELIGUIONES
(
  relgid smallint not null default nextval('reqrelig_relgid_seq'::regclass),
  relgnombre varchar(30) not null,
  relgobserba varchar(255),
  CONSTRAINT reqrelig_pkey PRIMARY KEY (relgid)
);

ALTER SEQUENCE reqmtrc_trcid_seq OWNED BY reqrelig.relgid;
-------------------------------------------------------------------------------3



-------------------------------------------------------------------------------4
CREATE SEQUENCE IF NOT EXISTS reqmcantd_cantdid_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 32765   --- SMALLINT
    CACHE 1;


create table reqmcantd -- TIPO DOCUMENTO DE CANDIDATOS
(
 cantdid smallint not null default nextval('reqmcantd_cantdid_seq'::regclass),
 cantdnombre char(25) not null,
 cantdactivo char(1) not null,
 CONSTRAINT reqmcantd_pkey PRIMARY KEY (cantdid)
);

ALTER SEQUENCE reqmtrc_trcid_seq OWNED BY reqmcantd.cantdid; 



-------------------------------------------------------------------------------5
CREATE SEQUENCE IF NOT EXISTS public.reqmcan_canid_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647   --- INTEGER
    CACHE 1;
	
create table reqmcan -- CANDIDATOS
(
 canid integer not null default nextval('reqmcan_canid_seq'::regclass),
 reqid integer not null,
 canfecing date, -- FECHA DE INGRESO DE REGISTRO
 ------- INFORMACION BASICA DEL CANDIDATO
 cannombre1 varchar(25) not null,
 cannombre2 varchar(25),
 cannombre3 varchar(25),
 canapellido1 varchar(25) not null,
 canapellido2 varchar(25),
 canapellidoc varchar(25),
 cangenero varchar(1), -- NUEVO USO 22/10/2022 Masculino Femenino
 cantrato char(1), -- 22/10/2022 NUEVO para el trato a usar (señor, señora, señorita)
 cantipiden char(5), -- TIPO DE IDENTIFICACION (DPI, PASAPORTE, CERTIFICADO NAC)
 canidentif varchar(25),
 canfecnac date not null,
 canestadocivil char(1),
 -- INFORMACION PARA CONTACTAR AL CANDIDATO
 canemail varchar(100),
 candireccion varchar(150),
 cantelefono varchar(20),
 -- INFORMACION EXTRA DEL CANDIDATO
 relgid smallint, --- 22/10/2022 Cambiado Antes: canreligion
 cannivelacademico char(3),
 cancarrera varchar(60),
 cansalud varchar(255),
 cantrasporte char(3),
 canfamilialab char(1),
 canfamiliapub char(1),
 canobserva varchar(255),
 -- INFORMACION INTERNA
 canfindesemana char(1),
 canturnorota char(1),
 canprocesant char(1),
 trcid smallint,
 canestado char(1) not null, -- ESTADO DEL CANDIDATO
 canfecprocita date,
 canfecprochora timestamp without time zone,
 -- INFORMACION DE RECHAZO
 trechid smallint, --************* COMBOBO BOX? MOTIVO RECHAZO *** 22/10/2022 Cambiado Antes: canmotrech
 canmotrechfec date, -- FECHA DE RECHAZO
 canmotrechob varchar(255), --- OBSERBACIONES ADICIONALES POR FECHAZO
 --- REFERENCIAS DE TABLA
 CONSTRAINT reqmcan_pkey PRIMARY KEY (canid),
 CONSTRAINT reqmcan_reqid_fkey FOREIGN KEY (reqid) -- REQUISICION
    REFERENCES reqmreq (reqid) MATCH SIMPLE
    ON UPDATE NO ACTION 
    ON DELETE NO ACTION,
 CONSTRAINT reqmcan_trcid_fkey FOREIGN KEY (trcid) --  TIPO DE RECLUTAMIENTO CANDIDATOS
    REFERENCES reqmtrc (trcid) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION,
 CONSTRAINT reqmcan_trechid_fkey FOREIGN KEY (trechid) --  TIPOS DE RECHAZO DE CANDIDATOS
    REFERENCES reqmtrech (trechid) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION,
 CONSTRAINT reqmcan_relgid_fkey FOREIGN KEY (relgid) --  RELIGIONES
    REFERENCES reqrelig (relgid) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
);	

ALTER SEQUENCE reqmtrc_trcid_seq OWNED BY reqmcan.canid;
---------------------------------------------------------------------------------------5

-------------------------------------------------------------------------------4
--- ACA EN BD
--- 20221025_2

ALTER TABLE IF EXISTS public.reqmcan
    ADD COLUMN caninfofam character varying(255);

ALTER TABLE IF EXISTS public.reqmcan
    ADD COLUMN canexperilab character varying(255);
	
-------------------------------------------------------------------------------4
--- ACA EN BD
--- 20221026_1

ALTER TABLE reqmcantd ALTER COLUMN cantdnombre TYPE varchar(100);


--- ACA EN BD
--- 20221031_1

----------------------------------------------------------------------------------------6

CREATE SEQUENCE IF NOT EXISTS public.reqmcandoc_cdocid_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647   --- INTEGER
    CACHE 1;

create table reqmcandoc -- DOCUMENTOS EN BASE DE DATOS DE CANDIDATOS
(
 canid integer not null,
 cdocid integer not null default nextval('reqmcandoc_cdocid_seq'::regclass),
 cantdid smallint null,
 cdocfecing date,
 cdocobserv varchar(255),
 cdocarchdir varchar(255),
 cdocarchiv bytea,
 CONSTRAINT reqmcandoc_pkey PRIMARY KEY (canid, cdocid),
 CONSTRAINT reqmcandoc_canid_fkey FOREIGN KEY (canid)
    REFERENCES public.reqmcan (canid) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION, 
 CONSTRAINT reqmcandoc_cantdid_fkey FOREIGN KEY (cantdid) -- REQUISICION
    REFERENCES reqmcantd (cantdid) MATCH SIMPLE
    ON UPDATE NO ACTION 
    ON DELETE NO ACTION
);

ALTER SEQUENCE reqmcandoc_cdocid_seq OWNED BY reqmcandoc.cdocid;
---------------------------------------------------------------------------------------6
--- ACA EN BD
--- 20221031_1


---------------------------------------------------------------------------------------7
CREATE VIEW vw_reqcand AS
SELECT canid, cannombre1, cannombre2, canapellido1, canidentif, canemail,
      u.uornombre, a.afunombre, p.puenombre, r.reqid
FROM reqmcan c, reqmreq r, glbuniorga u, glbmareaf a, glbmpue p
WHERE c.reqid = r.reqid
AND u.uorid = r.pueuniorg
AND a.afuid = u.uorareaf
AND p.pueid = r.pueposicion


---------------------------------------------------------------------------------------7
--- ACA EN BD
--- 20221101_2

---------------------------------------------------------------------------------------8
ALTER TABLE reqmcan ADD COLUMN cancantproc smallint;
ALTER TABLE reqmcan ALTER COLUMN cantipiden SET NOT NULL;
ALTER TABLE reqmcan ALTER COLUMN canidentif SET NOT NULL;
ALTER TABLE reqmcan ADD CONSTRAINT reqmcan_canidentif_uniq UNIQUE (canidentif);
---------------------------------------------------------------------------------------8



---------------------------------------------------------------------------------------9	
CREATE TABLE reqdcan
(
  canid integer not null,
  reqid integer not null,
  cdetfecha timestamp without time zone,
  canestado char(1),
 CONSTRAINT reqdcan_pkey PRIMARY KEY (canid, reqid),
 CONSTRAINT reqdcan_canid_fkey FOREIGN KEY (canid)
    REFERENCES public.reqmcan (canid) MATCH SIMPLE -- CANDIDATOS
    ON UPDATE NO ACTION
    ON DELETE NO ACTION, 
 CONSTRAINT reqdcan_reqid_fkey FOREIGN KEY (reqid) -- REQUISICION
    REFERENCES reqmreq (reqid) MATCH SIMPLE
    ON UPDATE NO ACTION 
    ON DELETE NO ACTION
);
---------------------------------------------------------------------------------------9
--- ACA EN BD
--- 20221102_2

---------------------------------------------------------------------------------------10
ALTER TABLE reqmcan ALTER COLUMN reqid DROP NOT NULL;
ALTER TABLE reqmcan ADD COLUMN canusuoper integer;
ALTER TABLE reqmcan ADD COLUMN canfecoper timestamp without time zone;
ALTER TABLE reqmcan ADD CONSTRAINT reqmcan_canusuoper_fkey FOREIGN KEY (canusuoper)
        REFERENCES public.commempl (id_commempl) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION;

ALTER TABLE reqmcantd ADD COLUMN cantdpriv char(1);


ALTER TABLE reqdcan ADD COLUMN reqestado char(1);
ALTER TABLE reqdcan ADD COLUMN trechid smallint;
ALTER TABLE reqdcan ADD COLUMN canmotrechfec date;
ALTER TABLE reqdcan ADD COLUMN canmotrechob varchar(255);
ALTER TABLE reqdcan ADD CONSTRAINT reqdcan_trechid_fkey FOREIGN KEY (trechid)
    REFERENCES reqmtrech (trechid) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;
ALTER TABLE reqdcan DROP COLUMN IF EXISTS cdetfecha;
ALTER TABLE reqdcan ADD COLUMN rdcusuoper integer;
ALTER TABLE reqdcan ADD COLUMN rdccanfecoper timestamp without time zone;
ALTER TABLE reqdcan ADD CONSTRAINT reqdcan_rdcusuoper_fkey FOREIGN KEY (rdcusuoper)
        REFERENCES public.commempl (id_commempl) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION;
		
---------------------------------------------------------------------------------------10


---------------------------------------------------------------------------------------11
DROP VIEW public.vw_reqcand;
CREATE OR REPLACE VIEW vw_reqcand AS
SELECT c.canid, c.cannombre1, c.cannombre2, 
       c.canapellido1, c.canidentif, 
	   CASE WHEN c.cangenero = 'F' THEN 'FEMENINO' ELSE 'MASCULINO' END AS cangenero, 
	   c.canemail, u.uornombre, a.afunombre, p.puenombre, 
	   r.reqid
FROM reqmcan c 
     LEFT JOIN reqmreq r ON c.reqid = r.reqid 
     LEFT JOIN glbuniorga u ON u.uorid = r.pueuniorg
	 LEFT JOIN glbmareaf a ON a.afuid = u.uorareaf
	 LEFT JOIN glbmpue p ON p.pueid = r.pueposicion;
---------------------------------------------------------------------------------------11


---------------------------------------------------------------------------------------12
ALTER TABLE reqmcan ADD COLUMN canproccant SMALLINT;
ALTER TABLE reqdcan ADD COLUMN canproccant SMALLINT;
---------------------------------------------------------------------------------------12
--- ACA EN BD
--- 20221105_1